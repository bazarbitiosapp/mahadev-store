//
//  AIEnums.swift
//  Swift3Demo
//
//  Created by Agile Mac Mini 4 on 02/01/17.
//  Copyright © 2017 Agile Mac Mini 4. All rights reserved.
//

import Foundation

enum AIBackButtonType
{
    case BlackArrow
    case WhiteArrow
    case Cross
    case None
}

//MARK:- TEXTFIELD TYPE
enum AITextFieldType: Int{
    case
    Normal,
    Name,
    Password,
    Email,
    Phone,
    Age,
    Date,
    Gender
}

enum AILoginTexField: Int{
    
    case
    firstname,
    lastname,
    phone,
    email,
    specificcode
    
    var description: String {
        switch self {
        case .firstname:
            return "First Name"
        case .lastname:
            return "Last Name"
        case .phone:
            return "Phone"
        case .email:
            return "Email Address"
        case .specificcode:
            return "Specific Code"
        }
    }
    
}
