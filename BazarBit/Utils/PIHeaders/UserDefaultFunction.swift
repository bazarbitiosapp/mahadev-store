//
//  UserDefaultFunction.swift
//  BazarBit
//
//  Created by Parghi Infotech on 9/30/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit

class UserDefaultFunction: NSObject {
    
    class public func setCustomDictionary(dict: [String: Any], key:String) {
        let dictData = NSKeyedArchiver.archivedData(withRootObject: dict)
        UserDefaults.standard.set(dictData, forKey: key)
    }
    
    class public func getDictionary(forKey: String) -> [String: Any]? {
        let dictData = UserDefaults.standard.object(forKey: forKey)
        let object = NSKeyedUnarchiver.unarchiveObject(with: (dictData as! NSData) as Data)
        return object as? [String: Any]
    }
    
}
