//
//  VariationListCollectionViewCell.swift
//  BazarBit
//
//  Created by Parghi Infotech on 24/10/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit

class VariationListCollectionViewCell: UICollectionViewCell {
    
    var strVariation : String = ""
    var position : Int = 0
    
    @IBOutlet var btnVariationName: UIButton!
    @IBOutlet var imageViewStrikeLine : UIImageView!
}
