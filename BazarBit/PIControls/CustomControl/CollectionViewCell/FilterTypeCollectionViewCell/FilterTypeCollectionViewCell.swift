//
//  FilterTypeCollectionViewCell.swift
//  BazarBit
//
//  Created by Sangani ajay on 10/27/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit

class FilterTypeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var buttonType: PIFilterButton!
    @IBOutlet var labelTitle: PIFilterType!
    var isSelect : Bool = false
    
    override func awakeFromNib() {
        labelTitle.layer.cornerRadius = 10.0
        self.layer.cornerRadius = 10.0
    }
}
