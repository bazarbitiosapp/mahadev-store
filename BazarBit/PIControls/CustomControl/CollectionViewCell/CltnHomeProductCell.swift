//
//  CltnHomeProductCell.swift
//  BazarBit
//
//  Created by Parghi Infotech on 11/10/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import Cosmos

class CltnHomeProductCell: UICollectionViewCell {
    
    @IBOutlet weak var imgproduct: UIImageView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var wishListHeightConstraints: NSLayoutConstraint!
    
    @IBOutlet weak var lblSalePrice: UILabel!
    @IBOutlet weak var lblBaseprice: UILabel!
    @IBOutlet var lblSalePriceWidth: NSLayoutConstraint!
    @IBOutlet var lblBasePriceWidth: NSLayoutConstraint!
    
    @IBOutlet weak var btnWishList: UIButton!
    @IBOutlet weak var ratingView: CosmosView!
    
    override func awakeFromNib() {
        self.lblProductName.textColor = UIColor(hexString: Constants.TEXT_PRIMARY_COLOR)
        self.lblBaseprice.textColor = UIColor(hexString: Constants.TEXT_LIGHT_COLOR)
        self.lblSalePrice.textColor = UIColor(hexString: Constants.TEXT_PRIMARY_COLOR)        
    }
    
    //MARK: SETUP DATA
    func setUpBestSellerData(arrModelDataBestSeller : [AnyObject]) {
        print(arrModelDataBestSeller)
    }
    
    func setUpMostWantedData(arrModelDataMostWanted : [AnyObject]) {
        print(arrModelDataMostWanted)
    }
    
    func setUpNewArrivalData(arrModelDataNewArrival : [AnyObject]) {
        print(arrModelDataNewArrival)
    }
    
    func setUpProductImagesData(arrModelDataProductImages : [String]) {
        print(arrModelDataProductImages)
    }

    func setUpProductImagesDataMostWanted(arrModelDataProductImagesMostWanted : [String])
    {
         print(arrModelDataProductImagesMostWanted)
    }
    
    func setUpProductImagesDataNewArrival(arrModelDataProductImagesNewArrival : [String])
    {
         print(arrModelDataProductImagesNewArrival)
    }
}
