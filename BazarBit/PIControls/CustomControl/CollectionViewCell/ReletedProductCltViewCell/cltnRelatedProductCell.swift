//
//  cltnRelatedProductCell.swift
//  BazarBit
//
//  Created by Parghi Infotech on 23/10/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import Cosmos

class cltnRelatedProductCell: UICollectionViewCell {
    
    @IBOutlet weak var lblProductImage: UIImageView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblSalePrice: UILabel!
    @IBOutlet weak var lblBasePrice: UILabel!
    
    @IBOutlet var lblSalePriceWidth: NSLayoutConstraint!
    @IBOutlet var lblBasePriceWidth: NSLayoutConstraint!
    
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var btnWishList: UIButton!
}
