//
//  CltnHomeCategoryCell.swift
//  BazarBit
//
//  Created by Parghi Infotech on 12/10/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit

class CltnHomeCategoryCell: UICollectionViewCell {
    
    @IBOutlet weak var imgCategory: UIImageView!
    @IBOutlet weak var lblCategryName: UILabel!
    
    @IBOutlet weak var viewborder: UIView!
    override func awakeFromNib() {
        self.lblCategryName.textColor = UIColor(hexString: Constants.TEXT_PRIMARY_COLOR)
    }
    
}
