//
//  MenuListTableViewCell.swift
//  BazarBit
//
//  Created by Sangani ajay on 10/16/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit

class MenuListTableViewCell: UITableViewCell {
    
    @IBOutlet var lableTitle: PICustomMenuTitle!
    @IBOutlet var imageViewMenuIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
