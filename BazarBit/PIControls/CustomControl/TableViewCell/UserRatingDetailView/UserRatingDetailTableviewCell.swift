//
//  UserRatingDetailTableviewCell.swift
//  BazarBit
//
//  Created by Parghi Infotech on 27/10/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import Cosmos

class UserRatingDetailTableviewCell: UITableViewCell {

    @IBOutlet weak var lbluserratingname: UILabel!
    @IBOutlet weak var ratingview: CosmosView!
    @IBOutlet weak var lbluserratingdate: UILabel!
    @IBOutlet weak var tblReviewHeight: NSLayoutConstraint!
    @IBOutlet weak var lbluserreviews: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.selectionStyle = .none
        
        lbluserratingname.setTextLabelDynamicTextColor()
        lbluserratingdate.setTextLabelLightColor()
        lbluserreviews.setTextLabelLightColor()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
