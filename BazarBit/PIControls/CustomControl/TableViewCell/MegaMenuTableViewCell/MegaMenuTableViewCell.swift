//
//  MegaMenuTableViewCell.swift
//  BazarBit
//
//  Created by Sangani ajay on 10/25/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit

class MegaMenuTableViewCell: UITableViewCell {
    
    @IBOutlet var labelLeading: NSLayoutConstraint!
    @IBOutlet var lableTitle : UILabel!
    @IBOutlet var imageViewArrow : UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.selectionStyle = .none        
        lableTitle.setTextLabelDynamicTextColor()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
