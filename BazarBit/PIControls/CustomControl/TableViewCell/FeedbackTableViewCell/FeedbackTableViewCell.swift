//
//  FeedbackTableViewCell.swift
//  BazarBit
//
//  Created by Sangani ajay on 10/31/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import EZSwiftExtensions
import Kingfisher

class FeedbackTableViewCell: UITableViewCell {
    
    
    @IBOutlet var viewBorder: UIView!
    @IBOutlet var viewButtonBGHeight: NSLayoutConstraint!
    @IBOutlet var viewButtonBG: UIView!

    @IBOutlet var buttonAddFeedback: PIFeedbackButton!
    
    @IBOutlet var imageViewProductIcon: UIImageView!
    @IBOutlet var labelProductName: PILabelFeedback!
    @IBOutlet var labelOrderId: PILabelFeedback!
    @IBOutlet var labelDate: PILabelFeedback!
    @IBOutlet var labelPrice: PIProductPriceLabel!
    @IBOutlet var labelOrderStatus: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
        self.selectionStyle = .none
    }
    
    func setUpData(dictData : [String : AnyObject]) {
        print(dictData)
        
        self.labelProductName.text = PIService.object_forKeyWithValidationForClass_String(dict: dictData, key: "name")
        self.labelOrderId.text = "Order Id :- \(PIService.object_forKeyWithValidationForClass_String(dict: dictData, key: "order_id"))"
        
        self.labelDate.text = BazarBitDate.Convert_DateString_OneToAnotherFormat(strdate: (PIService.object_forKeyWithValidationForClass_String(dict: dictData, key: "delievery_datetime")))
        
        let product_Price : String = PIService.object_forKeyWithValidationForClass_String(dict: dictData, key: "price")
        let strPrice = String(format: "%.2f", product_Price.toFloat()!)
        
        self.labelPrice.text = BazarBitCurrency.setPriceSign(strPrice: strPrice)
        
        let order_item_status : String = PIService.object_forKeyWithValidationForClass_String(dict: dictData, key: "order_item_status")
        
        print(order_item_status)
        
        if order_item_status.toInt() == 4 {
            self.labelOrderStatus.textColor = UIColor.white
            self.labelOrderStatus.text = "Delivered"
            self.labelOrderStatus.backgroundColor = UIColor(hexString: "076f30")
        }
        
        if let key = dictData["main_image"] {
            if (key as? [String : AnyObject]) != nil
            {
                print(key)
                var imgUrl:String = PIService.object_forKeyWithValidationForClass_String(dict: key as! [String : AnyObject], key: "main_image")
                imgUrl = imgUrl.replacingOccurrences(of: " ", with: "%20")
                let url1 = URL(string: imgUrl)
                self.imageViewProductIcon.kf.setImage(with: url1)
            }
        }
    }

}
