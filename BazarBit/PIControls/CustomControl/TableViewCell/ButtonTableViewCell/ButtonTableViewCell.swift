//
//  ButtonTableViewCell.swift
//  BazarBit
//
//  Created by Sangani ajay on 10/27/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit

class ButtonTableViewCell: UITableViewCell {
    
    @IBOutlet var buttonDropDown : UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.selectionStyle = .none
        buttonDropDown.setImage(UIImage(named: "dropd"), for: UIControlState.normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
