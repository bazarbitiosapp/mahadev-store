//
//  ForgotPasswordVC.swift
//  BazarBit
//
//  Created by Parghi Infotech on 05/10/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class ForgotPasswordVC: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var btnLogin: PICustomButton!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var txtForgotEmail: UITextField!
    @IBOutlet weak var forgotLable: PICustomLable!
    var strUserName:String = ""
    var dictResponse:[String : AnyObject] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.navigationController?.isNavigationBarHidden = false
        
        btnLogin.setButtonTextThemeColor()
        
        // Main Logo Image set
        var LogoImage:String = APPDELEGATE.logoImage
        LogoImage = LogoImage.replacingOccurrences(of: " ", with: "%20")
        let url1 = URL(string: LogoImage)
        imgLogo.kf.setImage(with: url1)
        
        txtForgotEmail.setLeftPaddingPoints(10.0)
        
        // Set dynamic border Color & Radius
        txtForgotEmail.setTextFieldDynamicColor()
        
        // Set button text color
        btnForgotPassword.setButtonDynamicColor()
        
        // Set Shadow and corner radius
        btnForgotPassword.Shadow()
        forgotLable.setTextLabelThemeColor()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- ACTION METHOD
    @IBAction func btnLogin(_ sender: Any) {
        if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
            myDelegate.openLoginPage()
        }
    }
    
    // MARK:- TEXTFIELD METHOD
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string == ""
        {
            return true
        }
        
        if txtForgotEmail == textField {
            if (textField.text?.characters.count)! >= 50 {
                return false
            }
            else
            {
                return true
            }
        }
        return true
    }
    
    @IBAction func btnForgotPassword(_ sender: Any) {
        if checkValidation() {
            
            let strurl: String = BazarBit.forgotPasswordService()
            let url: URL = URL(string: strurl)!
            
            var headers: HTTPHeaders = [:]
            headers[BazarBit.ContentType] = BazarBit.applicationJson
            headers["app-id"] = BazarBit.appId
            headers["app-secret"] = BazarBit.appSecret
            
            var parameter: Parameters = [:]
            parameter["email"] = strUserName
            print(parameter)
            
            SVProgressHUD.show()
            PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: { (swiftyJson, responseDict) in
                
                print(responseDict)
                self.dictResponse = responseDict
                print(url)
                
                SVProgressHUD.dismiss()
                let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
                PIAlertUtils.displayAlertWithMessage(message)
                
                let status: String = PIService.object_forKeyWithValidationForClass_String(dict: self.dictResponse, key: "status")
                
                if status == "ok" {
                    
                    if (self.dictResponse["payload"] != nil) {
                        if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
                            myDelegate.openLoginPage()
                        }
                    }
                }
            })
        }
    }
    
    // MARK: - VALIDATION HELPER METHOD
    func checkValidation() -> Bool {
        
        var isValid: Bool = true
        strUserName = txtForgotEmail.text!
        strUserName = strUserName.trimmed()
        
        if PIValidation.isBlankString(str: strUserName) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please Enter Email Address...!")
            return isValid
            
        } else if (PIValidation.isEmailString(str: strUserName)) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please Enter Valid Email...!")
            return isValid
        }
        return isValid
    }
}
