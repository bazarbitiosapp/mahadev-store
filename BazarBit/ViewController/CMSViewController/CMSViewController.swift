//
//  CMSViewController.swift
//  BazarBit
//
//  Created by Sangani ajay on 10/31/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CMSViewController: UIViewController {
    
    @IBOutlet var viewNavigation : UIView!
    @IBOutlet var lableTitle: PICustomTitleLable!
    @IBOutlet var textViewDescription: PICustomTextView!
    
    var strTitle : String = ""
    var strSlug : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.view.backgroundColor = UIColor(red: 236.0/255.0, green: 236.0/255.0, blue: 236.0/255.0, alpha: 1.0)
        self.textViewDescription.backgroundColor = UIColor.clear
        
        strSlug = UserDefaults.standard.object(forKey: "slug") as! String
        strTitle = UserDefaults.standard.object(forKey: "title") as! String
        
        self.lableTitle.text = strTitle.capitalized
        
        self.ServiceCallForLoadStaticPage(strSlug: strSlug)
        
        viewNavigation.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    // MARK:- ACTION METHOD
    @IBAction func btnOpenDrawer(_ sender: Any) {
        self.evo_drawerController?.toggleLeftDrawerSide(animated: true, completion: nil)
    }
    
    //MARK: ServiceCallForLoadStaticPage
    func ServiceCallForLoadStaticPage(strSlug : String) {
        
        let strurl:String = BazarBit.LoadStaticPageService()
        
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        print(headers)
        
        var parameter:Parameters = [:]
        parameter["page"] = strSlug
        print(parameter)
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: responseDict, key: "status")
            let messege = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            
            if status == "ok"
            {
                if (responseDict["payload"] != nil)
                {
                    let dictData : [String: AnyObject] = responseDict["payload"] as! [String: AnyObject]
                    
                    print(dictData)
                    
                    let htmlData = NSString(string: PIService.object_forKeyWithValidationForClass_String(dict: dictData, key: "description")).data(using: String.Encoding.unicode.rawValue)
                    
                    let attributedString = try! NSAttributedString(data: htmlData!, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
                    
                    self.textViewDescription.attributedText = attributedString
                }
            }
            else
            {
                PIAlertUtils.displayAlertWithMessage(messege)
            }
        })
    }
}
