
//
//  LoginVC.swift
//  BazarBit
//
//  Created by Parghi Infotech on 9/30/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import Toast_Swift
import Kingfisher
import EZSwiftExtensions
import SVProgressHUD
import Alamofire
import Toaster
import NVActivityIndicatorView
import Crashlytics
import PinterestSDK

class LoginVC: UIViewController, GIDSignInUIDelegate,GIDSignInDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtpwd: UITextField!
    @IBOutlet weak var btnSignup: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnDontAccount: UIButton!
    @IBOutlet weak var btnForgotpwd: UIButton!
    @IBOutlet weak var btnSkip: PICustomButton!
    @IBOutlet weak var imgLogo: UIImageView!
    
    @IBOutlet var socialBGWidthLogin : NSLayoutConstraint!
    
    @IBOutlet var viewFbWidth : NSLayoutConstraint!
    @IBOutlet var viewInstaWidth : NSLayoutConstraint!
    @IBOutlet var viewGoogleWidth : NSLayoutConstraint!
    @IBOutlet var viewlinkedInWidth : NSLayoutConstraint!
    @IBOutlet var viewPintrestWidth : NSLayoutConstraint!
    
    @IBOutlet var viewSocialBG : UIView!
    
    //SOCIAL
    
    //FACEBOOK
    @IBOutlet var viewFbLogin : UIView!
    @IBOutlet var buttonFbLogin : UIButton!
    @IBOutlet weak var imgFacebook : UIImageView!
    
    //GMAIL
    @IBOutlet var viewGmailLogin : UIView!
    @IBOutlet var buttonGmailLogin : UIButton!
    @IBOutlet weak var imgGmail : UIImageView!
    
    //INSTAGRAM
    @IBOutlet var viewInstaLogin : UIView!
    @IBOutlet var buttonInstaLogin : UIButton!
    @IBOutlet weak var imgInstagram : UIImageView!
    
    //LINKEDIN
    @IBOutlet var viewLinkedInLogin : UIView!
    @IBOutlet var buttonLinkedInLogin : UIButton!
    @IBOutlet weak var imgLinkedIn : UIImageView!
    
    //PINTREST
    @IBOutlet var viewPintrestLogin : UIView!
    @IBOutlet var buttonPintrestLogin : UIButton!
    @IBOutlet weak var imgPintrest : UIImageView!
    
    var strUserName:String = ""
    var strPassword:String = ""
    var dictResponse:[String:AnyObject] = [:]
    
    var socialParameter : Parameters = [:]
    
    var socialType: String = ""
    var socialId: String = ""
    var socialname: String = ""
    var socialemail: String = ""
    
    var facebookDetail : [String : AnyObject]!
    var googleDetail : [String : AnyObject] = [:]
    var pintrestDetail : [String : AnyObject] = [:]
    
    var dicResponceCartTotalList:[String:AnyObject] = [:]
    
    var isFromLogin : Bool = false
    var strStoredEmail : String = ""
    
    // MARK: - UIView Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if IS_IPAD_DEVICE() {
            let viewSocialWidth : CGFloat = 75.0
            viewFbWidth.constant = viewSocialWidth
            viewInstaWidth.constant = viewSocialWidth
            viewGoogleWidth.constant = viewSocialWidth
            viewPintrestWidth.constant = viewSocialWidth
            viewlinkedInWidth.constant = viewSocialWidth
            
            socialBGWidthLogin.constant = 75.0 * CGFloat(APPDELEGATE.arySocialLoginButton.count)
            print(socialBGWidthLogin.constant)
            viewSocialBG.layoutIfNeeded()
        }else {
            socialBGWidthLogin.constant = 55.0 * CGFloat(APPDELEGATE.arySocialLoginButton.count)
            print(socialBGWidthLogin.constant)
            viewSocialBG.layoutIfNeeded()
        }
        
        if APPDELEGATE.arySocialLoginButton.contains("FB") {
            viewFbLogin.isHidden = false
        }else {
            viewFbWidth.constant = 0.0
             viewFbLogin.isHidden = true
        }
        
        if APPDELEGATE.arySocialLoginButton.contains("Instagram") {
            viewInstaLogin.isHidden = false
        }else {
            viewInstaWidth.constant = 0.0
            viewInstaLogin.isHidden = true
        }
        
        if APPDELEGATE.arySocialLoginButton.contains("Google") {
            viewGmailLogin.isHidden = false
        }else {
            viewGoogleWidth.constant = 0.0
            viewGmailLogin.isHidden = true
        }
        
        if APPDELEGATE.arySocialLoginButton.contains("Pinterest") {
            viewPintrestLogin.isHidden = false
        }else {
            viewPintrestWidth.constant = 0.0
            viewPintrestLogin.isHidden = true
        }
        
        if APPDELEGATE.arySocialLoginButton.contains("Linkedin") {
            viewLinkedInLogin.isHidden = false
        }else {
            viewlinkedInWidth.constant = 0.0
            viewLinkedInLogin.isHidden = true
        }
        
        txtEmail.isHidden = false
        txtpwd.isHidden = false
        
        txtEmail.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0)
        txtpwd.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0)
        
        self.navigationController?.isNavigationBarHidden = true
        
        // Set Left Padding on textfields
        txtEmail.setLeftPaddingPoints(10.0)
        txtpwd.setLeftPaddingPoints(10.0)
        
        // Set dynamic border Color & Radius
        txtEmail.setTextFieldDynamicColor()
        txtpwd.setTextFieldDynamicColor()
        
        // Set dynamic Button text Color
        btnForgotpwd.setButtonTextThemeColor()
        btnSignup.setButtonTextThemeColor()
        btnSkip.setButtonTextThemeColor()
        btnDontAccount.setButtonTextThemeColor()
        
        // Set Shadow and corner radius
        btnLogin.Shadow()
        if Constants.DeviceType.IS_IPAD {
            btnLogin.layer.cornerRadius = 25.0
        }else {
            btnLogin.layer.cornerRadius = 15.0
        }
        
        // Main Logo Image set
        var LogoImage:String = APPDELEGATE.logoImage
        LogoImage = LogoImage.replacingOccurrences(of: " ", with: "%20")
        let url1 = URL(string: LogoImage)
        imgLogo.kf.setImage(with: url1)
        
        // Gmail Logo Image set
        var GmailImage:String = APPDELEGATE.googleImage
        GmailImage = GmailImage.replacingOccurrences(of: " ", with: "%20")
        let url2 = URL(string: GmailImage)
        imgGmail.kf.setImage(with: url2)
        
        // facebook Logo Image set
        var FacebookImage:String = APPDELEGATE.facebookImage
        FacebookImage = FacebookImage.replacingOccurrences(of: " ", with: "%20")
        let url3 = URL(string: FacebookImage)
        imgFacebook.kf.setImage(with: url3)
        
        // Insta Logo Image set
        var InstaImage:String = APPDELEGATE.instagramImage
        InstaImage = InstaImage.replacingOccurrences(of: " ", with: "%20")
        let url4 = URL(string: InstaImage)
        imgInstagram.kf.setImage(with: url4)
        
        // LinkedIn Logo Image set
        var linkedInImage:String = APPDELEGATE.linkedInImage
         linkedInImage = linkedInImage.replacingOccurrences(of: " ", with: "%20")
         let url5 = URL(string: linkedInImage)
         imgLinkedIn.kf.setImage(with: url5)
         
         // Pintrest Logo Image set
         var pintrestImage:String = APPDELEGATE.pintrestImage
         pintrestImage = pintrestImage.replacingOccurrences(of: " ", with: "%20")
         let url6 = URL(string: pintrestImage)
         imgPintrest.kf.setImage(with: url6)
        
        if (UserDefaults.standard.object(forKey: BazarBit.USEREMAILID) != nil)
        {
            if isFromLogin == false {
                txtEmail.text = UserDefaults.standard.object(forKey: BazarBit.USEREMAILID) as? String
            }else {
                self.txtEmail.text = ""
            }
        }
        
        if strStoredEmail != "" {
            print(strStoredEmail)
            txtEmail.text = strStoredEmail
        }
    }
    
    // Paste Disable
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
    
    // MARK:- TEXTFIELD METHOD
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string == "" {
            return true
        }
        
        if txtEmail == textField {
            if (textField.text?.characters.count)! >= 30 {
                return false
            } else {
                return true
            }
        }
        
        if txtpwd == textField {
            if (textField.text?.characters.count)! >= 30 {
                return false
            } else {
                return true
            }
        }
        return true
    }
    
    // MARK:-  ACTION METHOD
    @IBAction func btnLogin(_ sender: Any) {
        
        if checkValidation() {
            let strurl: String = BazarBit.loginService()
            let url: URL = URL(string: strurl)!
            
            var headers: HTTPHeaders = [:]
            headers[BazarBit.ContentType] = BazarBit.applicationJson
            headers["app-id"] = BazarBit.appId
            headers["app-secret"] = BazarBit.appSecret
            
            var parameter: Parameters = [:]
            parameter["email"] = strUserName
            parameter["password"] = strPassword
            parameter["device_id"] = UserDefaults.standard.object(forKey: BazarBit.DEVICEID)

            print(parameter)
            
            PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: { (swiftyJson, responseDict) in
                
                //print(responseDict)
                self.dictResponse = responseDict
                //print(url)
                
                let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
                
                let status: String = PIService.object_forKeyWithValidationForClass_String(dict: self.dictResponse, key: "status")
                
                if status == "ok" {
                    if (UserDefaults.standard.object(forKey: BazarBit.SHIPPINGADDRESS) != nil) {
                        UserDefaults.standard.removeObject(forKey: BazarBit.SHIPPINGADDRESS)
                    }
                    
                    if (UserDefaults.standard.object(forKey: BazarBit.BILLINGADDRESS) != nil) {
                        UserDefaults.standard.removeObject(forKey: BazarBit.BILLINGADDRESS)
                    }
                    
                    if (self.dictResponse["payload"] != nil) {
                        
                        let payload: [String: AnyObject] = self.dictResponse["payload"] as! [String : AnyObject]
                        
                        UserDefaultFunction.setCustomDictionary(dict: payload as [String : AnyObject], key: BazarBit.USERACCOUNT)
                        
                        // USER ID
                        let strUserId: String = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "user_id")
                        
                        UserDefaults.standard.set(strUserId, forKey: BazarBit.USERID)
                        
                        // USER NAME
                        let strUserName : String = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "firstname")
                        
                        UserDefaults.standard.set(strUserName, forKey: BazarBit.USERNAME)
                        print(strUserName)
                        
                        // USER EMAIL ADDRESS
                        let strUserEmail : String = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "email")
                        
                        UserDefaults.standard.set(strUserEmail, forKey: BazarBit.USEREMAILADDRESS)
                        print(strUserEmail)
                        
                        // USER PROFILE IMAGE
                        if (payload["profilepicture"] as? [AnyObject]) != nil
                        {
                            let arrUserProfile : [AnyObject] = payload["profilepicture"] as! [AnyObject]
                            print(arrUserProfile)
                            
                            if arrUserProfile.count > 0
                            {
                                let profileImage : String = arrUserProfile[0] as! String
                                print(profileImage)
                                
                                UserDefaults.standard.set(profileImage, forKey: BazarBit.USERPROFILEIMAGE)
                            }
                        }
                        
                        let authoUser: [String: Any] = payload["authUser"] as! [String : Any]
                        UserDefaults.standard.set(authoUser["user_token"], forKey: BazarBit.AUTHTOKEN)
                        
                        if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
                            
                            if APPDELEGATE.GlobalVC != nil {
                   
                                self.navigationController?.popToViewController(APPDELEGATE.GlobalVC, animated: true)
                                APPDELEGATE.GlobalVC = nil
                                
                            } else {
                                myDelegate.openHomePage()
                            }
                        }
                    }
                }
                else
                {
                    PIAlertUtils.displayAlertWithMessage(message)
                }
            })
        }
    }
    
    @IBAction func btnSignUp(_ sender: Any) {
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "SignUpVC")
        self.navigationController?.pushViewController(nav!, animated: true)
    }
    
    @IBAction func btnForgotPassword(_ sender: Any) {
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "ForgotVC")
        self.navigationController?.pushViewController(nav!, animated: true)
    }
    
    @IBAction func btnSkip(_ sender: Any) {
        if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
            myDelegate.openHomePage()
        }
        self.GetCartCountData()
    }
    
    // MARK:-  SOCIAL ACTION METHOD
    @IBAction func btnFacebook(_ sender: Any) {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        self.getFBUserData()
                        fbLoginManager.logOut()
                    }
                }
            }
        }
    }
    
    @IBAction func btnGmail(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
        GIDSignIn.sharedInstance().signOut()
    }
    
    @IBAction func btnInstagram(_ sender: Any) {
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "instagramVC")
        self.navigationController?.pushViewController(nav!, animated: true)
    }
    
    @IBAction func btnLinkedIn(_ sender: Any) {
        let objLinkedIn = self.storyboard?.instantiateViewController(withIdentifier: "LinkedInViewController")
        self.navigationController?.pushViewController(objLinkedIn!, animated: true)
    }
    
    @IBAction func btnPintrest(_ sender: Any) {

//        PDKClient.sharedInstance().createPin(with: imageToShare, link: URL.init(string: "https://someUrl"), onBoard: "MyBoard", description: "",
//                                             progress: { (percent) in
//        }, withSuccess: { (response) in
//            print("Success")
//        }) { (error) in
//            if let error = error {
//                print(error)
//            }
//        }
        PDKClient.sharedInstance().createBoard("Hi", boardDescription: "Moin", withSuccess: { (PDKResponseObject) in
                print("HHHHHH")
        }) { (Error) in
            print(Error ?? "nil")
        }
        PDKClient.sharedInstance().authenticate(withPermissions: [PDKClientReadPublicPermissions,PDKClientWritePublicPermissions,PDKClientReadRelationshipsPermissions,PDKClientWriteRelationshipsPermissions], withSuccess: { (PDKResponseObject) in
            
            
            self.pintrestDetail["id"] = PDKResponseObject?.user().identifier as AnyObject
            self.pintrestDetail["firstName"] = PDKResponseObject?.user().firstName as AnyObject
            self.pintrestDetail["lastName"] = PDKResponseObject?.user().lastName as AnyObject
            
            self.socialId = (PDKResponseObject?.user().identifier)!
            self.socialType = "pintrest"
            
            let fName : String = (PDKResponseObject?.user().firstName)!
            let lName : String = (PDKResponseObject?.user().lastName)!
            
            self.socialname = "\(fName) \(lName)"
            self.socialParameter["social_id"] = self.socialId
            self.socialParameter["social_type"] = self.socialType
            self.socialLoginService()
            
        }) { (Error) in
            print(Error ?? "nil")
        }
    }
    
    //MARK: SOCIAL SERVICE METHOD
    func socialLoginService() {
        
        let strurl: String = BazarBit.SocialMediaService()
        let url: URL = URL(string: strurl)!
        
        var headers: HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        
        PIService.serviceCall(url: url, method: .post, parameter: socialParameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true) { (swiftyJson, responseDict) in
            
            let dict:[String: AnyObject] = responseDict
            
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            
            let status: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "status")
            
            if status == "ok" {
                
                if (dict["payload"] != nil) {
                    
                    let payload: [String: AnyObject] = dict["payload"] as! [String : AnyObject]
                    UserDefaultFunction.setCustomDictionary(dict: payload, key: BazarBit.USERACCOUNT)
                    
                    let strUserId: String = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "user_id")
                    UserDefaults.standard.set(strUserId, forKey: BazarBit.USERID)
                    
                    let authoUser: [String: AnyObject] = payload["authUser"] as! [String : AnyObject]
                    UserDefaults.standard.set(authoUser["user_token"], forKey: BazarBit.AUTHTOKEN)
                    
                    let strFName:String = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "firstname")
                    
                    UserDefaults.standard.set(strFName, forKey: BazarBit.USERNAME)
                    
                    let strEmail:String = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "username")
                    UserDefaults.standard.set(strEmail, forKey: BazarBit.USEREMAILADDRESS)
                    
                    if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
                        myDelegate.openHomePage()
                    }
                }
            } else {
                
                let registerVC = self.storyboard?.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
                
                if self.socialType == "facebook" {
                    registerVC.dictfacebook = self.facebookDetail
                }
                
                if self.socialType == "google" {
                    registerVC.dictGoogle = self.googleDetail
                }
                
                if self.socialType == "pintrest" {
                    registerVC.dictPintrest = self.pintrestDetail
                }
                
                // FAcbook Detail And Gmail Details Push
                registerVC.socialId = self.socialId
                registerVC.socialType = self.socialType
                registerVC.socialName = self.socialname
                self.navigationController?.pushViewController(registerVC, animated: true)
                PIAlertUtils.displayAlertWithMessage(message)
            }
        }
    }
    
    // MARK: - CART COUNT SERVICE METHOD
    func GetCartCountData() {
        
        let strurl:String = BazarBit.CountCartTotalService()
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        
        var parameter:Parameters = [:]
        let UserId = UserDefaults.standard.object(forKey: BazarBit.USERID)
        
        if UserId != nil {
            parameter["user_id"] = UserDefaults.standard.object(forKey: BazarBit.USERID)
        }
        else {
            let strSessionId:String = BazarBit.getSessionId()
            parameter["session_id"] = strSessionId
            parameter["user_id"] = ""
        }
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            self.dicResponceCartTotalList = responseDict
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: self.dicResponceCartTotalList, key: "status")
            
            if status == "ok"
            {
                if self.dicResponceCartTotalList["payload"] != nil
                {
                    let payload:[String:AnyObject] = self.dicResponceCartTotalList["payload"] as! [String:AnyObject]
                    
                    let strCartTotal: String = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "cart_items")
                    
                    UserDefaults.standard.set(strCartTotal, forKey: BazarBit.CARTTOTALCOUNT)
                    
                    if (UserDefaults.standard.object(forKey: BazarBit.CARTTOTALCOUNT) != nil) {
                    }
                }
            }
        })
    }
    
    // MARK: - SOCIAL HELPER METHOD
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    self.facebookDetail = result as! [String : AnyObject]
                    
                    // Get user Social Details
                    self.socialId = PIService.object_forKeyWithValidationForClass_String(dict: self.facebookDetail, key: "id")
                    self.socialType = "facebook"
                    self.socialname = PIService.object_forKeyWithValidationForClass_String(dict: self.facebookDetail, key: "name")
                    self.socialemail = PIService.object_forKeyWithValidationForClass_String(dict: self.facebookDetail, key: "email")
                    
                    self.socialParameter["social_id"] = self.socialId
                    self.socialParameter["social_type"] = self.socialType
                    self.socialParameter["email_id"] =  self.socialemail
                    self.socialLoginService()
                }
            })
        }
    }
    
    // Get user Gmail Social Details Delegate methods
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let firstName = user.profile.givenName
            let lastName = user.profile.familyName
            let email = user.profile.email
            
            // Custom Key For use in SignUp Page
            self.googleDetail["fullName"] = fullName as AnyObject
            self.googleDetail["givenName"] = firstName as AnyObject
            self.googleDetail["familyName"] = lastName as AnyObject
            self.googleDetail["email"] = email as AnyObject
            
            self.socialId = userId!
            self.socialType = "google"
            self.socialname = fullName!
            self.socialemail = email!
            
            socialParameter["social_id"] = socialId
            socialParameter["social_type"] = socialType
            socialParameter["email_id"] = socialemail
            socialLoginService()
            
        } else {
            print("\(error.localizedDescription)")
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        print("error")
    }
    
    // MARK: - VALIDATION HELPER METHOD
    func checkValidation() -> Bool {
        
        var isValid: Bool = true
        
        strUserName = txtEmail.text!
        strPassword = txtpwd.text!
        
        strUserName = strUserName.trimmed()
        strPassword = strPassword.trimmed()
        
        if PIValidation.isBlankString(str: strUserName) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please Enter Email Address...!")
            
            return isValid
            
        } else if (PIValidation.isEmailString(str: strUserName)) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please Enter Valid Email...!")
            return isValid
        }
            
        else if (PIValidation.isBlankString(str: strPassword))
        {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please Enter Password...!")
            return isValid
            
        } else if strPassword.characters.count <= 5 || strPassword.characters.count >= 30 {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please Enter Password Between 6 to 30 Character...!")
            return isValid
        }
        return isValid
    }
    
    // MARK: - GMAIL HELPER METHOD
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
}
