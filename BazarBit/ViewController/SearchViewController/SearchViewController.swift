//
//  SearchViewController.swift
//  BazarBit
//
//  Created by Parghi Infotech on 01/11/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import Alamofire

class SearchViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {
    
    @IBOutlet var viewNavigation : UIView!
    @IBOutlet var viewSearch : UIView!
    @IBOutlet var labelSearchNotFound : UILabel!
    @IBOutlet weak var tblOpenSearch: UITableView!
    @IBOutlet weak var txtSearchproducts: UITextField!
    @IBOutlet weak var noResultView: UIView! 
    
    var strTemp : String = ""
    var dicResponceSearchList:[String:AnyObject] = [:]
    var arrSearchProductList:[AnyObject] = []
    var arrProductName:[String] = []
    var tabbar: TabbarVC!
    
    // MARK:- UIVIEW METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        
        noResultView.isHidden = false
        tblOpenSearch.isHidden = true
        
        tblOpenSearch.dataSource = self
        tblOpenSearch.delegate = self
        
        viewNavigation.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        viewSearch.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        labelSearchNotFound.textColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Set Uitextfield Left Image
        let leftImageView = UIImageView()
        leftImageView.image = UIImage(named: "search")
        let leftView = UIView()
        leftView.addSubview(leftImageView)
        
        if Constants.DeviceType.IS_IPAD {
            leftView.frame = CGRect(x: 30, y: 0, width: 35, height: 25)
            leftImageView.frame = CGRect(x: 12, y: 0, width: 25, height: 25)
        }
        else
        {
            leftView.frame = CGRect(x: 15, y: 0, width: 25, height: 15)
            leftImageView.frame = CGRect(x: 6, y: 0, width: 15, height: 15)
        }
        txtSearchproducts.leftViewMode = .always
        txtSearchproducts.leftView = leftView
        txtSearchproducts.setTextFieldDynamicColor()
        txtSearchproducts.setCornerRadius(radius: 5)
        txtSearchproducts.backgroundColor = UIColor.white
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        tblOpenSearch.isHidden = true
        noResultView.isHidden = false
    }
    
    // MARK:- UITEXTFIELD METHOD
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        noResultView.isHidden = true
        
        if textField.text?.characters.count == 1 && string == "" {
            self.noResultView.isHidden = false
            self.tblOpenSearch.isHidden = true
            return true
        }
        
        if string == ""
        {
            strTemp = (textField.text?.substring(to: (textField.text?.index(before: (textField.text?.endIndex)!))!))!
            GetSearchProductList(strSearch: strTemp)
        }
        else{
            tblOpenSearch.isHidden = false
            strTemp = textField.text! + string
            print(strTemp)
            GetSearchProductList(strSearch: strTemp)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK- ACTION METHOD
    @IBAction func btnBack(_ sender: Any) {
        if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
            myDelegate.openHomePage()
        }
    }
    
    // MARK:- UITABLEVIEW METHODS
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblOpenSearch {
            return arrSearchProductList.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblOpenSearch {
            
            let Opentblcell:HomeSearchproductcell  = tableView.dequeueReusableCell(withIdentifier: "HomeSearchproductcell", for: indexPath) as! HomeSearchproductcell
            
            let dict = arrSearchProductList[indexPath.row] as! [String:AnyObject]
            print(dict)
            
            Opentblcell.lblSearchList.text = arrProductName[indexPath.row]
            
            return Opentblcell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = arrSearchProductList[indexPath.row] as! [String:AnyObject]
        print(dict)
        
        APPDELEGATE.ProductID = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "product_id")
        //print(APPDELEGATE.ProductID)
        APPDELEGATE.slugName = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "slug")
        //print(APPDELEGATE.slugName)
        
        //        if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
        //            myDelegate.OpenProductDetailPage()
        //
        //        }
        let productDetailVC = storyboard?.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
        self.navigationController?.pushViewController(productDetailVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tblOpenSearch
        {
            if Constants.DeviceType.IS_IPAD {
                return 44
            }else {
                return 30
            }
        }
        return 0.0
    }
    
    // MARK:- SERVICE METHOD
    func GetSearchProductList(strSearch : String) {
        let strurl:String = BazarBit.GetSearchProductListService()
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        
        var parameter:Parameters = [:]
        parameter["productname"] = strSearch
        print(parameter)
        
        PIService.serviceCallWithoutLoader(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, completion: {(swiftyJson, responseDict) in
            
            self.dicResponceSearchList = responseDict
            let status:String = self.dicResponceSearchList["status"] as! String
            
            if status == "ok"
            {
                self.arrSearchProductList = []
                if self.dicResponceSearchList["payload"] != nil
                {
                    let payload:[String:AnyObject] = self.dicResponceSearchList["payload"] as! [String:AnyObject]
                    print(payload)
                    
                    if (payload["products"] as? [AnyObject]) != nil
                    {
                        self.arrSearchProductList = payload["products"] as! [AnyObject]
                        print(self.arrSearchProductList)
                        
                        self.arrProductName = []
                        if self.arrSearchProductList.count > 0
                        {
                            for i in 0...self.arrSearchProductList.count-1
                            {
                                let dictName:[String:AnyObject] = self.arrSearchProductList[i] as! [String:AnyObject]
                                print(dictName)
                                
                                if (dictName["name"] as? String) != nil
                                {
                                    let productName = PIService.object_forKeyWithValidationForClass_String(dict: dictName, key: "name")
                                    //print(productName)
                                    
                                    self.arrProductName.append(productName)
                                    //print(self.arrProductName)
                                }
                            }
                        }
                        
                        if self.arrSearchProductList.count != 0
                        {
                            self.noResultView.isHidden = true
                            self.tblOpenSearch.isHidden = false
                        }else {
                            self.noResultView.isHidden = false
                            self.tblOpenSearch.isHidden = true
                        }
                        self.tblOpenSearch.reloadData()
                    }
                }
            }
        })
    }
}
