//
//  OrderDetailViewController.swift
//  BazarBit
//
//  Created by Parghi Infotech on 10/30/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import Alamofire
import Toaster
import Cosmos
import IQKeyboardManagerSwift

class OrderDetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    
    @IBOutlet var viewNavigation : UIView!
    var scrlView = TPKeyboardAvoidingScrollView()
    
    @IBOutlet var viewOrderDetail: UIView!
    @IBOutlet var viewPaymentDetail: UIView!
    @IBOutlet var viewAddressDetail: UIView!
    @IBOutlet var viewAddYourReview: UIView!
    
    @IBOutlet var viewPaymentType_1_COD: UIView!
    @IBOutlet var viewPaymentType_2: UIView!
    
    @IBOutlet var viewCosmos: CosmosView!
    
    var dimView:UIView?
    
    @IBOutlet var viewPaymentInfoHeight : NSLayoutConstraint!
    
    @IBOutlet var viewDiscountHeight : NSLayoutConstraint!
    @IBOutlet var viewShippingChargeHeight : NSLayoutConstraint!
    @IBOutlet var viewCodeHeight : NSLayoutConstraint!
    
    @IBOutlet var lblTitleOrderDate: UILabel!
    @IBOutlet var lblTitleOrderID: UILabel!
    @IBOutlet var lblTitleOrderTotal: UILabel!
    
    @IBOutlet var lblOrderDate: UILabel!
    @IBOutlet var lblOrderID: UILabel!
    @IBOutlet var lblOrderTotal: UILabel!
    
    @IBOutlet var lblTitleItemTotal: UILabel!
    @IBOutlet var lblTitleDiscount: UILabel!
    @IBOutlet var lblTitleShippingCharge: UILabel!
    @IBOutlet var lblTitleCodeCharge: UILabel!
    @IBOutlet var lblTitleTotal: UILabel!
    
    @IBOutlet var lblPaymentInfo: UILabel!
    @IBOutlet var lblItemTotal: UILabel!
    @IBOutlet var lblDiscount: UILabel!
    @IBOutlet var lblShippingCharge: UILabel!
    @IBOutlet var lblCodeCharge: UILabel!
    @IBOutlet var lblTotal: UILabel!
    
    @IBOutlet var tblOrderItemReview: UITableView!
    @IBOutlet var tblOrderItemReviewHeight: NSLayoutConstraint!
    @IBOutlet var arrOrderItem: [AnyObject] = []
    
    @IBOutlet var lblTitleShippingAddress: UILabel!
    @IBOutlet var lblShippingFullName: UILabel!
    @IBOutlet var lblShippingAddress: UILabel!
    @IBOutlet var lblShippingCity: UILabel!
    @IBOutlet var lblShippingState: UILabel!
    @IBOutlet var lblShippingPhoneNo: UILabel!
    
    @IBOutlet var lblTitleBillingAddress: UILabel!
    @IBOutlet var lblBillingFullName: UILabel!
    @IBOutlet var lblBillingAddress: UILabel!
    @IBOutlet var lblBillingCity: UILabel!
    @IBOutlet var lblBillingState: UILabel!
    @IBOutlet var lblBillingPhoneNo: UILabel!
    
    
    @IBOutlet var lblTitleReview: UILabel!
    @IBOutlet var lblMsg: UILabel!
    
    @IBOutlet var txtName: UITextField!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtTitle: UITextField!
    @IBOutlet var txtDiscribtion: UITextField!
    
    @IBOutlet var lblTitleReviewDetail: UILabel!
    
    @IBOutlet var txtAccountHolderName: UITextField!
    @IBOutlet var txtBankName: UITextField!
    @IBOutlet var txtBranchName: UITextField!
    @IBOutlet var txtAccountNo: UITextField!
    @IBOutlet var txtIFSCCode: UITextField!
    
    var strAccountHolderName: String = ""
    var strBankName: String = ""
    var strBranchName: String = ""
    var strAccountNo: String = ""
    var strIFSCCode: String = ""
    var strReason_COD: String = ""
    
    @IBOutlet var lblTitleReviewDetail_Type2: UILabel!
    
    @IBOutlet var txtReason_COD: UITextField!
    @IBOutlet var txtReason_2Type: UITextField!
    var strReason_2Type: String = ""
    
    @IBOutlet var btnSubmit_COD: UIButton!
    @IBOutlet var btnSubmit_2Type: UIButton!
    
    var strReviewName: String = ""
    var strReviewEmail: String = ""
    var strReviewTitle: String = ""
    var strReviewDescribtion: String = ""
    var strRecommendedThisProduct: String = ""
    var strItemOrderId: String = ""
    var strProductID: String = ""
    
    var strUpdateItemStatus: String = ""
    var strUpdateItemOrderId: String = ""
    
    @IBOutlet var BtnSubmit: UIButton!
    
    @IBOutlet var imgYes: UIImageView!
    @IBOutlet var imgNo: UIImageView!
    
    @IBOutlet var btnYes: UIButton!
    @IBOutlet var btnNo: UIButton!
    
    @IBOutlet var lblYes: UILabel!
    @IBOutlet var lblNo: UILabel!
    
    var strSlug: String = ""
    var arrSignleRecord: [AnyObject] = []
    
    //AJAY - CANCEL ORDER
    @IBOutlet var viewReturnOrder_PayuMoney: UIView!
    @IBOutlet var btnCancelOrder_PayUMoney: UIButton!
    
    @IBOutlet var textFieldAccountHolderName: UITextField!
    @IBOutlet var textFieldBankName: UITextField!
    @IBOutlet var textFieldBranchName: UITextField!
    @IBOutlet var textFieldAccountNo: UITextField!
    @IBOutlet var textFieldIFSCCode: UITextField!
    @IBOutlet var textFieldReason: UITextField!
    
    var strCancel_AccountHolderName: String = ""
    var strCancel_BankName: String = ""
    var strCancel_BranchName: String = ""
    var strCancel_AccountNo: String = ""
    var strCancel_IFSCCode: String = ""
    var strCancel_Reason_COD: String = ""
    
    //MARK: - UIView Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setPropertiesOFView()
        addPropertyPopupFunction()
        addPropertyOFReturnOrderPopupFunction()
        setBorderPropertiesForTextField()
        callService_OrderDetail()
        
        viewNavigation.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        
        lblTitleOrderDate.setTextLabelDynamicTextColor()
        lblTitleOrderID.setTextLabelDynamicTextColor()
        lblTitleOrderTotal.setTextLabelDynamicTextColor()
        
        lblOrderDate.setTextLabelLightColor()
        lblOrderID.setTextLabelLightColor()
        lblOrderTotal.setTextLabelLightColor()
        
        //PAYMENT INFO
        lblTitleItemTotal.setTextLabelDynamicTextColor()
        lblTitleDiscount.setTextLabelLightColor()
        lblTitleShippingCharge.setTextLabelLightColor()
        lblTitleCodeCharge.setTextLabelLightColor()
        lblTitleTotal.setTextLabelDynamicTextColor()
        
        lblPaymentInfo.setTextLabelDynamicTextColor()
        lblItemTotal.setTextLabelDynamicTextColor()
        lblDiscount.setTextLabelLightColor()
        lblShippingCharge.setTextLabelLightColor()
        lblCodeCharge.setTextLabelLightColor()
        lblTotal.setTextLabelDynamicTextColor()
        
        //SHIPPING ADDRESS
        lblTitleShippingAddress.setTextLabelDynamicTextColor()
        lblShippingFullName.setTextLabelDynamicTextColor()
        lblShippingAddress.setTextLabelLightColor()
        lblShippingCity.setTextLabelLightColor()
        lblShippingState.setTextLabelLightColor()
        lblShippingPhoneNo.setTextLabelLightColor()
        
        //BILLING ADDRESS
        lblTitleBillingAddress.setTextLabelDynamicTextColor()
        lblBillingFullName.setTextLabelDynamicTextColor()
        lblBillingAddress.setTextLabelLightColor()
        lblBillingCity.setTextLabelLightColor()
        lblBillingState.setTextLabelLightColor()
        lblBillingPhoneNo.setTextLabelLightColor()
        
        lblTitleReview.setTextLabelDynamicTextColor()
        lblMsg.setTextLabelLightColor()
        
        txtName.setTextFieldDynamicColor()
        txtEmail.setTextFieldDynamicColor()
        txtTitle.setTextFieldDynamicColor()
        txtDiscribtion.setTextFieldDynamicColor()
        
        BtnSubmit.setButtonDynamicColor()
        BtnSubmit.Shadow()
        
        lblTitleReviewDetail.setTextLabelDynamicTextColor()
        lblTitleReviewDetail_Type2.setTextLabelDynamicTextColor()
        
        txtAccountHolderName.setTextFieldDynamicColor()
        txtBankName.setTextFieldDynamicColor()
        txtBranchName.setTextFieldDynamicColor()
        txtAccountNo.setTextFieldDynamicColor()
        txtIFSCCode.setTextFieldDynamicColor()
        txtReason_COD.setTextFieldDynamicColor()
        txtReason_2Type.setTextFieldDynamicColor()
        
        //RETURN ORDER PAYUMONEY
        textFieldAccountHolderName.setTextFieldDynamicColor()
        textFieldBankName.setTextFieldDynamicColor()
        textFieldBranchName.setTextFieldDynamicColor()
        textFieldAccountNo.setTextFieldDynamicColor()
        textFieldIFSCCode.setTextFieldDynamicColor()
        textFieldReason.setTextFieldDynamicColor()
        
        btnCancelOrder_PayUMoney.setButtonDynamicColor()
        btnCancelOrder_PayUMoney.Shadow()
        
        
        btnSubmit_COD.setButtonDynamicColor()
        btnSubmit_COD.Shadow()
        btnSubmit_2Type.setButtonDynamicColor()
        btnSubmit_2Type.Shadow()
        
        lblYes.setTextLabelDynamicTextColor()
        lblNo.setTextLabelDynamicTextColor()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- UITableView Method
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOrderItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblOrderItemReview {
            
            let orderItemReviewCell : OrderItemReviewCell = tableView.dequeueReusableCell(withIdentifier: "OrderItemReviewCell") as! OrderItemReviewCell
            
            if Constants.DeviceType.IS_IPAD {
                orderItemReviewCell.viewHeightConstraints.constant = 44.0
            } else {
                orderItemReviewCell.viewHeightConstraints.constant = 30.0
            }
            
            let DictItem: [String: AnyObject] = arrOrderItem[indexPath.row] as! [String: AnyObject]
            let DictOrder: [String: AnyObject] = arrSignleRecord[0] as! [String: AnyObject]
            
            let orderPaymentStatus : String = PIService.object_forKeyWithValidationForClass_String(dict: DictOrder, key: "order_payment_status")
            print(orderPaymentStatus)
            let strpaymentType: String = PIService.object_forKeyWithValidationForClass_String(dict: DictOrder, key: "order_payment_type_display")
            print(strpaymentType)
            
            let strProductName: String = PIService.object_forKeyWithValidationForClass_String(dict: DictItem, key: "name")
            let strQuantity: String = PIService.object_forKeyWithValidationForClass_String(dict: DictItem, key: "quantity")
            var strOrderDateTime: String = PIService.object_forKeyWithValidationForClass_String(dict: DictItem, key: "expected_delivery_time")
            var strPrize: String = PIService.object_forKeyWithValidationForClass_String(dict: DictItem, key: "price")
            
            let reviewDetailID: String = PIService.object_forKeyWithValidationForClass_String(dict: DictItem, key: "reviewdetail_id")
            print(reviewDetailID)
            
            let strOrderItemStatus: String = PIService.object_forKeyWithValidationForClass_String(dict: DictItem, key: "order_item_status")
            var orderItemStatus: Int = 0
            
            if (strOrderItemStatus.toInt() != nil) {
                orderItemStatus = strOrderItemStatus.toInt()!
            }
            
            let strPaymentType: String = PIService.object_forKeyWithValidationForClass_String(dict: DictItem, key: "order_payment_type")
            var paymentType: Int = 0
            
            if (strPaymentType.toInt() != nil) {
                paymentType = strPaymentType.toInt()!
            }
            
            // Equal width constraints
            orderItemReviewCell.btnReviewWidthConstraints.constant = self.view.frame.size.width / 2
            orderItemReviewCell.btnCancelWidthConstraints.constant = self.view.frame.size.width / 2
       
            if orderItemStatus >= 0 {
                
                if paymentType > 1 && orderItemStatus != 1 {
                    
                    if orderItemStatus == 5 {
                        orderItemReviewCell.btnCancel.setTitle("Cancelled Order", for: .normal)
                        orderItemReviewCell.btnCancel.isEnabled = false
                        orderItemReviewCell.btnCancel.isHidden = false
                        
                    }
                        
                    else if orderItemStatus == 4 {
                        let strDeliveryDays: String = PIService.object_forKeyWithValidationForClass_String(dict: DictItem, key: "DeliveryDays")
                        let strReturnDays: String = PIService.object_forKeyWithValidationForClass_String(dict: DictItem, key: "return_days")
                        
                        var DeliveryDays: Int = 0
                        var ReturnDays: Int = 0
                        
                        if (strDeliveryDays.toInt() != nil) {
                            DeliveryDays = strDeliveryDays.toInt()!
                        }
                        
                        if (strReturnDays.toInt() != nil) {
                            ReturnDays = strReturnDays.toInt()!
                        }
                        
                        if ReturnDays > 0 && DeliveryDays <= ReturnDays {
                            orderItemReviewCell.btnCancel.setTitle("Return Order", for: .normal)
                            orderItemReviewCell.btnCancel.isEnabled = true
                            orderItemReviewCell.btnCancel.isHidden = false
                            
                        } else {
                            orderItemReviewCell.btnCancel.setTitle("Order Delivered", for: .normal)
                            orderItemReviewCell.btnCancel.isEnabled = false
                            orderItemReviewCell.btnCancel.isHidden = false
                        }
                    }
                        
                    else if orderItemStatus == 6 {
                        orderItemReviewCell.btnCancel.setTitle("Order Returned", for: .normal)
                        orderItemReviewCell.btnCancel.isEnabled = false
                        orderItemReviewCell.btnCancel.isHidden = false
                        
                    }
                        
                    else if orderItemStatus == 7 {
                        orderItemReviewCell.btnCancel.setTitle("Order Refund", for: .normal)
                        orderItemReviewCell.btnCancel.isEnabled = false
                        orderItemReviewCell.btnCancel.isHidden = false
                        
                    }
                    else if orderItemStatus == 3 {
                        orderItemReviewCell.btnCancel.isEnabled = true
                        orderItemReviewCell.btnCancel.isHidden = true
                        
                        if orderItemReviewCell.btnCancel.isHidden == true
                        {
                            if Constants.DeviceType.IS_IPAD {
                                orderItemReviewCell.btnCancelWidthConstraints.constant = 0
                            }
                            else
                            {
                                orderItemReviewCell.btnCancelWidthConstraints.constant = 0
                            }
                        }
                    }
                        
                    else {
                        orderItemReviewCell.btnCancel.setTitle("Cancel", for: .normal)
                        orderItemReviewCell.btnCancel.isEnabled = false
                        orderItemReviewCell.btnCancel.isHidden = false
                    }
                }
                else
                {
                    if orderItemStatus == 5 {
                        orderItemReviewCell.btnCancel.setTitle("Cancelled Order", for: .normal)
                        orderItemReviewCell.btnCancel.isEnabled = false
                        orderItemReviewCell.btnCancel.isHidden = false
                        
                    }
                        
                    else if orderItemStatus == 4 {
                        let strDeliveryDays: String = PIService.object_forKeyWithValidationForClass_String(dict: DictItem, key: "DeliveryDays")
                        let strReturnDays: String = PIService.object_forKeyWithValidationForClass_String(dict: DictItem, key: "return_days")
                        
                        var DeliveryDays: Int = 0
                        var ReturnDays: Int = 0
                        
                        if (strDeliveryDays.toInt() != nil) {
                            DeliveryDays = strDeliveryDays.toInt()!
                        }
                        
                        if (strReturnDays.toInt() != nil) {
                            ReturnDays = strReturnDays.toInt()!
                        }
                        
                        if ReturnDays > 0 && DeliveryDays <= ReturnDays {
                            
                            orderItemReviewCell.btnCancel.setTitle("Return Order", for: .normal)
                            orderItemReviewCell.btnCancel.isEnabled = true
                            orderItemReviewCell.btnCancel.isHidden = false
                        } else {
                            
                            orderItemReviewCell.btnCancel.setTitle("Order Delivered", for: .normal)
                            orderItemReviewCell.btnCancel.isEnabled = false
                            orderItemReviewCell.btnCancel.isHidden = false
                        }
                    }
                        
                    else if orderItemStatus == 6 {
                        orderItemReviewCell.btnCancel.setTitle("Order Returned", for: .normal)
                        orderItemReviewCell.btnCancel.isEnabled = false
                        orderItemReviewCell.btnCancel.isHidden = false
                    }
                        
                    else if orderItemStatus == 7 {
                        orderItemReviewCell.btnCancel.setTitle("Order Refund", for: .normal)
                        orderItemReviewCell.btnCancel.isEnabled = false
                        orderItemReviewCell.btnCancel.isHidden = false
                    }
                        
                    else if orderItemStatus == 3 {
                        orderItemReviewCell.btnCancel.isEnabled = true
                        orderItemReviewCell.btnCancel.isHidden = true
                        
                        if orderItemReviewCell.btnCancel.isHidden == true
                        {
                            if Constants.DeviceType.IS_IPAD {
                                orderItemReviewCell.btnCancelWidthConstraints.constant = 0
                            }else {
                                orderItemReviewCell.btnCancelWidthConstraints.constant = 0
                            }
                        }
                    }
                    else {
                        orderItemReviewCell.btnCancel.setTitle("Cancel", for: .normal)
                        orderItemReviewCell.btnCancel.isEnabled = true
                        orderItemReviewCell.btnCancel.isHidden = false
                    }
                }
            }
            if strpaymentType == "PayuMoney" && orderItemStatus == 0 && orderPaymentStatus == "0"{
                orderItemReviewCell.btnCancel.setTitle("Cancelled Order", for: .normal)
                orderItemReviewCell.btnCancel.isEnabled = false
                orderItemReviewCell.btnCancel.isHidden = false
            }
            
            
            // Reiviews Visible/Unvisible
            if reviewDetailID != ""
            {
                if Constants.DeviceType.IS_IPAD {
                    orderItemReviewCell.btnAddYourReview.isHidden = true
                    orderItemReviewCell.btnReviewWidthConstraints.constant = 0
                } else {
                    orderItemReviewCell.btnAddYourReview.isHidden = true
                    orderItemReviewCell.btnReviewWidthConstraints.constant = 0
                }
            } else {
                if Constants.DeviceType.IS_IPAD {
                    orderItemReviewCell.btnAddYourReview.isHidden = false
                }else {
                    orderItemReviewCell.btnAddYourReview.isHidden = false
                }
            }
            
            if orderItemReviewCell.btnCancel.isHidden == true && orderItemReviewCell.btnAddYourReview.isHidden == true {
                if Constants.DeviceType.IS_IPAD {
                    orderItemReviewCell.viewForButton.isHidden = true
                    orderItemReviewCell.viewHeightConstraints.constant = 0
                } else {
                    orderItemReviewCell.viewForButton.isHidden = true
                    orderItemReviewCell.viewHeightConstraints.constant = 0
                }
            }
            
            if (DictItem["main_image"] as? [String:AnyObject]) != nil {
                
                let dictImg  = DictItem["main_image"] as! [String:AnyObject]
                
                var imgUrl = PIService.object_forKeyWithValidationForClass_String(dict: dictImg, key: "main_image")
                
                imgUrl = imgUrl.replacingOccurrences(of: " ", with: "%20")
                let strurl = URL(string: imgUrl)
                orderItemReviewCell.imgReview.kf.setImage(with: strurl)
            }
            
            strOrderDateTime = Convert_DateString_ForCell(strdate: strOrderDateTime)
            strPrize = BazarBitCurrency.setPriceSign(strPrice: strPrize)
            
            orderItemReviewCell.lblProductName.text = strProductName
            orderItemReviewCell.lblProductQuantity.text = strQuantity
            orderItemReviewCell.lblProductDate.text = strOrderDateTime
            orderItemReviewCell.lblProductAmount.text = strPrize
            
            orderItemReviewCell.viewProduct.applyShadowWithColor(.lightGray)
            
            return orderItemReviewCell
        }
        
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if Constants.DeviceType.IS_IPAD {
            return 200.0
        } else {
            return 130.0
        }
    }
    
    // MARK: - Action Method
    
    @IBAction func btnBack(_ sender: Any)  {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnYes(_ sender: Any)  {
        imgYes.image = #imageLiteral(resourceName: "radio")
        imgNo.image = #imageLiteral(resourceName: "radioLight")
        strRecommendedThisProduct = "1"
    }
    
    @IBAction func btnNo(_ sender: Any)  {
        imgNo.image = #imageLiteral(resourceName: "radio")
        imgYes.image = #imageLiteral(resourceName: "radioLight")
        strRecommendedThisProduct = "0"
    }
    
    @IBAction func btnSubmit(_ sender: Any)  {
        self.view.endEditing(true)
        if checkValidation() {
            callService_AddReview()
        }
    }
    
    @IBAction func btnCancel(_ sender: Any)  {
        popUpHide(popupView: viewAddYourReview)
        self.view.endEditing(true)
    }
    
    @IBAction func btnReturnOrderCOD_Cancel(_ sender: Any)  {
        popUpHide(popupView: viewPaymentType_1_COD)
        self.view.endEditing(true)
    }
    
    //---------
    //MARK: buttonReturnOrder From PayuMoney
    @IBAction func buttonCancel_ReturnOrder(_ sender: Any)  {
        popUpHide(popupView: viewReturnOrder_PayuMoney)
        self.view.endEditing(true)
    }
    
    @IBAction func btnSubmit_ReturnOrder(_ sender: Any)  {
        self.view.endEditing(true)
        if checkValidation_ReturnOrder() {
            popUpHide(popupView: viewReturnOrder_PayuMoney)
            callService_ReturnOrder_PayuMoney()
        }
    }
    
    //----------
    
    @IBAction func btnReturnOrder2_Cancel(_ sender: Any)  {
        popUpHide(popupView: viewPaymentType_2)
        self.view.endEditing(true)
    }
    
    @IBAction func btnSubmit_COD(_ sender: Any)  {
        self.view.endEditing(true)
        if checkValidation_COD() {
            popUpHide(popupView: viewPaymentType_1_COD)
            callService_ReturnOrder_COD()
        }
    }
    
    @IBAction func btnSubmit_2Type(_ sender: Any)  {
        self.view.endEditing(true)
        if checkValidation_2Type() {
            popUpHide(popupView: viewPaymentType_2)
            callService_ReturnOrder_2Type()
        }
    }
    
    @IBAction func btnAddYourReview(_ sender: AnyObject)  {
        
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tblOrderItemReview)
        let indexPath = self.tblOrderItemReview.indexPathForRow(at: buttonPosition)
        
        let dict: [String : AnyObject] = arrOrderItem[(indexPath?.row)!] as! [String: AnyObject]
        
        strReviewName = (UserDefaults.standard.value(forKey: BazarBit.USERNAME) as? String)!
        txtName.text = strReviewName
        txtEmail.text =  UserDefaults.standard.object(forKey: BazarBit.USEREMAILADDRESS) as? String
        txtTitle.text = ""
        self.txtDiscribtion.text = ""
        
        imgNo.image = #imageLiteral(resourceName: "radioLight")
        imgYes.image = #imageLiteral(resourceName: "radioLight")
        strRecommendedThisProduct = ""
        self.viewCosmos.rating = 0.0
        
        popUpShow(popupView: self.viewAddYourReview)
        
        strItemOrderId = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "orderitem_id")
        strProductID = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "product_id")
    }
    
    @IBAction func btnReviewCancel(_ sender: AnyObject)  {
        
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tblOrderItemReview)
        let indexPath = self.tblOrderItemReview.indexPathForRow(at: buttonPosition)
        
        let dict: [String: AnyObject] = arrOrderItem[(indexPath?.row)!] as! [String : AnyObject]
        
        print(dict)
        
        strUpdateItemOrderId = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "orderitem_id")
        strUpdateItemStatus = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "order_item_status")
        strProductID = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "product_id")
        
        let OrderItemCell: OrderItemReviewCell = tblOrderItemReview.cellForRow(at: indexPath!) as! OrderItemReviewCell
        
        let strTitle: String = (OrderItemCell.btnCancel.titleLabel?.text)!
        
        if strTitle == "Cancel" {
            
            let alertController: UIAlertController = UIAlertController(title: "", message: "Are you sure you want to cancel this order?", preferredStyle: .alert)
            
            let YesAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                
                let dict: [String: AnyObject] = self.arrSignleRecord[0] as! [String : AnyObject]
                
                let strPaymentType = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "order_payment_type")
                
                if strPaymentType != "1" {
                    
                    self.textFieldAccountHolderName.text = ""
                    self.textFieldBankName.text = ""
                    self.textFieldBranchName.text = ""
                    self.textFieldAccountNo.text = ""
                    self.textFieldIFSCCode.text = ""
                    self.textFieldReason.text = ""
                    
                    self.popUpShow(popupView: self.viewReturnOrder_PayuMoney)
                }else {
                    self.callService_UpdateItemStatus()
                }
            }
            let NoAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            }
            
            alertController.addAction(NoAction)
            alertController.addAction(YesAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        } else if strTitle == "Return Order" {
            
            let dict: [String: AnyObject] = arrSignleRecord[0] as! [String : AnyObject]
            
            let strPaymentType = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "order_payment_type")
            
            if strPaymentType == "1" {
                
                txtAccountHolderName.text = ""
                txtBankName.text = ""
                txtBranchName.text = ""
                txtAccountNo.text = ""
                txtIFSCCode.text = ""
                txtReason_COD.text = ""
                
                popUpShow(popupView: viewPaymentType_1_COD)
                
            } else if strPaymentType == "2" {
                self.txtReason_2Type.text = ""
                popUpShow(popupView: viewPaymentType_2)
            }
        }
    }
    
    // MARK: - UITextField Method    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string == "" {
            return true
        }
        
        if textField == txtAccountHolderName {
            if (textField.text?.characters.count)! >= 30 {
                return false
            }
            
        }  else if textField == txtBankName {
            if (textField.text?.characters.count)! >= 30 {
                return false
            }
            
            if (string.isNumber()) {
                return false
            }
            
        }  else if textField == txtBranchName {
            if (textField.text?.characters.count)! >= 30 {
                return false
            }
            
            if (string.isNumber()) {
                return false
            }
            
        } else if textField == txtAccountNo {
            if (textField.text?.characters.count)! >= 20 {
                return false
            }
            
            if !(string.isNumber()) {
                return false
            }
            
        } else if textField == txtIFSCCode {
            
            if (textField.text?.characters.count)! >= 15 {
                return false
            }
        }
        
        //RETURN ORDER
        if textField == textFieldAccountHolderName {
            if (textField.text?.characters.count)! >= 30 {
                return false
            }
            
        }  else if textField == textFieldBankName {
            if (textField.text?.characters.count)! >= 30 {
                return false
            }
            
            if (string.isNumber()) {
                return false
            }
            
        }  else if textField == textFieldBranchName {
            if (textField.text?.characters.count)! >= 30 {
                return false
            }
            
            if (string.isNumber()) {
                return false
            }
            
        } else if textField == textFieldAccountNo {
            if (textField.text?.characters.count)! >= 20 {
                return false
            }
            
            if !(string.isNumber()) {
                return false
            }
            
        } else if textField == textFieldIFSCCode {
            
            if (textField.text?.characters.count)! >= 15 {
                return false
            }
        }
        
        return true
    }
    
    // MARK:- UIPopup method
    func popUpShow(popupView :UIView) {
        
        dimView = UIView()
        dimView?.frame = self.view.frame
        dimView?.backgroundColor = UIColor.black
        dimView?.alpha = 0.5
        
        var x:Int = Int(UIScreen.main.bounds.width)
        var y:Int = Int(UIScreen.main.bounds.height)
        
        x =  (x - Int(popupView.frame.width)) / 2
        y = (y - Int(popupView.frame.height)) / 2
        
        popupView.frame = CGRect(x: CGFloat(x), y: CGFloat(y), w: popupView.frame.width, h: popupView.frame.height)
        popupView.layer.cornerRadius = 5.0
        popupView.layer.masksToBounds = true
        
        self.view.addSubview(dimView!)
        self.view.bringSubview(toFront: popupView)
        popupView.alpha = 1.0
        
    }
    
    func popUpHide(popupView :UIView) {
        dimView?.removeFromSuperview()
        popupView.alpha = 0.0
    }
    
    // MARK: - Service Method
    func callService_OrderDetail()  {
        
        let strurl: String = BazarBit.GetSingleOrderService()
        let url: URL = URL(string: strurl)!
        
        let strAuthToken: String = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as! String
        
        var headers: HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        headers["auth-token"] = strAuthToken
        
        var parameter: Parameters = [:]
        parameter["slug"] = strSlug
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: { (swiftyJson, responseDict) in
            
            print(responseDict)
            let dictResponse: [String: AnyObject] = responseDict
            
            let message: String = BazarBit.getmessageFromService(dict: dictResponse as [String : AnyObject])
            
            let status: String = PIService.object_forKeyWithValidationForClass_String(dict: dictResponse, key: "status")
            
            if status == "ok" {
                
                if (dictResponse["payload"] as? [AnyObject]) != nil {
                    
                    self.arrSignleRecord = dictResponse["payload"] as! [AnyObject]
                    
                    if self.arrSignleRecord.count > 0 {
                        
                        let dictOrder: [String: AnyObject] = self.arrSignleRecord[0] as! [String:AnyObject]
                        
                        
                        if (dictOrder["orderitems"] as? [AnyObject]) != nil {
                            self.arrOrderItem = dictOrder["orderitems"] as! [AnyObject]
                            
                            if self.arrOrderItem.count > 0 {
                                let dictItem: [String: AnyObject] = self.arrOrderItem[0] as! [String: AnyObject]
                                self.DisplayRecordOrderDetail(dictOrder: dictOrder, dictItem: dictItem)
                                
                                self.setHeightofTableView()
                                self.tblOrderItemReview.dataSource = self
                                self.tblOrderItemReview.delegate = self
                                self.tblOrderItemReview.reloadData()
                                
                            } else {
                                self.DisplayRecordOrderDetail(dictOrder: dictOrder, dictItem: [:])
                                self.arrOrderItem = []
                                self.setHeightofTableView()
                                self.tblOrderItemReview.dataSource = self
                                self.tblOrderItemReview.delegate = self
                                self.tblOrderItemReview.reloadData()
                            }
                        } else {
                            
                            self.DisplayRecordOrderDetail(dictOrder: dictOrder, dictItem: [:])
                            self.arrOrderItem = []
                            self.setHeightofTableView()
                            self.tblOrderItemReview.dataSource = self
                            self.tblOrderItemReview.delegate = self
                            self.tblOrderItemReview.reloadData()
                        }
                    }
                    
                } else {
                }
            } else {
                
                PIAlertUtils.displayAlertWithMessage(message)
            }
        })
    }
    
    func callService_AddReview()  {
        
        let strurl: String = BazarBit.GetAddReviewService()
        let url: URL = URL(string: strurl)!
        
        let strAuthToken: String = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as! String
        
        var headers: HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        headers["auth-token"] = strAuthToken
        
        var parameter: Parameters = [:]
        
        let strUserId: String = UserDefaults.standard.object(forKey: BazarBit.USERID) as! String
        let strRating: String = "\(viewCosmos.rating)"
        
        parameter["description"] = strReviewDescribtion
        parameter["email"] = self.strReviewEmail
        parameter["itemorder_id"] = strItemOrderId
        parameter["name"] = strReviewName
        parameter["product_id"] = strProductID
        parameter["rating"] = strRating
        parameter["title"] = strReviewTitle
        parameter["user_id"] = strUserId
        parameter["recommendthisproject"] = strRecommendedThisProduct
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: { (swiftyJson, responseDict) in
            
            print(responseDict)
            let dictResponse: [String: AnyObject] = responseDict
            
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            
            let status: String = PIService.object_forKeyWithValidationForClass_String(dict: dictResponse, key: "status")
            
            if status == "ok" {
                PIAlertUtils.displayAlertWithMessage(message)
                self.popUpHide(popupView: self.viewAddYourReview)
                self.callService_OrderDetail()
            }else {
                PIAlertUtils.displayAlertWithMessage(message)
            }
        })
    }
    
    func callService_UpdateItemStatus()  {
        
        let strurl: String = BazarBit.GetUpdateItemStatusService()
        
        let url: URL = URL(string: strurl)!
        
        let strAuthToken: String = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as! String
        
        var headers: HTTPHeaders = [:]
        
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        headers["auth-token"] = strAuthToken
        
        var parameter: Parameters = [:]
        parameter["itemorderid"] = strUpdateItemOrderId
        parameter["status"] = "5"
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: { (swiftyJson, responseDict) in
            
            print(responseDict)
            let dictResponse: [String: AnyObject] = responseDict
            
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            
            let status: String = PIService.object_forKeyWithValidationForClass_String(dict: dictResponse, key: "status")
            
            if status == "ok" {
                PIAlertUtils.displayAlertWithMessage(message)
                self.callService_OrderDetail()
                
            } else {
                PIAlertUtils.displayAlertWithMessage(message)
            }
        })
    }
    
    //MARK: SERVICE CALL FOR CANCEL ORDER PAYUMOVEY
    func callService_ReturnOrder_PayuMoney()  {
        
        let strurl: String = BazarBit.GetUpdateItemStatusService()
        let url: URL = URL(string: strurl)!
        
        let strAuthToken: String = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as! String
        
        var headers: HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        headers["auth-token"] = strAuthToken
        
        var parameter: Parameters = [:]
        parameter["itemorderid"] = strUpdateItemOrderId
        parameter["order_id"] = strSlug
        parameter["product_id"] = strProductID
        parameter["account_holder_name"] = strCancel_AccountHolderName
        parameter["bank_name"] = strCancel_BankName
        parameter["branch_name"] = strCancel_BranchName
        parameter["account_no"] = strCancel_AccountNo
        parameter["ifsc_code"] = strCancel_IFSCCode
        parameter["reason"] = strCancel_Reason_COD
        parameter["status"] = "5"
        parameter["operation_type"] = "online"
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: { (swiftyJson, responseDict) in
            
            let dictResponse: [String: AnyObject] = responseDict
            
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            let status: String = PIService.object_forKeyWithValidationForClass_String(dict: dictResponse, key: "status")
            
            if status == "ok" {
                PIAlertUtils.displayAlertWithMessage(message)
                self.callService_OrderDetail()
                
            } else {
                PIAlertUtils.displayAlertWithMessage(message)
            }
        })
    }
    
    func callService_ReturnOrder_COD()  {
        
        let strurl: String = BazarBit.GetReturnOrderService()
        let url: URL = URL(string: strurl)!
        
        let strAuthToken: String = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as! String
        
        var headers: HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        headers["auth-token"] = strAuthToken
        
        var parameter: Parameters = [:]
        parameter["itemorder_id"] = strUpdateItemOrderId
        parameter["order_id"] = strSlug
        parameter["product_id"] = strProductID
        parameter["account_holder_name"] = strAccountHolderName
        parameter["bank_name"] = strBankName
        parameter["branch_name"] = strBranchName
        parameter["account_no"] = strAccountNo
        parameter["ifsc_code"] = strIFSCCode
        parameter["reason"] = strReason_COD
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: { (swiftyJson, responseDict) in
            
            let dictResponse: [String: AnyObject] = responseDict
            
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            let status: String = PIService.object_forKeyWithValidationForClass_String(dict: dictResponse, key: "status")
            
            if status == "ok" {
                PIAlertUtils.displayAlertWithMessage(message)
                self.callService_OrderDetail()
                
            } else {
                PIAlertUtils.displayAlertWithMessage(message)
            }
        })
    }
    
    func callService_ReturnOrder_2Type()  {
        
        let strurl: String = BazarBit.GetReturnNetOrderService()
        
        let url: URL = URL(string: strurl)!
        
        let strAuthToken: String = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as! String
        
        var headers: HTTPHeaders = [:]
        
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        headers["auth-token"] = strAuthToken
        
        var parameter: Parameters = [:]
        parameter["itemorder_id"] = strUpdateItemOrderId
        parameter["order_id"] = strSlug
        parameter["product_id"] = strProductID
        parameter["reason"] = strReason_2Type
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: { (swiftyJson, responseDict) in
            
            print(responseDict)
            let dictResponse: [String: AnyObject] = responseDict
            
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            
            let status: String = PIService.object_forKeyWithValidationForClass_String(dict: dictResponse, key: "status")
            
            if status == "ok" {
                PIAlertUtils.displayAlertWithMessage(message)
                self.callService_OrderDetail()
            } else {
                PIAlertUtils.displayAlertWithMessage(message)
            }
        })
    }
    
    // MARK: - Helper Method
    
    func checkValidation_2Type() -> Bool {
        
        var isValid: Bool = true
        
        strReason_2Type = txtReason_2Type.text!
        strReason_2Type = strReason_2Type.trimmed()
        
        if PIValidation.isBlankString(str: strReason_2Type)  {
            isValid = false
            
            PIAlertUtils.displayAlertWithMessage("Please enter reason!")
            return isValid
        }
        return isValid
    }
    
    func checkValidation_COD() -> Bool {
        
        var isValid: Bool = true
        
        strAccountHolderName = txtAccountHolderName.text!
        strAccountHolderName = strAccountHolderName.trimmed()
        
        strBankName = txtBankName.text!
        strBankName = strBankName.trimmed()
        
        strBranchName = txtBranchName.text!
        strBranchName = strBranchName.trimmed()
        
        strAccountNo = txtAccountNo.text!
        strAccountNo = strAccountNo.trimmed()
        
        strIFSCCode = txtIFSCCode.text!
        strIFSCCode = strIFSCCode.trimmed()
        
        strReason_COD = txtReason_COD.text!
        strReason_COD = strReason_COD.trimmed()
        
        if PIValidation.isBlankString(str: strAccountHolderName)  {
            isValid = false
            
            PIAlertUtils.displayAlertWithMessage("Please enter holder name!")
            
            return isValid
            
        }   else if PIValidation.isBlankString(str: strBankName) {
            isValid = false
            
            PIAlertUtils.displayAlertWithMessage("Please enter bank name!")
            
            return isValid
            
        }  else if PIValidation.isBlankString(str: strBranchName) {
            isValid = false
            
            PIAlertUtils.displayAlertWithMessage("Please enter Branch Name!")
            
            return isValid
            
        } else if PIValidation.isBlankString(str: strAccountNo) {
            isValid = false
            
            PIAlertUtils.displayAlertWithMessage("Please enter Account No!")
            return isValid
            
        } else if PIValidation.isBlankString(str: strIFSCCode) {
            isValid = false
            
            PIAlertUtils.displayAlertWithMessage("Please enter IFSC code!")
            return isValid
            
        } else if PIValidation.isBlankString(str: strReason_COD) {
            isValid = false
            
            PIAlertUtils.displayAlertWithMessage("Please enter reason!")
            return isValid
        }
        
        return isValid
    }
    
    //RETURN ORDER PAYUMONEY
    func checkValidation_ReturnOrder() -> Bool {
        
        var isValid: Bool = true
        
        strCancel_AccountHolderName = textFieldAccountHolderName.text!
        strCancel_AccountHolderName = strCancel_AccountHolderName.trimmed()
        
        strCancel_BankName = textFieldBankName.text!
        strCancel_BankName = strCancel_BankName.trimmed()
        
        strCancel_BranchName = textFieldBranchName.text!
        strCancel_BranchName = strCancel_BranchName.trimmed()
        
        strCancel_AccountNo = textFieldAccountNo.text!
        strCancel_AccountNo = strCancel_AccountNo.trimmed()
        
        strCancel_IFSCCode = textFieldIFSCCode.text!
        strCancel_IFSCCode = strCancel_IFSCCode.trimmed()
        
        strCancel_Reason_COD = textFieldReason.text!
        strCancel_Reason_COD = strCancel_Reason_COD.trimmed()
        
        if PIValidation.isBlankString(str: strCancel_AccountHolderName)  {
            isValid = false
             
            PIAlertUtils.displayAlertWithMessage("Please enter holder name!")
            
            return isValid
            
        }   else if PIValidation.isBlankString(str: strCancel_BankName) {
            isValid = false
            
            PIAlertUtils.displayAlertWithMessage("Please enter bank name!")
            
            return isValid
            
        }  else if PIValidation.isBlankString(str: strCancel_BranchName) {
            isValid = false
            
            PIAlertUtils.displayAlertWithMessage("Please enter Branch Name!")
            
            return isValid
            
        } else if PIValidation.isBlankString(str: strCancel_AccountNo) {
            isValid = false
            
            PIAlertUtils.displayAlertWithMessage("Please enter Account No!")
            return isValid
            
        } else if PIValidation.isBlankString(str: strCancel_IFSCCode) {
            isValid = false
            
            PIAlertUtils.displayAlertWithMessage("Please enter IFSC code!")
            return isValid
            
        } else if PIValidation.isBlankString(str: strCancel_Reason_COD) {
            isValid = false
            
            PIAlertUtils.displayAlertWithMessage("Please enter reason!")
            return isValid
        }
        
        return isValid
    }
    
    func checkValidation() -> Bool {
        
        var isValid: Bool = true
        
        strReviewName = txtName.text!
        strReviewName = strReviewName.trimmed()
        
        strReviewEmail = txtEmail.text!
        strReviewEmail = strReviewEmail.trimmed()
        
        strReviewTitle = txtTitle.text!
        strReviewTitle = strReviewTitle.trimmed()
        
        strReviewDescribtion = txtDiscribtion.text!
        strReviewDescribtion = strReviewDescribtion.trimmed()
        
        if PIValidation.isBlankString(str: strReviewName)  {
            isValid = false
            
            PIAlertUtils.displayAlertWithMessage("Please enter Name!")
            
            return isValid
            
        }   else if PIValidation.isBlankString(str: strReviewEmail) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please enter email!")
            return isValid
        }
        else if PIValidation.isEmailString(str: strReviewEmail) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please Enter Valid Email!")
            return isValid
        }
            
        else if PIValidation.isBlankString(str: strReviewTitle) {
            isValid = false
            
            PIAlertUtils.displayAlertWithMessage("Please enter Title!")
            
            return isValid
            
        } else if PIValidation.isBlankString(str: strReviewDescribtion) {
            isValid = false
            
            PIAlertUtils.displayAlertWithMessage("Please enter Description!")
            return isValid
            
        } else if PIValidation.isBlankString(str: strRecommendedThisProduct) {
            isValid = false
            
            PIAlertUtils.displayAlertWithMessage("Please select recommend product!")
            return isValid
        }
        return isValid
    }
    
    func addPropertyPopupFunction()  {
        
        var popupWidth: Int = 0
        var popupHeight: Int = 0
        
        if Constants.DeviceType.IS_IPAD {
            popupWidth = 400
            popupHeight = 280
        } else {
            popupWidth = 270
            popupHeight = 420
        }
        
        var x:Int = Int(UIScreen.main.bounds.width)
        var y:Int = Int(UIScreen.main.bounds.height)
        
        x =  (x - Int(popupWidth)) / 2
        y = (y - Int(popupHeight)) / 2
        
        viewAddYourReview.frame = CGRect(x: CGFloat(x), y: CGFloat(y), w: CGFloat(popupWidth), h: CGFloat(popupHeight))
        self.viewAddYourReview.alpha = 0.0
        
        viewAddYourReview.layoutIfNeeded()
        self.view.addSubview(viewAddYourReview)
    }
    
    func addPropertyOFReturnOrderPopupFunction()  {
        
        self.view.addSubview(viewPaymentType_1_COD)
        self.viewPaymentType_1_COD.alpha = 0.0
        
        self.view.addSubview(viewPaymentType_2)
        self.viewPaymentType_2.alpha = 0.0
        
        self.view.addSubview(viewReturnOrder_PayuMoney)
        self.viewReturnOrder_PayuMoney.alpha = 0.0
        
        var popupWidth: Int = 0
        var popupHeight: Int = 0
        
        if Constants.DeviceType.IS_IPAD {
            popupWidth = 400
            popupHeight = 300
        } else if Constants.DeviceType.IS_IPHONE_5 {
            popupWidth = 270
            popupHeight = 430
        } else {
            popupWidth = 320
            popupHeight = 430
        }
        
        var x:Int = Int(UIScreen.main.bounds.width)
        var y:Int = Int(UIScreen.main.bounds.height)
        
        x =  (x - Int(popupWidth)) / 2
        y = (y - Int(popupHeight)) / 2
        
        viewPaymentType_1_COD.frame = CGRect(x: CGFloat(x), y: CGFloat(y), w: CGFloat(popupWidth), h: CGFloat(popupHeight))
        
        viewPaymentType_1_COD.layoutIfNeeded()
        
        if Constants.DeviceType.IS_IPAD {
            popupWidth = 400
            popupHeight = 280
        } else {
            popupWidth = 270
            popupHeight = 160
        }
        
        x =  (x - Int(popupWidth)) / 2
        y = (y - Int(popupHeight)) / 2
        
        viewPaymentType_2.frame = CGRect(x: CGFloat(x), y: CGFloat(y), w: CGFloat(popupWidth), h: CGFloat(popupHeight))
        
        viewPaymentType_2.layoutIfNeeded()
        
        if Constants.DeviceType.IS_IPAD {
            popupWidth = 400
            popupHeight = 300
        } else if Constants.DeviceType.IS_IPHONE_5 {
            popupWidth = 270
            popupHeight = 430
        } else {
            popupWidth = 320
            popupHeight = 430
        }
        
        x =  (x - Int(popupWidth)) / 2
        y = (y - Int(popupHeight)) / 2
        
        viewReturnOrder_PayuMoney.frame = CGRect(x: CGFloat(x), y: CGFloat(y), w: CGFloat(popupWidth), h: CGFloat(popupHeight))
        
        viewReturnOrder_PayuMoney.layoutIfNeeded()
    }
    
    func setBorderPropertiesForTextField()  {
        
        txtName.setLeftPaddingPoints(10.0)
        txtName.DynamictextBorder()
        txtName.DynamicPlaceholdertextColor()
        
        txtEmail.setLeftPaddingPoints(10.0)
        txtEmail.DynamictextBorder()
        txtEmail.DynamicPlaceholdertextColor()
        
        txtTitle.setLeftPaddingPoints(10.0)
        txtTitle.DynamictextBorder()
        txtTitle.DynamicPlaceholdertextColor()
        
        txtDiscribtion.setLeftPaddingPoints(10.0)
        txtDiscribtion.DynamictextBorder()
        txtDiscribtion.DynamicPlaceholdertextColor()
        
        txtAccountHolderName.setLeftPaddingPoints(10.0)
        txtAccountHolderName.DynamictextBorder()
        txtAccountHolderName.DynamicPlaceholdertextColor()
        
        txtBankName.setLeftPaddingPoints(10.0)
        txtBankName.DynamictextBorder()
        txtBankName.DynamicPlaceholdertextColor()
        
        txtBranchName.setLeftPaddingPoints(10.0)
        txtBranchName.DynamictextBorder()
        txtBranchName.DynamicPlaceholdertextColor()
        
        txtAccountNo.setLeftPaddingPoints(10.0)
        txtAccountNo.DynamictextBorder()
        txtAccountNo.DynamicPlaceholdertextColor()
        
        txtIFSCCode.setLeftPaddingPoints(10.0)
        txtIFSCCode.DynamictextBorder()
        txtIFSCCode.DynamicPlaceholdertextColor()
        
        txtReason_COD.setLeftPaddingPoints(10.0)
        txtReason_COD.DynamictextBorder()
        txtReason_COD.DynamicPlaceholdertextColor()
        
        txtReason_2Type.setLeftPaddingPoints(10.0)
        txtReason_2Type.DynamictextBorder()
        txtReason_2Type.DynamicPlaceholdertextColor()
        
        BtnSubmit.layer.cornerRadius = 20.0
        BtnSubmit.DynamicBtnTitleColor()
        
        btnSubmit_COD.layer.cornerRadius = 20.0
        btnSubmit_COD.DynamicBtnTitleColor()
        
        btnSubmit_2Type.layer.cornerRadius = 20.0
        btnSubmit_2Type.DynamicBtnTitleColor()
        
        //FOR RETURN ORDER
        textFieldAccountHolderName.setLeftPaddingPoints(10.0)
        textFieldAccountHolderName.DynamictextBorder()
        textFieldAccountHolderName.DynamicPlaceholdertextColor()
        
        textFieldBankName.setLeftPaddingPoints(10.0)
        textFieldBankName.DynamictextBorder()
        textFieldBankName.DynamicPlaceholdertextColor()
        
        textFieldBranchName.setLeftPaddingPoints(10.0)
        textFieldBranchName.DynamictextBorder()
        textFieldBranchName.DynamicPlaceholdertextColor()
        
        textFieldAccountNo.setLeftPaddingPoints(10.0)
        textFieldAccountNo.DynamictextBorder()
        textFieldAccountNo.DynamicPlaceholdertextColor()
        
        textFieldIFSCCode.setLeftPaddingPoints(10.0)
        textFieldIFSCCode.DynamictextBorder()
        textFieldIFSCCode.DynamicPlaceholdertextColor()
        
        textFieldReason.setLeftPaddingPoints(10.0)
        textFieldReason.DynamictextBorder()
        textFieldReason.DynamicPlaceholdertextColor()
        
        btnCancelOrder_PayUMoney.layer.cornerRadius = 20.0
        btnCancelOrder_PayUMoney.DynamicBtnTitleColor()
    }
    
    func setHeightofTableView() {
        if Constants.DeviceType.IS_IPAD  {
            tblOrderItemReviewHeight.constant = CGFloat(arrOrderItem.count * 200)
        } else {
            tblOrderItemReviewHeight.constant = CGFloat(arrOrderItem.count * 130)
        }
    }
    
    func setPropertiesOFView()  {
        viewOrderDetail.applyShadowWithColor(.lightGray)
        viewPaymentDetail.applyShadowWithColor(.lightGray)
        viewAddressDetail.applyShadowWithColor(.lightGray)
    }
    
    func DisplayRecordOrderDetail(dictOrder: [String: AnyObject], dictItem: [String: AnyObject])  {
        
        var HeightValue:  CGFloat = 0.0
        
        if Constants.DeviceType.IS_IPAD {
            HeightValue = 30
        } else {
            HeightValue = 21
        }
        
        if Constants.DeviceType.IS_IPAD {
            self.viewPaymentInfoHeight.constant = 220.0
        } else {
            self.viewPaymentInfoHeight.constant = 160.0
        }
        
        var strDate: String = PIService.object_forKeyWithValidationForClass_String(dict: dictOrder, key: "order_datetime")
        strDate = Convert_DateString_OneToAnotherFormat(strdate: strDate)
        lblOrderDate.text = strDate
        
        let strOrderID: String = PIService.object_forKeyWithValidationForClass_String(dict: dictOrder, key: "order_id")
        lblOrderID.text = strOrderID
        
        var strOrderTotal: String = PIService.object_forKeyWithValidationForClass_String(dict: dictOrder, key: "order_product_amount")
        
        var strBasePrice = String(format: "%.2f", (strOrderTotal.toFloat()!))
        strBasePrice = BazarBitCurrency.setPriceSign(strPrice: strBasePrice)
        lblOrderTotal.text = strBasePrice
        
        strOrderTotal = BazarBitCurrency.setPriceSign(strPrice: strOrderTotal)
        lblItemTotal.text = strOrderTotal
        
        var strDiscount: String = PIService.object_forKeyWithValidationForClass_String(dict: dictOrder, key: "order_total_discount")
        
        if strDiscount.toInt() != 0 {
            strDiscount = BazarBitCurrency.setPriceSign(strPrice: strDiscount)
            lblDiscount.text = strDiscount
            self.viewDiscountHeight.constant = HeightValue
        }else {
            self.viewDiscountHeight.constant = 0.0
            self.viewPaymentInfoHeight.constant = self.viewPaymentInfoHeight.constant - HeightValue
        }
        
        var strShippingCharge: String = PIService.object_forKeyWithValidationForClass_String(dict: dictOrder, key: "order_shipping_amount")
        
        if strShippingCharge.toInt() != 0 {
            strShippingCharge = BazarBitCurrency.setPriceSign(strPrice: strShippingCharge)
            lblShippingCharge.text = strShippingCharge
            self.viewShippingChargeHeight.constant = HeightValue
        }else {
            self.viewShippingChargeHeight.constant = 0.0
            self.viewPaymentInfoHeight.constant = self.viewPaymentInfoHeight.constant - HeightValue
        }
        
        var strCodeCharge: String = PIService.object_forKeyWithValidationForClass_String(dict: dictOrder, key: "order_cod_amount")
        
        if strCodeCharge.toInt() != 0 {
            strCodeCharge = BazarBitCurrency.setPriceSign(strPrice: strCodeCharge)
            lblCodeCharge.text = strCodeCharge
            self.viewPaymentInfoHeight.constant = self.viewPaymentInfoHeight.constant - HeightValue
        }else {
            self.viewCodeHeight.constant = 0.0
            self.viewPaymentInfoHeight.constant = self.viewPaymentInfoHeight.constant - HeightValue
        }
        
        var strTotal: String = PIService.object_forKeyWithValidationForClass_String(dict: dictOrder, key: "order_amount")
        strTotal = BazarBitCurrency.setPriceSign(strPrice: strTotal)
        
        lblTotal.text = strTotal
        
        let strPayment: String = PIService.object_forKeyWithValidationForClass_String(dict: dictOrder, key: "order_payment_type_display")
        lblPaymentInfo.text = "Payment Information (\(strPayment))"
        
        if dictItem.count > 0 {
            
            let strShppingFullname: String = PIService.object_forKeyWithValidationForClass_String(dict: dictItem, key: "fullname")
            lblShippingFullName.text = strShppingFullname
            
            let strShppingAddress: String = PIService.object_forKeyWithValidationForClass_String(dict: dictItem, key: "address")
            lblShippingAddress.text = strShppingAddress
            
            let strShppingCity: String = PIService.object_forKeyWithValidationForClass_String(dict: dictItem, key: "city")
            let strShppingPincode: String = PIService.object_forKeyWithValidationForClass_String(dict: dictItem, key: "pincode")
            lblShippingCity.text = "\(strShppingCity) - \(strShppingPincode)"
            
            let strShppingState: String = PIService.object_forKeyWithValidationForClass_String(dict: dictItem, key: "state")
            lblShippingState.text = strShppingState
            
            let strShppingPhoneNumber: String = PIService.object_forKeyWithValidationForClass_String(dict: dictItem, key: "phone_number")
            lblShippingPhoneNo.text = strShppingPhoneNumber
            
            
            let strBillingFullname: String = PIService.object_forKeyWithValidationForClass_String(dict: dictItem, key: "billing_fullname")
            lblBillingFullName.text = strBillingFullname
            
            let strBillingAddress: String = PIService.object_forKeyWithValidationForClass_String(dict: dictItem, key: "billing_address")
            lblBillingAddress.text = strBillingAddress
            
            let strBillingCity: String = PIService.object_forKeyWithValidationForClass_String(dict: dictItem, key: "billing_city")
            let strBillingPincode: String = PIService.object_forKeyWithValidationForClass_String(dict: dictItem, key: "billing_pincode")
            lblBillingCity.text = "\(strBillingCity) - \(strBillingPincode)"
            
            let strBillingState: String = PIService.object_forKeyWithValidationForClass_String(dict: dictItem, key: "billing_state")
            lblBillingState.text = strBillingState
            
            let strBillingPhoneNumber: String = PIService.object_forKeyWithValidationForClass_String(dict: dictItem, key: "billing_phonenumber")
            lblBillingPhoneNo.text = strBillingPhoneNumber
        }
    }
    
    func Convert_DateString_OneToAnotherFormat(strdate : String) -> String {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let now = dateformatter.date(from: strdate)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMM-yyyy"
        let strCurrentDate: String = formatter.string(from: now!)
        return strCurrentDate
    }
    
    func Convert_DateString_ForCell(strdate : String) -> String {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd"
        let now = dateformatter.date(from: strdate)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMM-yyyy"
        let strCurrentDate: String = formatter.string(from: now!)
        return strCurrentDate
    }
    
    func GetImageUrl(strurl: String) -> String {
        var strUrl = strurl
        strUrl = strUrl.replacingOccurrences(of: "[", with: "")
        strUrl = strUrl.replacingOccurrences(of: "\"", with: "")
        strUrl = strUrl.replacingOccurrences(of: "]", with: "")
        strUrl = strUrl.replacingOccurrences(of: " ", with: "%20")
        return strUrl
    }
}


class OrderItemReviewCell: UITableViewCell {
    
    @IBOutlet var imgReview: UIImageView!
    @IBOutlet var lblProductName: UILabel!
    @IBOutlet var lblTitleProductQuantity: UILabel!
    @IBOutlet var lblProductQuantity: UILabel!
    @IBOutlet var lblProductDate: UILabel!
    @IBOutlet var lblProductAmount: UILabel!
    
    @IBOutlet weak var viewForButton: UIView!
    @IBOutlet weak var viewWidthConstraints: NSLayoutConstraint!
    @IBOutlet weak var viewHeightConstraints: NSLayoutConstraint!
    @IBOutlet weak var btnCancelWidthConstraints: NSLayoutConstraint!
    
    @IBOutlet weak var btnReviewWidthConstraints: NSLayoutConstraint!
    
    @IBOutlet var viewProduct: UIView!
    @IBOutlet var btnCancel: UIButton!
    @IBOutlet var btnAddYourReview: UIButton!
    
    override func awakeFromNib() {
        self.lblProductName.setTextLabelDynamicTextColor()
        self.lblTitleProductQuantity.setTextLabelLightColor()
        self.lblProductQuantity.setTextLabelDynamicTextColor()
        self.lblProductDate.setTextLabelLightColor()
        self.lblProductAmount.setTextLabelDynamicTextColor()
        
        btnCancel.setTitleColor(UIColor(hexString: Constants.THEME_PRIMARY_COLOR), for: .normal)
        
        btnAddYourReview.setTitleColor(UIColor(hexString: Constants.THEME_PRIMARY_COLOR), for: .normal)
        
    }
    
}
