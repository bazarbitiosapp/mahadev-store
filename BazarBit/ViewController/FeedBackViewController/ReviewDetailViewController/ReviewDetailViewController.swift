//
//  ReviewDetailViewController.swift
//  BazarBit
//
//  Created by Parghi Infotech on 10/26/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import Alamofire
import Toaster
import Cosmos

class ReviewDetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate ,UIScrollViewDelegate {
    
    @IBOutlet var viewNavigation : UIView!
    
    @IBOutlet var tblReviewList: UITableView!
    @IBOutlet var tblReviewListHeight: NSLayoutConstraint!
    
    @IBOutlet var NoReviewView: UIView!
    @IBOutlet var lableNoReviewFound : UILabel!
    
    var offset1:Int = 0
    
    var arrReviewList: [AnyObject] = []
    // MARK: - UIView Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        callService_ReviewList()
        
        viewNavigation.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        lableNoReviewFound.setTextLabelThemeColor()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- UITableView Method
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tblReviewList {
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tblReviewList {
            return arrReviewList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblReviewList {
            
            let identifier : String = "ReviewListCell"
            
            let reviewListCell : ReviewListCell = tableView.dequeueReusableCell(withIdentifier: identifier) as! ReviewListCell
            
            let dict: [String: AnyObject] = arrReviewList[indexPath.row] as! [String: AnyObject]
            
            let strProductName: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "name")
            let strDescribtion: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "description")
            var strAddedOn: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "added_on")
            
            strAddedOn = Convert_DateString_OneToAnotherFormat(strdate: strAddedOn)
            reviewListCell.lblProductName.text = strProductName
            reviewListCell.lblDescribtion.text = strDescribtion
            reviewListCell.lblDate.text = strAddedOn
            
            let strRating: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "rating")
            let value = Double(strRating)
            reviewListCell.viewRating.rating = value!
            
            return reviewListCell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if Constants.DeviceType.IS_IPAD {
            return 100.0
        } else {
            return 85.0
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if ((tblReviewList.contentOffset.y + tblReviewList.frame.size.height) >= tblReviewList.contentSize.height)
        {
            self.offset1 = 10 + self.offset1
            callService_ReviewList()
        }
    }
    
    // MARK: - Action Method
    @IBAction func btnBack(_ sender: Any)  {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Service Method
    func callService_ReviewList()  {
        
        var isLoader : Bool = false
        if offset1 == 0 {
            isLoader = true
        }
        else {
            isLoader = false
        }
        
        let strurl: String = BazarBit.GetListReviewService()
        
        let url: URL = URL(string: strurl)!
        
        let strAuthToken: String = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as! String
        
        var headers: HTTPHeaders = [:]
        
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        headers["auth-token"] = strAuthToken
        
        var parameter: Parameters = [:]
        
        parameter["limit"] = 10
        parameter["offset"] = offset1
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: isLoader, completion: { (swiftyJson, responseDict) in
            
            let dictResponse: [String: AnyObject] = responseDict
            BazarBitLoader.stopLoader()
            
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            let status: String = PIService.object_forKeyWithValidationForClass_String(dict: dictResponse, key: "status")
            
            if status == "ok" {
                
                if (dictResponse["payload"] as? [String:AnyObject]) != nil {
                    
                    let payload : [String:AnyObject] = dictResponse["payload"] as! [String:AnyObject]
                    
                    if self.offset1 == 0
                    {
                        self.arrReviewList = []
                    }
                    
                    if (payload["reviews"] as? [AnyObject]) != nil {
                        
                        let dic = payload["reviews"] as! [AnyObject]
                        for i in dic
                        {
                            self.arrReviewList.append(i)
                        }
                        
                        //print(self.arrReviewList.count)
                        
                        if self.arrReviewList.count > 0 {
                            self.tblReviewList.isHidden = false
                            self.NoReviewView.isHidden = true
                            
                            if Constants.DeviceType.IS_IPAD {
                                self.tblReviewListHeight.constant = CGFloat(self.arrReviewList.count * 110)
                                
                            } else {
                                self.tblReviewListHeight.constant = CGFloat(self.arrReviewList.count * 85)
                            }
                        }else
                        {
                            self.tblReviewList.isHidden = true
                            self.NoReviewView.isHidden = false
                        }
                    }
                    
                    self.tblReviewList.dataSource = self
                    self.tblReviewList.delegate = self
                    self.tblReviewList.reloadData()
                    
                } else {
                    self.tblReviewList.isHidden = true
                    self.NoReviewView.isHidden = false
                    self.tblReviewList.reloadData()
                }
            } else {
                PIAlertUtils.displayAlertWithMessage(message)
            }
        })
    }
    
    func Convert_DateString_OneToAnotherFormat(strdate : String) -> String {
        
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd"
        let now = dateformatter.date(from: strdate)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMM-yyyy"
        let strCurrentDate: String = formatter.string(from: now!)
        return strCurrentDate
    }
}


class ReviewListCell: UITableViewCell {
    
    @IBOutlet var lblProductName: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblDescribtion: UILabel!
    
    @IBOutlet var viewRating: CosmosView!
    
    override func awakeFromNib() {
        lblProductName.setTextLabelDynamicTextColor()
        lblDate.setTextLabelLightColor()
        lblDescribtion.setTextLabelLightColor()
    }
}
