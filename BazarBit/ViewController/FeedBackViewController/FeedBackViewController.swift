//
//  FeedBackViewController.swift
//  BazarBit
//
//  Created by Sangani ajay on 10/31/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class FeedBackViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,UIScrollViewDelegate {
    
    @IBOutlet var viewNoData : UIView!
    @IBOutlet var lableNoFeedback: UILabel!
    
    @IBOutlet var viewNavigation : UIView!
    @IBOutlet var lableTitle: PICustomTitleLable!
    
    var aryFeedbackData : [AnyObject] = []
    
    @IBOutlet var tableViewFeedbackList : UITableView!
    var identifier : String = "feedbackCell"
    
    var offset1:Int=0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.isNavBarHidden = true
        viewNoData.isHidden = true
        self.tableViewFeedbackList.backgroundColor = UIColor(red: 236.0/255.0, green: 236.0/255.0, blue: 236.0/255.0, alpha: 1.0)
        
        viewNavigation.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        lableNoFeedback.setTextLabelThemeColor()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        self.ServiceCallForGetDeliveredOrder()
    }
    
    // MARK:- ACTION METHOD
    @IBAction func btnOpenDrawer(_ sender: Any) {
        self.evo_drawerController?.toggleLeftDrawerSide(animated: true, completion: nil)
    }
    
    //MARK: buttonAddFeedbackClicked
    @IBAction func buttonAddFeedbackClicked(_ sender: AnyObject) {
        
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewFeedbackList)
        let indexPath = self.tableViewFeedbackList.indexPathForRow(at: buttonPosition)
        
        let dictFeedBack: [String: AnyObject] = self.aryFeedbackData[(indexPath?.row)!] as! [String: AnyObject]
        
        let strOrderItemId: String = PIService.object_forKeyWithValidationForClass_String(dict: dictFeedBack, key: "orderitem_id")
        let strProductId: String = PIService.object_forKeyWithValidationForClass_String(dict: dictFeedBack, key: "product_id")
        
        let objAddFeedbackVC : AddFeedbackViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddFeedbackViewController") as! AddFeedbackViewController
        
        objAddFeedbackVC.strProductId = strProductId
        objAddFeedbackVC.strOrderItemId = strOrderItemId
        
        self.navigationController?.pushViewController(objAddFeedbackVC, animated: true)
    }
    
    //MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        let dict : [String: AnyObject] = aryFeedbackData[indexPath.row] as! [String : AnyObject]
        
        let feedbackID : String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "feedback_id")
        
        if feedbackID != "" {
            if IS_IPAD_DEVICE() {
                return 180.0
            }
            return 140.0
        }
        else
        {
            if IS_IPAD_DEVICE() {
                return 220.0
            }
            return 180.0
        }
    }
    
    //MARK: UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryFeedbackData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let feedbackCell : FeedbackTableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier) as! FeedbackTableViewCell
        
        let dict : [String: AnyObject] = aryFeedbackData[indexPath.row] as! [String : AnyObject]
        
        let feedbackID : String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "feedback_id")
        
        if feedbackID != "" {
            feedbackCell.viewButtonBGHeight.constant = 0.0
            feedbackCell.viewBorder.isHidden = true
            feedbackCell.buttonAddFeedback.isHidden = true
        }
        else
        {
            feedbackCell.viewButtonBGHeight.constant = 50.0
            feedbackCell.viewBorder.isHidden = false
            feedbackCell.buttonAddFeedback.isHidden = false
        }
        
        feedbackCell.setUpData(dictData: aryFeedbackData[indexPath.row] as! [String : AnyObject])
        
        return feedbackCell
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if ((tableViewFeedbackList.contentOffset.y + tableViewFeedbackList.frame.size.height) >= tableViewFeedbackList.contentSize.height)
        {
            self.offset1 = 10 + self.offset1
            ServiceCallForGetDeliveredOrder()
        }
    }
    
    //MARK: ServiceCallForGetDeliveredOrder
    func ServiceCallForGetDeliveredOrder() {
        
        var isLoader : Bool = false
        if offset1 == 0 {
            isLoader = true
        }
        else {
            isLoader = false
        }
        
        let strurl:String = BazarBit.GetDeliveredOrderService()
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        
        var parameter:Parameters = [:]
        let UserId = UserDefaults.standard.object(forKey: BazarBit.USERID)
        
        if UserId != nil {
            headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
            parameter["limit"] = 10
            parameter["offset"] = offset1
        }else {
            parameter["limit"] = 10
            parameter["offset"] = offset1
        }
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: isLoader, completion: {(swiftyJson, responseDict) in
            
            print(responseDict)
            var response : [String : AnyObject] = responseDict
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: response, key: "status")
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            
            if status == "ok"
            {
                if (response["payload"] as? [AnyObject] != nil)
                {
                    if self.offset1 == 0 {
                        self.aryFeedbackData = []
                    }
                    
                    let arrPayload = response["payload"] as! [AnyObject]
                    
                    for i in arrPayload
                    {
                        self.aryFeedbackData.append(i)
                    }
                    
                    self.tableViewFeedbackList.delegate = self
                    self.tableViewFeedbackList.dataSource = self
                    
                    if self.aryFeedbackData.count > 0 {
                        self.viewNoData.isHidden = true
                        self.tableViewFeedbackList.reloadData()
                    }
                    else {
                        self.viewNoData.isHidden = false
                        self.tableViewFeedbackList.reloadData()
                    }
                    self.tableViewFeedbackList.reloadData()
                }
                else
                {
                    PIAlertUtils.displayAlertWithMessage(message)
                    self.tableViewFeedbackList.reloadData()
                }
            }
        })
    }
}
