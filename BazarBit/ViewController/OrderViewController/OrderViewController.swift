//
//  OrderViewController.swift
//  BazarBit
//
//  Created by Parghi Infotech on 10/27/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import Alamofire
import Toaster

class OrderViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var viewNavigation : UIView!
    
    var isFromMenu : Bool = false
    @IBOutlet var buttonBack : UIButton!
    @IBOutlet var buttonMenu : UIButton!
    
    //HEaderView
    //@IBOutlet var headerView : UIView!
    
    @IBOutlet var tblOrderList: UITableView!
    @IBOutlet var NoOrderView: UIView!
    @IBOutlet var viewFilter: UIView!
    
    @IBOutlet var txtFormDate: UITextField!
    @IBOutlet var txtToDate: UITextField!
    
    @IBOutlet var viewDatePickerFrom: UIView!
    @IBOutlet var DatePickerFrom: UIDatePicker!
    
    @IBOutlet var viewDatePickerTo: UIView!
    @IBOutlet var DatePickerTo: UIDatePicker!
    
    @IBOutlet var btnApply : UIButton!
    
    var dimView:UIView?
    
    var arrOrderList : [AnyObject] = []
    var strDate: String = ""
    var parameterOrderList: Parameters = [:]
    
    var strFromDate: String = ""
    var strToDate: String = ""
    
    var offset1:Int=0
    
    // MARK: - UIView Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addPropertyPopupFunction()
        
        // Do any additional setup after loading the view.
        
        txtToDate.setTextFieldDynamicColorForFilter()
        txtFormDate.setTextFieldDynamicColorForFilter()
        
        txtFormDate.setLeftPaddingPoints(10.0)
        txtToDate.setLeftPaddingPoints(10.0)
        
        btnApply.setButtonTextThemeColor()
        
        if isFromMenu == true {
            buttonMenu.isHidden = false
            buttonBack.isHidden = true
        }else {
            buttonMenu.isHidden = true
            buttonBack.isHidden = false
        }
        
        viewNavigation.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        
        self.tblOrderList.tableHeaderView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: tblOrderList.bounds.size.width, height: 0.01))
        
        tblOrderList.tableFooterView = UIView(frame: CGRect.zero)
        tblOrderList.sectionFooterHeight = 0.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        callService_OrderList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: buttonOpenDrawer
    @IBAction func btnOpenDrawer(_ sender: Any) {
        self.evo_drawerController?.toggleLeftDrawerSide(animated: true, completion: nil)
    }
    
    // MARK:- UITableView Method
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tblOrderList {
            return arrOrderList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == tblOrderList {
            return 50.0
        }
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        view.backgroundColor = UIColor.white
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section:
        Int) -> UIView? {
        
        let headerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.size.width, height: 50))
        
        let spaceView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.size.width, height: 10))
        spaceView.backgroundColor = UIColor(red: 238.0/255.0, green: 238.0/255.0, blue: 238.0/255.0, alpha: 1.0)
        
        headerView.addSubview(spaceView)
        
        let dictOrder: [String: AnyObject] = arrOrderList[section] as! [String: AnyObject]
        let strOrderId: String = PIService.object_forKeyWithValidationForClass_String(dict: dictOrder, key: "order_id")
        
        let label: UILabel = UILabel(frame: CGRect(x: 14, y: 10, width: tableView.size.width - 28, height: 40))
        label.setTextLabelDynamicTextColor()
        label.font = UIFont.appFont_Light_WithSize(14.0)
        label.text = "Order Id :- \(strOrderId)"
        headerView.addSubview(label)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tblOrderList {
            let dict: [String: AnyObject] = arrOrderList[section] as! [String: AnyObject]
            
            if (dict["ordereditems"] as? [AnyObject]) != nil {
                let arrOrderItem: [AnyObject] = dict["ordereditems"] as! [AnyObject]
                return arrOrderItem.count
            } else {
                return 0
            }
        }        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblOrderList {
            
            let orderListCell : OrderListCell = tableView.dequeueReusableCell(withIdentifier: "OrderListCell") as! OrderListCell
            
            let dictOrder: [String: AnyObject] = arrOrderList[indexPath.section] as! [String: AnyObject]
            print(dictOrder)
            let arrOrderItem: [AnyObject] = dictOrder["ordereditems"] as! [AnyObject]
            print(arrOrderItem)
            let DictItem: [String: AnyObject] = arrOrderItem[indexPath.row] as! [String: AnyObject]
            print(DictItem)
            
            let orderPaymentStatus : String = PIService.object_forKeyWithValidationForClass_String(dict: dictOrder, key: "order_payment_status")
            let strProductName: String = PIService.object_forKeyWithValidationForClass_String(dict: DictItem, key: "name")
            let strOrderId: String = PIService.object_forKeyWithValidationForClass_String(dict: dictOrder, key: "order_id")
            var strOrderDateTime: String = PIService.object_forKeyWithValidationForClass_String(dict: dictOrder, key: "order_datetime")
            var strPrize: String = PIService.object_forKeyWithValidationForClass_String(dict: DictItem, key: "price")
            let strPaymentStatus: String = PIService.object_forKeyWithValidationForClass_String(dict: DictItem, key: "order_item_payment_status_display")
            let strpaymentType: String = PIService.object_forKeyWithValidationForClass_String(dict: dictOrder, key: "order_payment_type_display")
            print(strpaymentType)
            
            if let key = DictItem["main_image"] {
                if (key as? [String : AnyObject]) != nil
                {
                    print(key)
                    var imgUrl:String = key["main_image"] as! String
                    imgUrl = imgUrl.replacingOccurrences(of: " ", with: "%20")
                    let url1 = URL(string: imgUrl)
                    orderListCell.imgOrder.kf.setImage(with: url1)
                }
            }
            
            strOrderDateTime = Convert_DateString_OneToAnotherFormat(strdate: strOrderDateTime)
            strPrize = BazarBitCurrency.setPriceSign(strPrice: strPrize)
            
            orderListCell.lblProductName.text = strProductName
           
            orderListCell.lblOrderDate.text = strOrderDateTime
            orderListCell.lblProductPrize.text = strPrize
            orderListCell.lblPaymentStatus.text = strPaymentStatus
            
            orderListCell.lblProductName.setTextLabelLightColor()
            
            orderListCell.lblOrderDate.setTextLabelLightColor()
            orderListCell.lblProductPrize.setTextLabelDynamicTextColor()
            
            let strOrderItemStatus: String = PIService.object_forKeyWithValidationForClass_String(dict: DictItem, key: "order_item_status")
            
            print(strPaymentStatus)
            print(strOrderItemStatus)
            
            if strOrderItemStatus == "0" {
                orderListCell.lblPaymentStatus.text = "Pending Order"
                orderListCell.lblPaymentStatus.backgroundColor = UIColor(hexString: "#FF7F50")
                
            } else if strOrderItemStatus == "1" {
                
                orderListCell.lblPaymentStatus.text = "Confirm Order"
                orderListCell.lblPaymentStatus.backgroundColor = UIColor(hexString: "#28b62c")
                
            } else if strOrderItemStatus == "2" {
                
                orderListCell.lblPaymentStatus.text = "In Process"
                orderListCell.lblPaymentStatus.backgroundColor = UIColor(hexString: "#d4af37")
                
            } else if strOrderItemStatus == "3" {
                
                orderListCell.lblPaymentStatus.text = "Out for Delivery"
                orderListCell.lblPaymentStatus.backgroundColor = UIColor(hexString: "#88b04b")
                
                
            } else if strOrderItemStatus == "4" {
                
                orderListCell.lblPaymentStatus.text = "Delivered"
                orderListCell.lblPaymentStatus.backgroundColor = UIColor(hexString: "#076f30")
                
            } else if strOrderItemStatus == "5" {
                
                orderListCell.lblPaymentStatus.text = "Cancel Order"
                orderListCell.lblPaymentStatus.backgroundColor = UIColor(hexString: "#FF0000")
                
            } else if strOrderItemStatus == "6" {
                
                orderListCell.lblPaymentStatus.text = "Return Order"
                orderListCell.lblPaymentStatus.backgroundColor = UIColor(hexString: "#158cba")
                
            } else if strOrderItemStatus == "7" {
                
                orderListCell.lblPaymentStatus.text = "Refund Order"
                orderListCell.lblPaymentStatus.backgroundColor = UIColor(hexString: "#72c29b")
            }
            if strpaymentType == "PayuMoney" && strOrderItemStatus == "0" && orderPaymentStatus == "0"{
                orderListCell.lblPaymentStatus.text = "Cancel Order"
                orderListCell.lblPaymentStatus.backgroundColor = UIColor(hexString: "#FF0000")
            }
            return orderListCell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if Constants.DeviceType.IS_IPAD {
            return 150.0
        } else {
            return 120.0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let orderDetailVc: OrderDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailViewController") as! OrderDetailViewController
        
        let dictOrder: [String: AnyObject] = arrOrderList[indexPath.section] as! [String: AnyObject]
        
       // print(dictOrder["order_id"] ?? "nil")
        
        let strSlug: String = PIService.object_forKeyWithValidationForClass_String(dict: dictOrder, key: "order_id")
        orderDetailVc.strSlug = strSlug
        self.navigationController?.pushViewController(orderDetailVc, animated: true)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
       
        if ((tblOrderList.contentOffset.y + tblOrderList.frame.size.height) >= tblOrderList.contentSize.height)
        {
            print(self.offset1)
            self.offset1 = 10 + self.offset1
            callService_OrderList()
        }
    }
    
    // MARK: - Action Method
    @IBAction func btnBack(_ sender: Any)  {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - FromDate Show-Hide Method
    @IBAction func btnFromDate(_ sender: Any)  {
        DatePickerFrom.maximumDate = Date()
        self.view.bringSubview(toFront: viewDatePickerFrom)
        viewDatePickerFrom.isHidden = false
        viewDatePickerTo.isHidden = true
        hidePickerPopUp(viewPicker: viewDatePickerTo)
        viewDatePickerFrom.alpha = 1
        showPickerPopUp(viewPicker: viewDatePickerFrom)
    }
    
    @IBAction func btnFromDateDone(_ sender: Any)  {
        hidePickerPopUp(viewPicker: viewDatePickerFrom)
        let strFromDate: String = convertFromToDate(date: DatePickerFrom.date)
        txtFormDate.text = strFromDate
    }
    
    @IBAction func btnFromDateCancel(_ sender: Any)  {
        hidePickerPopUp(viewPicker: viewDatePickerFrom)
    }
    
    // MARK: - ToDate Show-Hide Method
    @IBAction func btnToDate(_ sender: Any)  {
        
        if txtFormDate.text != "" {
            
            DatePickerTo.minimumDate = DatePickerFrom.date
            DatePickerTo.maximumDate = Date()
            
            self.view.bringSubview(toFront: viewDatePickerTo)
            viewDatePickerFrom.isHidden = true
            viewDatePickerTo.isHidden = false
            hidePickerPopUp(viewPicker: viewDatePickerFrom)
            viewDatePickerTo.alpha = 1
            showPickerPopUp(viewPicker: viewDatePickerTo)
        } else {
            PIAlertUtils.displayAlertWithMessage("Please Select FromDate!")
        }
    }
    
    @IBAction func btnToDateDone(_ sender: Any)  {
        hidePickerPopUp(viewPicker: viewDatePickerTo)
        let strToDate: String = convertFromToDate(date: DatePickerTo.date)
        txtToDate.text = strToDate
        
        DatePickerFrom.maximumDate = DatePickerTo.date
    }
    
    @IBAction func btnToDateCancel(_ sender: Any)  {
        hidePickerPopUp(viewPicker: viewDatePickerTo)
    }
    
    // MARK: - Show-Hide Filter Method    
    @IBAction func btnFilter(_ sender:AnyObject) {
        popUpShow(popupView: viewFilter)
    }
    
    @IBAction func btnFilterApply(_ sender:AnyObject) {
        
        popUpHide(popupView: viewFilter)
        self.view.endEditing(true)
        
        parameterOrderList["fromdate"] = txtFormDate.text
        parameterOrderList["todate"] = txtToDate.text
    
        self.offset1 = 0
        callService_OrderList()
    }
    
    @IBAction func btnFilterCancel(_ sender:AnyObject) {
        popUpHide(popupView: viewFilter)
        self.view.endEditing(true)
        viewDatePickerTo.alpha = 0
        viewDatePickerFrom.alpha = 0
        hidePickerPopUp(viewPicker: viewDatePickerTo)
        hidePickerPopUp(viewPicker: viewDatePickerFrom)
   
        txtFormDate.text = ""
        txtToDate.text = ""
        
        parameterOrderList["fromdate"] = txtFormDate.text
        parameterOrderList["todate"] = txtToDate.text
        self.offset1 = 0
        
        self.callService_OrderList()
    }
    
    // MARK:- UIPopup method
    func popUpShow(popupView :UIView) {
        
        dimView = UIView()
        dimView?.frame = self.view.frame
        dimView?.backgroundColor = UIColor.black
        dimView?.alpha = 0.5
        
        var x:Int = Int(UIScreen.main.bounds.width)
        var y:Int = Int(UIScreen.main.bounds.height)
        
        x = 0
        y = 0
        
        popupView.frame = CGRect(x: CGFloat(x), y: CGFloat(y), w: popupView.frame.width, h: popupView.frame.height)
        popupView.layer.cornerRadius = 0.0
        popupView.layer.masksToBounds = true
        
        self.view.addSubview(dimView!)
        self.view.bringSubview(toFront: popupView)
        popupView.alpha = 1.0
    }
    
    func popUpHide(popupView :UIView) {
        dimView?.removeFromSuperview()
        popupView.alpha = 0.0
    }
    
    // MARK:- Show Picker PopUp method Bottom-Up
    func showPickerPopUp(viewPicker: UIView) {
        
        viewPicker.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, w: UIScreen.main.bounds.width, h: viewPicker.frame.size.height)
        
        UIView.animate(withDuration: 0.5, animations:{
            viewPicker.frame = CGRect(x: 0, y: UIScreen.main.bounds.height - viewPicker.frame.size.height , w: UIScreen.main.bounds.width, h: viewPicker.frame.size.height)
        })
    }
    
    func hidePickerPopUp(viewPicker: UIView)  {
        
        viewPicker.frame = CGRect(x: 0, y: UIScreen.main.bounds.height - viewPicker.frame.size.height , w: UIScreen.main.bounds.width, h: viewPicker.frame.size.height)
        
        UIView.animate(withDuration: 0.5, animations:{
            viewPicker.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, w: UIScreen.main.bounds.width, h: viewPicker.frame.size.height)
        })
    }
    
    // MARK: - Service Method
    func callService_OrderList()  {
        
        var isLoader : Bool = false
        if offset1 == 0 {
            isLoader = true
        }
        else {
            isLoader = false
        }
        
        let strurl: String = BazarBit.GetOrderListService()
        
        let url: URL = URL(string: strurl)!
   
        var headers: HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        
        let UserId = UserDefaults.standard.object(forKey: BazarBit.USERID)
        
        if UserId != nil {
            headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
            parameterOrderList["limit"] = 10
            parameterOrderList["offset"] = offset1
            print(parameterOrderList)
        }
        else
        {
            parameterOrderList["limit"] = 10
            parameterOrderList["offset"] = offset1
            print(parameterOrderList)
        }
        
        PIService.serviceCall(url: url, method: .post, parameter: parameterOrderList, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: isLoader, completion: { (swiftyJson, responseDict) in
            
            print(responseDict)
            let dictResponse: [String: AnyObject] = responseDict
  
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            let status: String = PIService.object_forKeyWithValidationForClass_String(dict: dictResponse, key: "status")
            
            if status == "ok" {
                
                if self.offset1 == 0 {
                    self.arrOrderList = []
                }
                
                if (dictResponse["payload"] as? [String:AnyObject]) != nil {
                    
                    let payload : [String:AnyObject] = dictResponse["payload"] as! [String:AnyObject]
                    
                    if (payload["orders"] as? [AnyObject]) != nil {
                        
                        let dic = payload["orders"] as! [AnyObject]
                        
                        for i in dic
                        {
                            self.arrOrderList.append(i)
                        }
                        
                        print(self.arrOrderList.count)
                        
                        if self.arrOrderList.count > 0 {
                            self.tblOrderList.isHidden = false
                            self.NoOrderView.isHidden = true
                            
                        } else {
                            self.tblOrderList.isHidden = true
                            self.NoOrderView.isHidden = false
                        }
                        
                        self.tblOrderList.dataSource = self
                        self.tblOrderList.delegate = self
                        self.tblOrderList.reloadData()
                    }

                } else {
                    self.tblOrderList.isHidden = true
                    self.NoOrderView.isHidden = false
                }
            } else {
                
                PIAlertUtils.displayAlertWithMessage(message)
            }
        })
    }
    
    // MARK: - Helper Method
    
    func convertFromToDate(date : Date) -> String {
        let dateFormate = DateFormatter()
        dateFormate.dateFormat = "yyyy-MM-dd"
        let newDate = dateFormate.string(from: date)
        return newDate
    }
    
    func addPropertyPopupFunction()  {
        
        self.view.addSubview(viewFilter)
        self.viewFilter.alpha = 0.0
        
        let popupWidth: Int = Int(UIScreen.main.bounds.width)
        var popupHeight: Int = 0
        
        if Constants.DeviceType.IS_IPAD {
            popupHeight = 160
            
        } else {
            popupHeight = 130
        }
        
        viewFilter.frame = CGRect(x: 0, y: 0, w: CGFloat(popupWidth), h: CGFloat(popupHeight))
        
        viewFilter.layoutIfNeeded()
        
        viewDatePickerFrom.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, w: UIScreen.main.bounds.width, h: viewDatePickerFrom.frame.size.height)
        self.view.addSubview(viewDatePickerFrom)
        viewDatePickerFrom.layoutIfNeeded()
        
        viewDatePickerTo.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, w: UIScreen.main.bounds.width, h: viewDatePickerTo.frame.size.height)
        self.view.addSubview(viewDatePickerTo)
        viewDatePickerTo.layoutIfNeeded()
        
    }
    
    func Convert_DateString_OneToAnotherFormat(strdate : String) -> String {
        
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let now = dateformatter.date(from: strdate)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMM-yyyy"
        let strCurrentDate: String = formatter.string(from: now!)
        return strCurrentDate
    }
    
    func GetImageUrl(strurl: String) -> String {
        
        var strUrl = strurl
        
        strUrl = strUrl.replacingOccurrences(of: "[", with: "")
        strUrl = strUrl.replacingOccurrences(of: "\"", with: "")
        strUrl = strUrl.replacingOccurrences(of: "]", with: "")
        strUrl = strUrl.replacingOccurrences(of: " ", with: "%20")        
        return strUrl
    }
    
    // MARK: - Validation Method
    
    func checkValidation() -> Bool {
        
        var isValid: Bool = true
        
        strFromDate = txtFormDate.text!
        strFromDate = strFromDate.trimmed()
        
        strToDate = txtToDate.text!
        strToDate = strToDate.trimmed()
        
        if PIValidation.isBlankString(str: strFromDate)  {
            isValid = false
            
            PIAlertUtils.displayAlertWithMessage("Please select FromDate!")
            
            return isValid
            
        }  else if PIValidation.isBlankString(str: strToDate) {
            isValid = false
            
            PIAlertUtils.displayAlertWithMessage("Please select ToDate!")
            
            return isValid
        }
        return isValid
    }
}

class OrderListCell: UITableViewCell {
    
    @IBOutlet var lblProductName: UILabel!
    @IBOutlet var lblOrderDate: UILabel!
    @IBOutlet var lblProductPrize: UILabel!
    @IBOutlet var lblPaymentStatus: UILabel!
    @IBOutlet var imgOrder: UIImageView!
    
    override func awakeFromNib() {
        self.selectionStyle = .none
    }
}
