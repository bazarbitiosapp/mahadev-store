//
//  LinkedInViewController.swift
//  BazarBit
//
//  Created by Sangani ajay on 1/31/18.
//  Copyright © 2018 Parghi Infotech. All rights reserved.
//

import UIKit
import Alamofire

class LinkedInViewController: UIViewController, UIWebViewDelegate {
    
    // MARK: IBOutlet Properties
    @IBOutlet weak var webView: UIWebView!
    
    var socialParameter: Parameters = [:]
    var linkedInDetail : [String: AnyObject] = [:]
    var socialType: String = ""
    var socialId: String = ""
    var socialName: String = ""
    
    // MARK: Constants
    let linkedInKey = BazarBit.CLIENT_ID
    let linkedInSecret = BazarBit.CLIENT_SECRET
    let authorizationEndPoint = "https://www.linkedin.com/uas/oauth2/authorization"
    let accessTokenEndPoint = "https://www.linkedin.com/uas/oauth2/accessToken"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        webView.delegate = self
        startAuthorization()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK:- UIACTION METHODS
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: UIWebViewDelegate Functions
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool
    {
        let url = request.url!
        if url.host == "localhost" {
            if url.absoluteString.range(of: "code") != nil {
                
                let urlParts = url.absoluteString.components(separatedBy: "?")
                let code = urlParts[1].components(separatedBy: "=")[1]
                requestForAccessToken(authorizationCode: code)
            }
        }
        return true
    }
    
    // MARK: Custom Functions
    func startAuthorization() {
        // Specify the response type which should always be "code".
        let responseType = "code"
        
        
        let redirectURL = "http://localhost/bazzarbitFront".addingPercentEncoding( withAllowedCharacters: .alphanumerics)
        
        // Create a random string based on the time intervale (it will be in the form linkedin12345679).
        let state = "linkedin\(Int(NSDate().timeIntervalSince1970))"
        
        // Set preferred scope.
        let scope = "r_emailaddress"
        
        // Create the authorization URL string.
        var authorizationURL = "\(authorizationEndPoint)?"
        authorizationURL += "response_type=\(responseType)&"
        authorizationURL += "client_id=\(linkedInKey)&"
        authorizationURL += "redirect_uri=\(String(describing: redirectURL!))&"
        authorizationURL += "state=\(state)&"
        authorizationURL += "scope=\(scope)"
        
        print(authorizationURL)
        
        // Create a URL request and load it in the web view.
        let request = URLRequest(url: URL(string: authorizationURL)!)
        webView.loadRequest(request)
    }
    
    
    func requestForAccessToken(authorizationCode: String) {
        let grantType = "authorization_code"
        
        let redirectURL = "http://localhost/bazzarbitFront".addingPercentEncoding( withAllowedCharacters: .alphanumerics)
        
        // Set the POST parameters.
        var postParams = "grant_type=\(grantType)&"
        postParams += "code=\(authorizationCode)&"
        postParams += "redirect_uri=\(String(describing: redirectURL!))&"
        postParams += "client_id=\(linkedInKey)&"
        postParams += "client_secret=\(linkedInSecret)"
        
        // Convert the POST parameters into a NSData object.
        let postData = postParams.data(using: String.Encoding.utf8)
        print(postData ?? "nil")
        
        
        // Initialize a mutable URL request object using the access token endpoint URL string.
        var request = URLRequest(url: URL(string: accessTokenEndPoint)!)
        
        // Indicate that we're about to make a POST request.
        request.httpMethod = "POST"
        
        // Set the HTTP body using the postData object created above.
        request.httpBody = postData
        
        // Add the required HTTP header field.
        request.addValue("application/x-www-form-urlencoded;", forHTTPHeaderField: "Content-Type")
        
        print(request)
        
        // Initialize a NSURLSession object.
        let session = URLSession(configuration: URLSessionConfiguration.default)
        
        // Make the request.
        let task: URLSessionDataTask = session.dataTask(with: request) { (data, response, error) -> Void in
            // Get the HTTP status code of the request.
            let statusCode = (response as! HTTPURLResponse).statusCode
            
            print(response ?? "nil")
            print(statusCode)
            
            if statusCode == 200 {
                // Convert the received JSON data into a dictionary.
                do {
                    let dataDictionary = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                    
                    let responseDict : [String : AnyObject] = dataDictionary as! [String : AnyObject]
                    print(responseDict)
                    
                    let accessToken : String = PIService.object_forKeyWithValidationForClass_String(dict: responseDict, key: "access_token")
                    
                    UserDefaults.standard.set(accessToken, forKey: "LIAccessToken")
                    UserDefaults.standard.synchronize()
                    
                    self.getProfileInfo()
                    
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.dismiss(animated: true, completion: nil)
                    })
                }
                catch {
                    print("Could not convert JSON data into a dictionary.")
                }
            }
        }
        task.resume()
    }
    
    func getProfileInfo() {
        if let accessToken : String = UserDefaults.standard.object(forKey: "LIAccessToken") as? String {
            // Specify the URL string that we'll get the profile info from.
            
            print(accessToken)
            
            let targetURLString = "https://api.linkedin.com/v1/people/~:(id,num-connections,first-name,last-name,picture-url,email-address)?oauth2_access_token=\(accessToken)&format=json"
            
            
            // Initialize a mutable URL request object.
            var request = URLRequest(url: URL(string: targetURLString)!)
            
            // Indicate that this is a GET request.
            request.httpMethod = "GET"
            
            // Initialize a NSURLSession object.
            let session = URLSession(configuration: URLSessionConfiguration.default)
            
            // Make the request.
            let task: URLSessionDataTask = session.dataTask(with: request) { (data, response, error) -> Void in
                // Get the HTTP status code of the request.
                
                print(response ?? "nil")
                
                let statusCode = (response as! HTTPURLResponse).statusCode
                
                if statusCode == 200 {
                    // Convert the received JSON data into a dictionary.
                    do {
                        let dataDictionary = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                        
                        print(dataDictionary)
                        
                        let responseDict : [String : AnyObject] = dataDictionary as! [String : AnyObject]
                        print(responseDict)
                        
                        self.linkedInDetail = responseDict
                        
                        self.socialId = PIService.object_forKeyWithValidationForClass_String(dict: responseDict, key: "id")
                        self.socialType = "linkedin"
                        
                        let fName : String = PIService.object_forKeyWithValidationForClass_String(dict: responseDict, key: "firstName")
                        let lName : String = PIService.object_forKeyWithValidationForClass_String(dict: responseDict, key: "lastName")
                        
                        self.socialName = "\(fName) \(lName)"
                        
                        self.socialParameter["social_id"] = self.socialId
                        self.socialParameter["social_type"] = self.socialType
                        self.socialLoginService()
                        
                        DispatchQueue.main.async(execute: { () -> Void in
                        })
                    }
                    catch {
                        print("Could not convert JSON data into a dictionary.")
                    }
                }
            }
            task.resume()
        }
    }
    
    // MARK: - Social Service Method
    func socialLoginService() {
        
        let strurl: String = BazarBit.SocialMediaService()
        let url: URL = URL(string: strurl)!
        
        var headers: HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        
        PIService.serviceCall(url: url, method: .post, parameter: socialParameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true) { (swiftyJson, responseDict) in
            
//            print(responseDict)
//            print(self.socialParameter)
            
            let dict:[String: AnyObject] = responseDict
            
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            
            let status: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "status")
            
            if status == "ok" {
                
                if (dict["payload"] != nil) {
                    
                    let payload: [String: AnyObject] = dict["payload"] as! [String : AnyObject]
                    
                    UserDefaultFunction.setCustomDictionary(dict: payload, key: BazarBit.USERACCOUNT)
                    
                    let strUserId: String = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "user_id")
                    
                    UserDefaults.standard.set(strUserId, forKey: BazarBit.USERID)
                    
                    let authoUser: [String: AnyObject] = payload["authUser"] as! [String : AnyObject]
                    UserDefaults.standard.set(authoUser["user_token"], forKey: BazarBit.AUTHTOKEN)
                    
                    let strFName:String = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "firstname")
                    
                    UserDefaults.standard.set(strFName, forKey: BazarBit.USERNAME)
                    
                    let strEmail:String = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "username")
                    
                    UserDefaults.standard.set(strEmail, forKey: BazarBit.USEREMAILADDRESS)
                    
                    if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
                        myDelegate.openHomePage()
                        PIAlertUtils.displayAlertWithMessage(message)
                    }
                }
            }
            else {
                
                let registerVC = self.storyboard?.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
                
                if self.socialType == "linkedin" {
                    registerVC.dictLinkedIn = self.linkedInDetail
                }
                registerVC.socialId = self.socialId
                registerVC.socialType = self.socialType
                registerVC.socialName = self.socialName
                
                self.navigationController?.pushViewController(registerVC, animated: true)
                PIAlertUtils.displayAlertWithMessage(message)
            }
        }
    }
}
