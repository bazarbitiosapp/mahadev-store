//
//  OtpViewController.swift
//  BazarBit
//
//  Created by Sangani ajay on 11/21/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import Alamofire

class OtpViewController: UIViewController, VPMOTPViewDelegate {
    
    @IBOutlet var labelMobileNumber: PIVarificationMsgLable!
    @IBOutlet var viewOtp: VPMOTPView!
    @IBOutlet var buttonContinue: PICustomButtonRoundedCornerBlack!
    @IBOutlet var buttonResendAgain: UIButton!
    @IBOutlet weak var btnLoginNow: PICustomButton!
    
    var strMobileNo : String!
    var strRegUserToken : String!
    var strOtp : String = ""
    var strEmail : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //OTP
        if IS_IPHONE_5_OR_5S() {
            viewOtp.otpFieldSize = 38
        }
        else if IS_IPAD_DEVICE() {
            viewOtp.otpFieldSize = 50
        }else {
            viewOtp.otpFieldSize = 45
        }
        
        viewOtp.otpFieldsCount = 6
        viewOtp.otpFieldDisplayType = .circular
        viewOtp.otpFieldBorderWidth = 0
        viewOtp.cursorColor = UIColor.white
        viewOtp.otpFieldDefaultBackgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)!
        viewOtp.otpFieldEnteredBackgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)!
        viewOtp.delegate = self as? VPMOTPViewDelegate
        viewOtp.initalizeUI()
     
        labelMobileNumber.text = "Please type the verification code send to \(strMobileNo!)"
        buttonContinue.layer.cornerRadius = 20.0
        
        btnLoginNow.setButtonTextThemeColor()
    }
    
    // Paste Disable
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        
        if action == #selector(UIResponderStandardEditActions.paste(_:)) || action == #selector(paste(_:)) || action == #selector(cut(_:)) || action == #selector(selectAll(_:)) || action == #selector(select(_:)){
            return false
        }
        return false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func hasEnteredAllOTP(hasEntered: Bool) {
        print("Has entered all OTP? \(hasEntered)")
        if hasEntered {
            var strEnterOTP = viewOtp.getOTPString()
            strEnterOTP = strEnterOTP.trimmed()
            strOtp = strEnterOTP
            
            if strEnterOTP.characters.count == 6 {
                ServiceCallForVerifyOtp()
            }
            else
            {
                PIAlertUtils.displayAlertWithMessage("Please Enter OTP..!")
            }
        }
    }
    
    func enteredOTP(otpString: String) {
        print("OTPString: \(otpString)")
        strOtp = otpString
    }
    
    //MARK: buttonContinue_Clicked
    @IBAction func buttonContinueClicked(_ sender: Any) {
        var strEnterOTP = viewOtp.getOTPString()
        strEnterOTP = strEnterOTP.trimmed()
        strOtp = strEnterOTP
        
        if strEnterOTP.characters.count == 6 {
            ServiceCallForVerifyOtp()
        }else {
            PIAlertUtils.displayAlertWithMessage("Please Enter OTP..!")
        }
    }
    
    @IBAction func btnLoginNow(_ sender: Any) {
        UserDefaults.standard.removeObject(forKey: BazarBit.USEREMAILID)
        
        if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
            myDelegate.openLoginPage()
        }
    }
   
    //MARK: buttonResendAgain_Clciked
    @IBAction func buttonResendClicked(_ sender: Any) {
        viewOtp.clearTextField()
        viewOtp.initalizeUI2()
        ServiceCallForResendOtp()
    }
    
    //MARK: ServiceCallForVerifyOtp
    func ServiceCallForVerifyOtp() {
        
        let strurl:String = BazarBit.SignUpVerifyOtp()
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
        print(headers)
        
        var parameter:Parameters = [:]
        parameter["otp"] = strOtp
        parameter["otp_token"] = strRegUserToken

        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in

            print(responseDict)
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: responseDict, key: "status")
        
            if status == "ok"
            {
                if (responseDict["payload"] != nil)
                {
                    let payload:[String:AnyObject] = responseDict["payload"] as! [String:AnyObject]
                    print(payload)
                    
                    let loginVC : LoginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    
                    loginVC.isFromLogin = true
                    print(self.strEmail)
                    loginVC.strStoredEmail = self.strEmail
                    self.navigationController?.pushViewController(loginVC, animated: true)
                    
                    /*if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
                        myDelegate.openLoginPage()
                        PIAlertUtils.displayAlertWithMessage("User Register Successfully..!")
                    } */
                }
            }
            else
            {
                PIAlertUtils.displayAlertWithMessage(message)
            }
        })
    }
    
    //MARK: ServiceCallForResendOtp
    func ServiceCallForResendOtp() {
        
        let strurl:String = BazarBit.SignUpResendOtp()
        
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
        print(headers)
        
        var parameter:Parameters = [:]
        parameter["user_token"] = UserDefaults.standard.object(forKey: "user_token_signup")
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            print(responseDict)
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: responseDict, key: "status")
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            
            if status == "ok"
            {
                if (responseDict["payload"] != nil)
                {
                    let payload:[String:AnyObject] = responseDict["payload"] as! [String:AnyObject]
                    let message:String = PIService.object_forKeyWithValidationForClass_String(dict: responseDict, key: "message")
                    PIAlertUtils.displayAlertWithMessage(message)
                }
            }
            else
            {
                PIAlertUtils.displayAlertWithMessage(message)
            }
        })
    }
}
