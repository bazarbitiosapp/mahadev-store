//
//  ContactUsViewController.swift
//  BazarBit
//
//  Created by Sangani ajay on 10/31/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ContactUsViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var viewNavigation : UIView!
    @IBOutlet var lableTitle: PICustomTitleLable!
    @IBOutlet var textFieldName: PITextField!
    @IBOutlet var textFieldEmail: PITextField!
    @IBOutlet var textFieldSubject: PITextField!
    @IBOutlet var textFieldMessge: PITextField!
    @IBOutlet var viewBack : UIView!
    
    var strName : String = ""
    var strEmail : String = ""
    var strSubject : String = ""
    var strMessage : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewBack.backgroundColor = UIColor(red: 236.0/255.0, green: 236.0/255.0, blue: 236.0/255.0, alpha: 1.0)
        
        viewNavigation.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        
        textFieldName.text = UserDefaults.standard.value(forKey: BazarBit.USERNAME) as? String
        textFieldEmail.text = UserDefaults.standard.value(forKey: BazarBit.USEREMAILADDRESS) as? String
    }
    
    // MARK:- ACTION METHOD
    @IBAction func btnOpenDrawer(_ sender: Any) {
        self.evo_drawerController?.toggleLeftDrawerSide(animated: true, completion: nil)
    }
    
    //MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string == ""
        {
            return true
        }
        
        if textField == textFieldName {
            if (textField.text?.characters.count)! >= 30 {
                return false
            }else {
                return true
            }
        }
        
        if textField == textFieldEmail {
            if (textField.text?.characters.count)! >= 30 {
                return false
            }else {
                return true
            }
        }
        
        if textField == textFieldSubject {
            if (textField.text?.characters.count)! >= 200 {
                return false
            }else {
                return true
            }
        }
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Validation Method    
    func checkValidation() -> Bool {
        
        var isValid: Bool = true
        
        strName = textFieldName.text!
        strEmail = textFieldEmail.text!
        strSubject = textFieldSubject.text!
        strMessage = textFieldMessge.text!
        
        strName = strName.trimmed()
        strEmail = strEmail.trimmed()
        strSubject = strSubject.trimmed()
        strMessage = strMessage.trimmed()
        
        if PIValidation.isBlankString(str: strName) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please Enter Name...!")
            return isValid
        }
        
        if PIValidation.isBlankString(str: strEmail) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please Enter Email...!")
            return isValid
            
        } else if (PIValidation.isEmailString(str: strEmail)) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Email Not Valid...!")
            return isValid
            
        } else if PIValidation.isBlankString(str: strSubject) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please Enter Subject...!")
            return isValid
            
        }
        else if PIValidation.isBlankString(str: strMessage) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please Enter Message...!")
            return isValid
        }
        return isValid
    }
    
    //MARK: buttonSubmit_Clicked
    @IBAction func buttonSubmitClicked(_ sender: Any) {
        if checkValidation() {
            self.ServiceCallForSaveContactForm()
        }
    }
    
    //MARK: ServiceCallForSaveContactForm
    func ServiceCallForSaveContactForm() {
        
        let strurl:String = BazarBit.SaveConstactFormService()
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
        
        var parameter:Parameters = [:]
        parameter["name"] = textFieldName.text!
        parameter["email"] = textFieldEmail.text!
        parameter["subject"] = textFieldSubject.text!
        parameter["message"] = textFieldMessge.text!
        print(parameter)
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            print(responseDict)
            var response : [String : AnyObject] = responseDict
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: response, key: "status")
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            
            if status == "ok"
            {
                self.textFieldName.text = ""
                self.textFieldEmail.text = ""
                self.textFieldSubject.text = ""
                self.textFieldMessge.text = ""
                
                if (response["payload"] != nil)
                {
                    let payload:[String : AnyObject] = response["payload"] as! [String : AnyObject]
                    PIAlertUtils.displayAlertWithMessage(message)
                }else {
                    PIAlertUtils.displayAlertWithMessage(message)
                }
            }else {
                PIAlertUtils.displayAlertWithMessage(message)
            }
        })
    }
}
