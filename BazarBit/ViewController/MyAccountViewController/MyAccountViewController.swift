//
//  MyAccountViewController.swift
//  BazarBit
//
//  Created by Sangani ajay on 10/16/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import AVFoundation
import Alamofire
import SwiftyJSON
import EZSwiftExtensions

class MyAccountViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPickerViewDelegate,UIPickerViewDataSource, UITextFieldDelegate {
    
    @IBOutlet var viewNavigation : UIView!
    @IBOutlet var lableTitle: PICustomTitleLable!
    
    @IBOutlet var viewBack : UIView!
    @IBOutlet var viewGender : UIView!
    @IBOutlet var scrlView : UIScrollView!
    
    @IBOutlet var textFieldFirstName: PITextField!
    @IBOutlet var textFieldLastName: PITextField!
    @IBOutlet var textFieldEmail: PITextField!
    @IBOutlet var textFieldMobileNo: PITextField!
    @IBOutlet var textFieldBirthDate: PITextField!
    @IBOutlet var textFieldAddress: PITextField!
    @IBOutlet var textFieldCountry: PITextField!
    @IBOutlet var textFieldState: PITextField!
    @IBOutlet var textFieldCity: PITextField!
    @IBOutlet var textFieldPinCode: PITextField!
    
    @IBOutlet var imageViewMale : UIImageView!
    @IBOutlet var imageViewFemale : UIImageView!
    
    var strFname : String = ""
    var strLname : String = ""
    var strEmail : String = ""
    var strMobileNo : String = ""
    var strDOB : String = ""
    var strAddress : String = ""
    var strCountry: String = ""
    var strState : String = ""
    var strCity : String = ""
    var strPinCode : String = ""
    var strGender : String = ""
    
    @IBOutlet var buttonProfile: UIButton!
    @IBOutlet var imageViewProfile : UIImageView!
    var imagePicker = UIImagePickerController()
    var popOver:UIPopoverController?
    
    var userProfile = [String: AnyObject]()
    var imageName : String = ""
    var strImgBase64String : String = ""
    var strImage : String = ""
    
    var profile_url : [String: Any] = [:]
    var old_Profile_url : String = ""
    
    var countryPicker = UIPickerView()
    var aryCountryCode = [AnyObject]()
    
    var statePicker = UIPickerView()
    var aryStateCode = [AnyObject]()
    
    var strSelectedCountry : String = ""
    
    var didSelectCountry : ((_ selectedCountry : String) -> Void)!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        textFieldPinCode.keyboardType = .numberPad
        self.viewBack.backgroundColor = UIColor(red: 243.0/255.0, green: 243.0/255.0, blue: 243.0/255.0, alpha: 1.0)
        self.view.backgroundColor = UIColor(red: 243.0/255.0, green: 243.0/255.0, blue: 243.0/255.0, alpha: 1.0)
        
        viewNavigation.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        
        imageViewProfile.RoundImage()
        buttonProfile.RoundButton()
        self.imagePicker.delegate = self
        
        addToolBar(textField: textFieldCountry)
        self.ServiceCallForGetCountryList()
        
        if IS_IPAD_DEVICE() {
            self.viewGender.layer.cornerRadius = 30.0
        }else {
            self.viewGender.layer.cornerRadius = 20.0
        }
        
        viewGender.layer.borderWidth = 1.0
        viewGender.layer.borderColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)?.cgColor
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        scrlView.layoutIfNeeded()
        self.ServiceCallForGetUserData()
    }
    
    //MARK: ImageViewClicked
    @IBAction func buttonImageViewClicked(_ sender: Any) {
        
        let alertController: UIAlertController = UIAlertController(title: "BazarBit", message: "Choose Photo?", preferredStyle: .alert)
        
        let selectPhoto = UIAlertAction(title: "Select Photo", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            self.openGallary()
        }
        
        let takePhoto = UIAlertAction(title: "Take Photo", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            self.openCamera()
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
        }
        
        alertController.addAction(selectPhoto)
        alertController.addAction(takePhoto)
        alertController.addAction(cancel)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    //MARK: UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        //IMAGE
        let imageUrl          = info[UIImagePickerControllerReferenceURL] as? NSURL
        
        imageName         = "\(imageUrl?.lastPathComponent ?? "nil")"
        
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageViewProfile.contentMode = .scaleToFill
            buttonProfile.setImage(chosenImage, for: .normal)
            
            if let base64String = UIImageJPEGRepresentation(chosenImage, 0.9)?.base64EncodedString() {
                strImgBase64String = base64String
                
                strImage = "data:image/jpeg;base64,\(strImgBase64String)"
                print(strImage)
                
                profile_url[imageName] = strImage
                print(profile_url)
            }
        } else{
            print("Something went wrong")
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker:
        UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    //Open Gallary
    func openGallary()
    {
        let pickerView: UIImagePickerController = UIImagePickerController()
        pickerView.allowsEditing = false
        pickerView.modalPresentationStyle = .popover
        pickerView.sourceType = UIImagePickerControllerSourceType.photoLibrary
        pickerView.delegate = self
        
        if IS_IPAD_DEVICE() {
            self.popOver = UIPopoverController(contentViewController: pickerView)
            self.popOver?.present(from: self.imageViewProfile.bounds, in: self.view,permittedArrowDirections: UIPopoverArrowDirection.up, animated: true)
            
        } else {
            self.present(pickerView, animated: true, completion: nil)
        }
    }
    
    //MARK: Open Camera
    func openCamera()
    {
        let deviceHasCamera = UIImagePickerController.isSourceTypeAvailable(.camera)
        
        if (deviceHasCamera) {
            let authStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
            switch authStatus {
            case .authorized: callCamera()
            case .denied: alertToEncourageCameraAccessInitially()
            case .notDetermined: alertPromptToAllowCameraAccessViaSetting()
            default: alertToEncourageCameraAccessInitially()
            }
        } else {
            let alertController = UIAlertController(title: "Error", message: "Device has no camera", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
            })
            alertController.addAction(defaultAction)
            present(alertController, animated: true, completion: nil)
        }
    }
    
    //NEW
    func callCamera(){
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self;
        myPickerController.sourceType = UIImagePickerControllerSourceType.camera
        
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    
    func alertToEncourageCameraAccessInitially() {
        let alert = UIAlertController(
            title: "IMPORTANT",
            message: "Camera access required for capturing photos!",
            preferredStyle: UIAlertControllerStyle.alert
        )
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Allow Camera", style: .cancel, handler: { (alert) -> Void in
            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        }))
        present(alert, animated: true, completion: nil)
    }
    
    func alertPromptToAllowCameraAccessViaSetting() {
        
        let alert = UIAlertController(
            title: "IMPORTANT",
            message: "Camera access required for capturing photos!",
            preferredStyle: UIAlertControllerStyle.alert
        )
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel) { alert in
            if AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo).count > 0 {
                AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo) { granted in
                    DispatchQueue.main.async() {
                        self.openCamera() } }
            }
            }
        )
        present(alert, animated: true, completion: nil)
    }
    
    //MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if textField == textFieldCountry {
            
            if aryCountryCode.count > 0 {
                textFieldCountry.inputView = countryPicker
            }else {
                PIAlertUtils.displayAlertWithMessage("Country not found...!")
            }
        }
        if textField == textFieldState {
            self.addToolBar(textField: self.textFieldState)
            self.ServiceCallForGetStateList(strCountryName: APPDELEGATE.strSelectedCountry)
            
        }
        else if textField == textFieldBirthDate
        {
            addToolBar(textField: textFieldBirthDate)
            let datePicker = UIDatePicker()
            datePicker.datePickerMode = .date
            datePicker.maximumDate = Date()
            textField.inputView = datePicker
            datePicker.addTarget(self, action: #selector(datePickerChanged(sender:)), for: .valueChanged)
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string == ""
        {
            return true
        }
        
        if textField == textFieldFirstName {
            if (textField.text?.characters.count)! >= 20 {
                return false
            }else {
                return true
            }
        }
        
        if textField == textFieldLastName {
            if (textField.text?.characters.count)! >= 20 {
                return false
            }else {
                return true
            }
        }
        
        if textField == textFieldEmail {
            if (textField.text?.characters.count)! >= 50 {
                return false
            }else {
                return true
            }
        }
        
        if textField == textFieldAddress {
            if (textField.text?.characters.count)! >= 80 {
                return false
            }else {
                return true
            }
        }
        
        if textField == textFieldPinCode {
            if (textField.text?.characters.count)! >= 6 {
                return false
            }else {
                return true
            }
        }
        
        if textField == textFieldMobileNo {
            if (textField.text?.characters.count)! >= 15 {
                return false
            }
            if !(string.isNumber()) {
                return false
            }
        }
        return true
    }
    
    func datePickerChanged(sender: UIDatePicker) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        let date = Date()
        if sender.date < date {
            textFieldBirthDate.text = formatter.string(from: sender.date)
        } else {
            PIAlertUtils.displayAlertWithMessage("Invalid Date")
        }
    }
    
    // MARK: - Picker toolbar Button Method
    
    func addToolBar(textField : PITextField){
        
        let toolBar = UIToolbar()
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(donePressed))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        textField.tintColor = UIColor.clear
        
        if textField == textFieldCountry {
            self.countryPicker.dataSource = self
            self.countryPicker.delegate = self
            self.countryPicker.reloadAllComponents()
            doneButton.tag = 0
        }
        else if textField == textFieldState
        {
            doneButton.tag = 1
            if APPDELEGATE.strSelectedCountry.characters.count == 0 {
                PIAlertUtils.displayAlertWithMessage("Please select Country first...!")
                textFieldState.inputView = UIView()
            }
            else if textField == textFieldState
            {
                self.statePicker.isHidden = true
                textFieldState.inputView = statePicker
            }
        }
        textField.inputAccessoryView = toolBar
    }
    
    func donePressed(sender:UIButton)  {
        print(sender.tag)
        if sender.tag == 0 { //COUNTRY
            
            let row : Int = countryPicker.selectedRow(inComponent: 0)
            
            let dict : [String : AnyObject] = aryCountryCode[row] as! [String : AnyObject]
            
            let strName:String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "country_name")
            APPDELEGATE.strSelectedCountry = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "country_name")
            
            textFieldCountry.text = strName
            textFieldCountry.resignFirstResponder()
            
        }else if sender.tag == 1 {
            textFieldState.resignFirstResponder()
        }else {
            textFieldBirthDate.resignFirstResponder()
        }
        textFieldBirthDate.resignFirstResponder()
    }
    
    // MARK: - PickerView Methods
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == countryPicker {
            return aryCountryCode.count
        }else {
            return aryStateCode.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == countryPicker {
            let dict : [String: AnyObject] = aryCountryCode[row] as! [String:AnyObject]
            let strCountryData:String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "country_name")
            return strCountryData
        }
        else
        {
            let dict : [String: AnyObject] = aryStateCode[row] as! [String:AnyObject]
            let strStateData:String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "state_name")
            return strStateData
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == statePicker
        {
            let dict : [String:AnyObject] = aryStateCode[row] as! [String:AnyObject]
            let strStateName:String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "state_name")
            textFieldState.text = strStateName
        }
    }
    
    //MARK: buttonBack_Clicked
    @IBAction func buttonBack_Clicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: buttonSubmit_Clicked
    @IBAction func buttonSubmitClicked(_ sender: Any) {
        if checkValidation() {
            self.ServiceCallForUpdateUserData()
        }
    }
    
    //MARK: buttonClear_Clicked
    @IBAction func buttonClearClicked(_ sender: Any) {
        textFieldFirstName.text! = ""
        textFieldLastName.text! = ""
        textFieldMobileNo.text! = ""
        textFieldBirthDate.text! = ""
        textFieldAddress.text! = ""
        textFieldCountry.text! = ""
        textFieldState.text! = ""
        textFieldCity.text! = ""
        textFieldPinCode.text! = ""
    }
    
    //MARK: buttonMaleClicked
    @IBAction func buttonMaleClicked(_ sender: Any) {
        self.DisplayGenderSelection(trueImgView:imageViewMale, falseImgView1: imageViewFemale)
        self.strGender = "Male"
    }
    
    //MARK: buttonFemaleClicked
    @IBAction func buttonFemaleClicked(_ sender: Any) {
        self.DisplayGenderSelection(trueImgView: imageViewFemale, falseImgView1: imageViewMale)
        self.strGender = "Female"
    }
    
    //MARK: DISPLAY GENDER SELECTION
    func DisplayGenderSelection(trueImgView: UIImageView, falseImgView1: UIImageView) {
        trueImgView.image = #imageLiteral(resourceName: "radio")
        falseImgView1.image = #imageLiteral(resourceName: "radioLight")
    }
    
    // MARK: - Validation Method
    
    func checkValidation() -> Bool {
        
        var isValid: Bool = true
        
        strFname = textFieldFirstName.text!
        strLname = textFieldLastName.text!
        strEmail = textFieldEmail.text!
        strMobileNo = textFieldMobileNo.text!
        strDOB = textFieldBirthDate.text!
        strAddress = textFieldAddress.text!
        strCountry = textFieldCountry.text!
        strState = textFieldState.text!
        strCity = textFieldCity.text!
        strPinCode = textFieldPinCode.text!
        
        
        strFname = strFname.trimmed()
        strLname = strLname.trimmed()
        strEmail = strEmail.trimmed()
        strMobileNo = strMobileNo.trimmed()
        strDOB = strDOB.trimmed()
        strAddress = strAddress.trimmed()
        strCountry = strCountry.trimmed()
        strState = strState.trimmed()
        strCity = strCity.trimmed()
        strPinCode = strPinCode.trimmed()
        
        if PIValidation.isBlankString(str: strFname) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please Enter First Name...!")
            return isValid
        }
        
        if PIValidation.isBlankString(str: strLname) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please Enter Last Name...!")
            return isValid
        }
        
        if PIValidation.isBlankString(str: strEmail) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please Enter Email...!")
            return isValid
            
        } else if (PIValidation.isEmailString(str: strEmail)) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Email Not Valid...!")
            return isValid
            
        } else if PIValidation.isBlankString(str: strMobileNo) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please Enter Mobile Number...!")
            return isValid
            
        }
        else if strMobileNo.characters.count < 10 || strMobileNo.characters.count > 15 {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Enter Valid Mobile No...!")
            return isValid
            
        }
        else if PIValidation.isBlankString(str: strGender) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please select gender...!")
            return isValid
            
        }
        else if PIValidation.isBlankString(str: strDOB) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please select date of birth...!")
            return isValid
            
        }
        else if PIValidation.isBlankString(str: strAddress) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please enter address...!")
            return isValid
            
        }
        else if PIValidation.isBlankString(str: strCountry) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please select country...!")
            return isValid
            
        }    else if PIValidation.isBlankString(str: strState) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please select state...!")
            return isValid
        }
        else if PIValidation.isBlankString(str: strCity) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please enter city...!")
            return isValid
        }
        else if PIValidation.isBlankString(str: strPinCode) {
            isValid = false
            PIAlertUtils.displayAlertWithMessage("Please enter pincode...!")
            return isValid
        }
        return isValid
    }
    
    //MARK: ServiceCallForGetCountryList
    func ServiceCallForGetCountryList() {
        
        let strurl:String = BazarBit.GetCountryListService()
        let url:URL = URL(string: strurl)!
        var headers:HTTPHeaders = [:]
        
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
        
        let parameter:Parameters = [:]
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            var response : [String : AnyObject] = responseDict
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: response, key: "status")
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            
            if status == "ok"
            {
                if (response["payload"] != nil)
                {
                    let payload:[AnyObject] = response["payload"] as! [AnyObject]
                    self.aryCountryCode = payload
                }
            } else {
                PIAlertUtils.displayAlertWithMessage(message)
            }
        })
    }
    
    //MARK: ServiceCallForGetUserData
    func ServiceCallForGetUserData() {
        
        let strurl:String = BazarBit.GetUserDataService()
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
        
        let parameter:Parameters = [:]
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            var response : [String : AnyObject] = responseDict
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: response, key: "status")
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            
            if status == "ok"
            {
                if (response["payload"] != nil)
                {
                    let payload:[String : AnyObject] = response["payload"] as! [String : AnyObject]
                    print(payload)
                    
                    self.textFieldFirstName.text! = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "firstname")
                    self.textFieldLastName.text! = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "lastname")
                    self.textFieldEmail.text! = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "email")
                    self.textFieldMobileNo.text! = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "phoneno")
                    self.textFieldBirthDate.text! = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "birthdate")
                    self.textFieldAddress.text! = "\(PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "address1"))\(PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "address2"))"
                    self.textFieldCountry.text! = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "country")
                    self.textFieldState.text! = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "state")
                    self.textFieldCity.text! = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "city")
                    self.textFieldPinCode.text! = PIService.object_forKeyWithValidationForClass_String(dict: payload, key: "postalcode")
                    
                    if payload["gender"] as! String == "Male"
                    {
                        self.DisplayGenderSelection(trueImgView:self.imageViewMale, falseImgView1: self.imageViewFemale)
                        self.strGender = "Male"
                    }
                    else if payload["gender"] as! String == "Female"
                    {
                        self.DisplayGenderSelection(trueImgView: self.imageViewFemale, falseImgView1: self.imageViewMale)
                        self.strGender = "Female"
                    }
                    
                    if payload["profilepicture"] as? [AnyObject] != nil
                    {
                        let aryImage : [AnyObject] = payload["profilepicture"] as! [AnyObject]
                        
                        print(aryImage)
                        if aryImage.count > 0
                        {
                            var imgUrl:String = aryImage[0] as! String
                            imgUrl = imgUrl.replacingOccurrences(of: " ", with: "%20")
                            self.old_Profile_url = imgUrl
                            let url1 = URL(string: imgUrl)
                            self.imageViewProfile.kf.setImage(with: url1)
                            
                            UserDefaults.standard.set(imgUrl, forKey: BazarBit.USERPROFILEIMAGE)
                        }
                        else
                        {
                            let url1 = URL(string: "")
                            self.imageViewProfile.kf.setImage(with: url1, placeholder: #imageLiteral(resourceName: "profile"))
                        }
                    }
                }
                else
                {
                    PIAlertUtils.displayAlertWithMessage(message)
                }
            }
        })
    }
    
    
    //MARK: ServiceCallForUpdateUserData
    func ServiceCallForUpdateUserData() {
        
        let strurl:String = BazarBit.UpdateUserDataService()
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
        print(headers)
        
        var parameter:Parameters = [:]
        parameter["firstname"] = textFieldFirstName.text!
        parameter["lastname"] = textFieldLastName.text!
        parameter["phoneno"] = textFieldMobileNo.text!
        parameter["pincode"] = textFieldPinCode.text!
        parameter["gender"] = strGender
        parameter["address"] = textFieldAddress.text!
        parameter["country"] = textFieldCountry.text!
        parameter["state"] = textFieldState.text!
        parameter["city"] = textFieldCity.text!
        parameter["dateofbirth"] = textFieldBirthDate.text!
        
        if profile_url.count != 0
        {
            parameter["profilepicture"] = profile_url
        }
        else
        {
            print(self.old_Profile_url)
            if (self.old_Profile_url != "") {
                parameter["old_image"] = self.old_Profile_url
            }
        }
        print(parameter)
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            BazarBitLoader.stopLoader()
            print(responseDict)
            
            var response : [String : AnyObject] = responseDict
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: response, key: "status")
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            
            if status == "ok"
            {
                if (response["payload"] != nil)
                {
                    let payload:[String : AnyObject] = response["payload"] as! [String : AnyObject]
                    print(payload)
                    
                    APPDELEGATE.openHomePage()
                    PIAlertUtils.displayAlertWithMessage(message)
                    
                    if payload["profilepicture"] as? [AnyObject] != nil
                    {
                        let aryImage : [AnyObject] = payload["profilepicture"] as! [AnyObject]
                    }
                    self.ServiceCallForGetUserData()
                }
            }
            else
            {
                PIAlertUtils.displayAlertWithMessage(message)
            }
        })
    }
    
    //MARK: ServiceCallForGetStateList
    func ServiceCallForGetStateList(strCountryName : String) {
        
        let strurl:String = BazarBit.GetStateListService()
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
        
        var parameter:Parameters = [:]
        parameter["country_name"] = strCountryName
        print(parameter)
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            BazarBitLoader.stopLoader()
            
            var response : [String : AnyObject] = responseDict
            
            let status:String = PIService.object_forKeyWithValidationForClass_String(dict: response, key: "status")
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            
            if status == "ok"
            {
                if (response["payload"] != nil)
                {
                    let payload:[AnyObject] = response["payload"] as! [AnyObject]
                    self.aryStateCode = payload
                    
                    if self.aryStateCode.count > 0 {
                        self.statePicker.isHidden = false
                        self.statePicker.dataSource = self
                        self.statePicker.delegate = self
                        self.statePicker.reloadAllComponents()
                    }else {
                        self.view.endEditing(true)
                        self.statePicker.isHidden = true
                        PIAlertUtils.displayAlertWithMessage("State not found...!")
                    }
                }
                else
                {
                    PIAlertUtils.displayAlertWithMessage(message)
                }
            }
        })
    }
}
