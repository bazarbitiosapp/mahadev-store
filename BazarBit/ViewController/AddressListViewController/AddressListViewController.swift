//
//  AddressListViewController.swift
//  BazarBit
//
//  Created by Parghi Infotech on 10/25/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import Alamofire
import Toaster

class AddressListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var lblHeaderName: UILabel!
    var isFromMenu : Bool = false
    @IBOutlet var buttonBack : UIButton!
    @IBOutlet var buttonMenu : UIButton!
    var strPinCode: String = ""
    
    @IBOutlet weak var viewNofound: UIView!
    @IBOutlet var tbladdressList: UITableView!
    @IBOutlet var tbladdressListHeight: NSLayoutConstraint!
    
    @IBOutlet var viewAddress: UIView!
    @IBOutlet var viewShipping: UIView!
    
    @IBOutlet var viewAddressHeight: NSLayoutConstraint!
    @IBOutlet var viewShippingHeight: NSLayoutConstraint!
    
    @IBOutlet var btnAddAddress: UIButton!
    @IBOutlet var viewNavigation : UIView!
    
    var strCheckService : String = ""
    var checkOutVC: CheckOutViewController!
    
    var arrAddressList: [AnyObject] = []
    var strUserAddressID: String = ""
    var strAddressType: String = ""
    
    var fromVC: String = ""
    var isFromMyAddress : Bool = false
    
    // MARK: - UIView Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        viewNavigation.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        
        if fromVC == "CheckOut" {
            self.viewAddress.isHidden = false
            self.viewShipping.isHidden = false
            
            self.viewAddressHeight.constant = 64.0
            self.viewShippingHeight.constant = 30.0
            
        } else {
            
            self.viewAddress.isHidden = true
            self.viewShipping.isHidden = true
            
            self.viewAddressHeight.constant = 0.0
            self.viewShippingHeight.constant = 0.0
        }
        
        self.viewAddress.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)

        setBorderPropertiesForButton()
        
        // Do any additional setup after loading the view.
        
        if isFromMenu == true {
            buttonMenu.isHidden = false
            buttonBack.isHidden = true
        }else {
            buttonMenu.isHidden = true
            buttonBack.isHidden = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.viewNofound.isHidden = true
        
        if strCheckService == "" {
            callService_AddressList()
        }
        
        if APPDELEGATE.chkLablecheck == "Shipping" {
            self.lblHeaderName.text = "Shipping Address"
            //APPDELEGATE.chkLablecheck = ""
        } else if APPDELEGATE.chkLablecheck == "Billing" {
            self.lblHeaderName.text = "Billing Address"
            //APPDELEGATE.chkLablecheck = ""
        }
    }
    
    //MARK: buttonOpenDrawer
    @IBAction func btnOpenDrawer(_ sender: Any) {
        self.evo_drawerController?.toggleLeftDrawerSide(animated: true, completion: nil)
    }
    
    // MARK:- UITableView Method
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tbladdressList {
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tbladdressList {
            return arrAddressList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tbladdressList {
            
            let identifier : String = "AddressListCell"
            
            let addressListCell : AddressListCell = tableView.dequeueReusableCell(withIdentifier: identifier) as! AddressListCell
            
            let dict: [String: AnyObject] = arrAddressList[indexPath.row] as! [String: AnyObject]
            
            if dict.count > 0
            {
                let strFirstName: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "first_name")
                let strLastName: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "last_name")
                
                let strName: String = "\(strFirstName) \(strLastName)"
                addressListCell.lblName.text = strName
                
                let strAddress: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "address")
                addressListCell.lblAddress.text = strAddress
                
                let strCity: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "city")
                strPinCode = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "pincode")
                
                let strFullAddress: String = "\(strCity) - \(strPinCode)"
                addressListCell.lblCity.text = strFullAddress
                
                let strState: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "state")
                addressListCell.lblState.text = strState
                
                let strMobileNo: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "phone_number")
                addressListCell.lblMobileNo.text = strMobileNo
            }
            addressListCell.viewAddressList.applyShadowWithColor(.lightGray)
            
            return addressListCell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if Constants.DeviceType.IS_IPAD {
            return 150.0
        } else {
            return 120.0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isFromMyAddress == false {
            if checkOutVC == nil {
                //APPDELEGATE.isBilling = true
                if strAddressType == "0" {
                    
                    APPDELEGATE.isBilling = true
                    
                    let checkOutVC: CheckOutViewController = self.storyboard?.instantiateViewController(withIdentifier: "CheckOutViewController") as! CheckOutViewController
                    
                    checkOutVC.strAddressType = "0"
                    APPDELEGATE.chkSelectedList = true
                    
                    let dict: [String: AnyObject] = arrAddressList[indexPath.row] as! [String: AnyObject]
                    
                    let strFirstName: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "first_name")
                    let strLastName: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "last_name")
                    
                    checkOutVC.strShippingName = "\(strFirstName) \(strLastName)"
                    
                    let strAddress: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "address")
                    checkOutVC.strShippingAddress = strAddress
                    
                    checkOutVC.strShippingOnlyCity = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "city")
                    checkOutVC.shippingPinCode = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "pincode")
                    
                    checkOutVC.paramterPinCode = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "pincode")
                    
                    let strFullAddress: String = "\(checkOutVC.strShippingOnlyCity ) - \(checkOutVC.shippingPinCode)"
                    checkOutVC.strShippingCity = strFullAddress
                    
                    let strState: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "state")
                    checkOutVC.strShippingState = strState
                    
                    let strLandMark: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "landmark")
                    checkOutVC.strShippingLandMark = strLandMark
                    
                    let strMobileNo: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "phone_number")
                    checkOutVC.strShippingPhoneNo = strMobileNo
                    
                    let strCountry: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "country")
                    checkOutVC.strShippingCountry = strCountry
                    
                    UserDefaultFunction.setCustomDictionary(dict: dict, key: BazarBit.SHIPPINGADDRESS)
                    
                    if isFromMyAddress == false {
                        self.navigationController?.pushViewController(checkOutVC, animated: true)
                    }
                    
                } else {
                    
                    let checkOutVC: CheckOutViewController = self.storyboard?.instantiateViewController(withIdentifier: "CheckOutViewController") as! CheckOutViewController
                    
                    checkOutVC.strAddressType = "1"
                    APPDELEGATE.chkSelectedList = true
                    APPDELEGATE.isBilling = true
                    
                    let dict: [String: AnyObject] = arrAddressList[indexPath.row] as! [String: AnyObject]
                    
                    let strFirstName: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "first_name")
                    let strLastName: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "last_name")
                    
                    checkOutVC.strBillingName = "\(strFirstName) \(strLastName)"
                    
                    let strAddress: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "address")
                    checkOutVC.strBillingAddress = strAddress
                    
                    checkOutVC.strBillingOnlyCity = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "city")
                    checkOutVC.strBillingPinCode = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "pincode")
                    
                    checkOutVC.paramterPinCode = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "pincode")
                    
                    let strFullAddress: String = "\(checkOutVC.strBillingOnlyCity) - \(checkOutVC.shippingPinCode)"
                    checkOutVC.strBillingCity = strFullAddress
                    
                    let strState: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "state")
                    checkOutVC.strBillingState = strState
                    
                    let strMobileNo: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "phone_number")
                    checkOutVC.strBillingPhoneNo = strMobileNo
                    
                    let strLandMark: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "landmark")
                    checkOutVC.strBillingLandMark = strLandMark
                    
                    let strCountry: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "country")
                    checkOutVC.strBillingCountry = strCountry
                    
                    UserDefaultFunction.setCustomDictionary(dict: dict, key: BazarBit.BILLINGADDRESS)
                    
                    if isFromMyAddress == false {
                        self.navigationController?.pushViewController(checkOutVC, animated: true)
                    }
                }
            }
            
            if checkOutVC != nil {
                if checkOutVC.strAddressType == "0"  {
                    
                    let dict: [String: AnyObject] = arrAddressList[indexPath.row] as! [String: AnyObject]
                    
                    let strFirstName: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "first_name")
                    let strLastName: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "last_name")
                    
                    checkOutVC.strShippingName = "\(strFirstName) \(strLastName)"
                    
                    let strAddress: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "address")
                    checkOutVC.strShippingAddress = strAddress
                    
                    checkOutVC.strShippingOnlyCity = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "city")
                    checkOutVC.shippingPinCode = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "pincode")
                    
                    checkOutVC.paramterPinCode = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "pincode")
                    
                    let strFullAddress: String = "\(checkOutVC.strShippingOnlyCity ) - \(checkOutVC.shippingPinCode)"
                    checkOutVC.strShippingCity = strFullAddress
                    
                    let strState: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "state")
                    checkOutVC.strShippingState = strState
                    
                    let strLandMark: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "landmark")
                    checkOutVC.strShippingLandMark = strLandMark
                    
                    let strMobileNo: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "phone_number")
                    checkOutVC.strShippingPhoneNo = strMobileNo
                    
                    let strCountry: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "country")
                    checkOutVC.strShippingCountry = strCountry
                    
                    UserDefaultFunction.setCustomDictionary(dict: dict, key: BazarBit.SHIPPINGADDRESS)
                    
                } else {
                    
                    APPDELEGATE.isBilling = false
                    APPDELEGATE.checkDisplayBillingRecord = false
                    
                    let dict: [String: AnyObject] = arrAddressList[indexPath.row] as! [String: AnyObject]
                    
                    let strFirstName: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "first_name")
                    let strLastName: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "last_name")
                    
                    checkOutVC.strBillingName = "\(strFirstName) \(strLastName)"
                    
                    let strAddress: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "address")
                    checkOutVC.strBillingAddress = strAddress
                    
                    checkOutVC.strBillingOnlyCity = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "city")
                    checkOutVC.strBillingPinCode = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "pincode")
                    
                    checkOutVC.paramterPinCode = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "pincode")
                    
                    let strFullAddress: String = "\(checkOutVC.strBillingOnlyCity) - \(checkOutVC.shippingPinCode)"
                    checkOutVC.strBillingCity = strFullAddress
                    
                    let strState: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "state")
                    checkOutVC.strBillingState = strState
                    
                    let strMobileNo: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "phone_number")
                    checkOutVC.strBillingPhoneNo = strMobileNo
                    
                    let strLandMark: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "landmark")
                    checkOutVC.strBillingLandMark = strLandMark
                    
                    let strCountry: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "country")
                    checkOutVC.strBillingCountry = strCountry
                    
                    UserDefaultFunction.setCustomDictionary(dict: dict, key: BazarBit.BILLINGADDRESS)
                    
                }
                checkOutVC.strVC = "AddressList"
                
                if checkOutVC.strPushFromShopping == "false" {
                    checkOutVC.strPushFromShopping = "true"
                    
                    if isFromMyAddress == false {
                        self.navigationController?.pushViewController(checkOutVC, animated: true)
                    }
                } else {
                    if isFromMyAddress == false {
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        }
    }
    
    // MARK: - Action Method
    @IBAction func btnBack(_ sender: Any)  {
        
        if APPDELEGATE.checkDisplayBillingRecord == false {
            APPDELEGATE.isBilling = false
        } else {
             APPDELEGATE.isBilling = true
        }
        
        APPDELEGATE.chkFirstAddress = false
        let viewVC : [UIViewController] = (self.navigationController?.viewControllers)!
        print(viewVC)
        if viewVC[viewVC.count-2] == APPDELEGATE.shoppingCartVC{
            let checkOutVC: CheckOutViewController = self.storyboard?.instantiateViewController(withIdentifier: "CheckOutViewController") as! CheckOutViewController
            
            self.navigationController?.pushViewController(checkOutVC, animated: true)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnEdit(_ sender: AnyObject)  {
        
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tbladdressList)
        let indexPath = self.tbladdressList.indexPathForRow(at: buttonPosition)
        
        let dict: [String: AnyObject] = arrAddressList[(indexPath?.row)!] as! [String: AnyObject]
        
        //            let addAddressVC: AddAddressViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressViewController") as! AddAddressViewController
        
        let EditAddressVC: EditAddressViewController = self.storyboard?.instantiateViewController(withIdentifier: "EditAddressViewController") as! EditAddressViewController
        
        //            addAddressVC.addAddressListObj = self
        //
        //            addAddressVC.dictEdit = dict
        //            addAddressVC.strPerform = "Edit"
        
        EditAddressVC.strAddressType = strAddressType
        EditAddressVC.dictEdit = dict
        
        self.navigationController?.pushViewController(EditAddressVC, animated: true)
        
    }
    
    @IBAction func btnAdd(_ sender: AnyObject)  {
        
        if APPDELEGATE.chkProfile == true {
            APPDELEGATE.chkLablecheck = "profile"
            let addAddressVC: ProfileAddAddressViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileAddAddressViewController") as! ProfileAddAddressViewController
            
            self.navigationController?.pushViewController(addAddressVC, animated: true)
        } else {
            if APPDELEGATE.strChkProfile == true{
                APPDELEGATE.chkLablecheck = "profile"
                let addAddressVC: ProfileAddAddressViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileAddAddressViewController") as! ProfileAddAddressViewController
                self.navigationController?.pushViewController(addAddressVC, animated: true)
            } else {
                let addAddressVC: AddAddressViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressViewController") as! AddAddressViewController
                
                addAddressVC.strPerform = "Add"
                addAddressVC.addAddressListObj = self
                
                self.navigationController?.pushViewController(addAddressVC, animated: true)
            }
        }
    }
    
    @IBAction func btnDelete(_ sender: AnyObject)  {
        
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tbladdressList)
        let indexPath = self.tbladdressList.indexPathForRow(at: buttonPosition)
        
        let dict: [String: AnyObject] = arrAddressList[(indexPath?.row)!] as! [String: AnyObject]
        
        strUserAddressID = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "useradress_id")
        strAddressType = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "address_type")
        
        let firstName = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "first_name")
        print(firstName)
        let lastName = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "last_name")
        print(lastName)
        
        if self.fromVC != "CheckOut" {
            APPDELEGATE.chkLablecheck = ""
            callService_DeleteAddress()
        }
        if APPDELEGATE.chkLablecheck == "Shipping" {
            if (UserDefaults.standard.object(forKey: BazarBit.SHIPPINGADDRESS) != nil) {
                
                let shipdict:[String:AnyObject] = UserDefaultFunction.getDictionary(forKey: BazarBit.SHIPPINGADDRESS)! as [String : AnyObject]
                
                let strShippingFirstName = PIService.object_forKeyWithValidationForClass_String(dict: shipdict, key: "first_name")
                print(strShippingFirstName)
                let strShippingLastName = PIService.object_forKeyWithValidationForClass_String(dict: shipdict, key: "last_name")
                print(strShippingLastName)
                
                if firstName == strShippingFirstName && lastName == strShippingLastName
                {
                    UserDefaults.standard.removeObject(forKey: BazarBit.SHIPPINGADDRESS)
                    APPDELEGATE.chkAddAddress = false
                    callService_DeleteAddress()
                } else {
                    callService_DeleteAddress()
                }
            } else {
                callService_DeleteAddress()
            }
        } else {
            //callService_DeleteAddress()
        }
        
        if APPDELEGATE.chkLablecheck == "Billing" {
            if (UserDefaults.standard.object(forKey: BazarBit.BILLINGADDRESS) != nil) {
                
                let billdict:[String:AnyObject] = UserDefaultFunction.getDictionary(forKey: BazarBit.BILLINGADDRESS)! as [String : AnyObject]
                
                let strBillingFirstName = PIService.object_forKeyWithValidationForClass_String(dict: billdict, key: "first_name")
                print(strBillingFirstName)
                let strBillingLastName = PIService.object_forKeyWithValidationForClass_String(dict: billdict, key: "last_name")
                print(strBillingLastName)
                
                if firstName == strBillingFirstName && lastName == strBillingLastName
                {
                    UserDefaults.standard.removeObject(forKey: BazarBit.BILLINGADDRESS)
                    APPDELEGATE.chkAddAddress = false
                    callService_DeleteAddress()
                } else {
                    callService_DeleteAddress()
                }
            } else {
                callService_DeleteAddress()
            }
        } else {
            //callService_DeleteAddress()
        }
    }
    
    //MARK: - Service Method
    func callService_AddressList()  {
        
        let strurl: String = BazarBit.GetaddressListService()
        let url: URL = URL(string: strurl)!
        
        var headers: HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        
        var parameter: Parameters = [:]
        let UserId = UserDefaults.standard.object(forKey: BazarBit.USERID)
        
        if UserId != nil {
            headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
            if self.fromVC == "CheckOut" {
                parameter["address_type"] = strAddressType
            } else {
                //parameter["address_type"] = strAddressType
            }
            print(parameter)
        }
        else
        {
            if self.fromVC == "CheckOut" {
                parameter["address_type"] = strAddressType
            } else {
                //parameter["address_type"] = strAddressType
            }
            print(parameter)
        }
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: { (swiftyJson, responseDict) in
            
            let dictResponse: [String: AnyObject] = responseDict
            
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            let status: String = PIService.object_forKeyWithValidationForClass_String(dict: dictResponse, key: "status")
            
            if status == "ok" {
                
                if (dictResponse["payload"] as? [AnyObject]) != nil {
                    self.arrAddressList = dictResponse["payload"] as! [AnyObject]
                    
                    if self.arrAddressList.count > 0 {
                        
                        self.strCheckService = ""
                        
                        self.tbladdressList.dataSource = self
                        self.tbladdressList.delegate = self
                        self.viewNofound.isHidden = true
                        
                        if Constants.DeviceType.IS_IPAD {
                            self.tbladdressListHeight.constant = CGFloat(self.arrAddressList.count * 140)
                        } else {
                            self.tbladdressListHeight.constant = CGFloat(self.arrAddressList.count * 120)
                        }
                        self.tbladdressList.reloadData()
                        
                    } else {
                        
                        if APPDELEGATE.chkLablecheck != "" {
                            self.strCheckService = "yes"
                            self.viewNofound.isHidden = false
                            
                            let addAddressVC: AddAddressViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressViewController") as! AddAddressViewController
                            addAddressVC.addAddressListObj = self
                            
                            self.navigationController?.pushViewController(addAddressVC, animated: true)
                            
                            if Constants.DeviceType.IS_IPAD {
                                self.tbladdressListHeight.constant = CGFloat(SCREEN_HEIGHT - 250)
                            } else {
                                self.tbladdressListHeight.constant = CGFloat(SCREEN_HEIGHT - 200)
                            }
                            
                            self.tbladdressList.reloadData()
                        } else {
                            
                            self.strCheckService = "yes"
                            self.viewNofound.isHidden = false
                            
                            let addAddressVC: ProfileAddAddressViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileAddAddressViewController") as! ProfileAddAddressViewController
                            
                            //addAddressVC.addAddressListObj = self
                            self.navigationController?.pushViewController(addAddressVC, animated: true)
                            
                            if Constants.DeviceType.IS_IPAD {
                                self.tbladdressListHeight.constant = CGFloat(SCREEN_HEIGHT - 250)
                            } else {
                                self.tbladdressListHeight.constant = CGFloat(SCREEN_HEIGHT - 200)
                            }
                            
                            self.tbladdressList.reloadData()
                        }
                    }
                } else {
                    
                    if APPDELEGATE.chkLablecheck != "" {
                        self.strCheckService = ""
                        let addAddressVC: AddAddressViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressViewController") as! AddAddressViewController
                        addAddressVC.addAddressListObj = self
                        self.navigationController?.pushViewController(addAddressVC, animated: true)
                    } else {
                        //self.strCheckService = ""
                        let addAddressVC: ProfileAddAddressViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileAddAddressViewController") as! ProfileAddAddressViewController
                        // addAddressVC.addAddressListObj = self
                        self.navigationController?.pushViewController(addAddressVC, animated: true)
                    }
                }
            } else {
                PIAlertUtils.displayAlertWithMessage(message)
            }
        })
    }
    
    func callService_DeleteAddress()  {
        
        let strurl: String = BazarBit.GetDeleteAddressService()
        
        let url: URL = URL(string: strurl)!
        
        let strAuthToken: String = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as! String
        
        var headers: HTTPHeaders = [:]
        
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        headers["auth-token"] = strAuthToken
        
        var parameter: Parameters = [:]
        parameter["useradress_id"] = strUserAddressID
        parameter["address_type"] = strAddressType
        
        print(parameter)
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: { (swiftyJson, responseDict) in
            
            print(responseDict)
            let dictResponse: [String: AnyObject] = responseDict
            
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            let status: String = PIService.object_forKeyWithValidationForClass_String(dict: dictResponse, key: "status")
            
            if status == "ok" {
                self.callService_AddressList()
                
                if self.arrAddressList.count > 1 {
                    PIAlertUtils.displayAlertWithMessage(message)
                }
                //PIAlertUtils.displayAlertWithMessage(message)
            }  else {
                PIAlertUtils.displayAlertWithMessage(message)
            }
        })
        
    }
    
    // MARK: - Helper Method
    func setBorderPropertiesForButton()  {
        btnAddAddress.layer.cornerRadius = 20.0
        btnAddAddress.setButtonDynamicColor()
        btnAddAddress.Shadow()
    }
}

class AddressListCell: UITableViewCell {
    
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblAddress: UILabel!
    @IBOutlet var lblCity: UILabel!
    @IBOutlet var lblState: UILabel!
    @IBOutlet var lblMobileNo: UILabel!
    
    @IBOutlet var btnEdit: UIButton!
    @IBOutlet var btnDelete: UIButton!
    
    @IBOutlet var viewAddressList: UIView!
    
    override func awakeFromNib() {
        lblName.setTextLabelDynamicTextColor()
        lblAddress.setTextLabelLightColor()
        lblCity.setTextLabelLightColor()
        lblState.setTextLabelLightColor()
        lblMobileNo.setTextLabelDynamicTextColor()
        
        btnEdit.setButtonTextLightColor()
        btnDelete.setButtonTextLightColor()
        self.viewAddressList.applyCornerRadius(5.0)
        self.viewAddressList.applyShadowDefault()
    }
}
