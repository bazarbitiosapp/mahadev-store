//
//  DrawerVC.swift
//  BazarBit
//
//  Created by Parghi Infotech on 09/10/17.
//  Copyright © 2017 Parghi Infotech. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class DrawerVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    var tabbar: TabbarVC!
    
    @IBOutlet weak var btnLogout: UIButton!
    @IBOutlet var imageViewLogout : UIImageView!
    @IBOutlet var labelTitle: PILabelDrawerTitle!
    
    @IBOutlet weak var viewLogout: UIView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var tblDrawer: UITableView!
    
    @IBOutlet var labelName: UILabel!
    @IBOutlet var labelEmail: UILabel!
    
    var firstSection: [AnyObject] = []
    var secondSection: [AnyObject] = []
    var allSection: [[AnyObject]] = []
    
    //New variable
    var alltitleValue : [AnyObject] = []
    var dictMergeData : [String:AnyObject] = [:]
    
    var index : IndexPath = [0,0]
    var isSelected : Bool = true
    
    var dictResponse:[String : AnyObject] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imgUser.RoundImage()
        self.ServiceCallForGetCMSPage()
        getMenuListing()
        btnLogout.DynamicBtnTitleColor()
    }
    
    func getMenuListing() {
        let HomeDictionary = ["slug":"","title":"Home"]
        //let MyAddressDictionary = ["slug":"","title":"My Address"]
        let feedbackDictionary = ["slug":"","title":"Feedback"]
        let contactUsDictionary = ["slug":"","title":"Contact Us"]
        
        alltitleValue.append(HomeDictionary as AnyObject)
        //alltitleValue.append(MyAddressDictionary as AnyObject)
        alltitleValue.append(feedbackDictionary as AnyObject)
        alltitleValue.append(contactUsDictionary as AnyObject)
        print(alltitleValue)
        tblDrawer.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //headerView.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        viewLogout.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        
        self.labelName.textColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        
        self.labelEmail.textColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        
        if (UserDefaults.standard.object(forKey: BazarBit.USERID) != nil) {
            self.imageViewLogout.image = UIImage(named: "logout1")
            self.labelTitle.text = "LOG OUT"
            self.labelTitle.setTextLabelButtonTextColor()
            
            self.labelName.text = UserDefaults.standard.value(forKey: BazarBit.USERNAME) as? String
            //self.labelName.setTextLabelButtonTextColor()
            
            self.labelEmail.text = UserDefaults.standard.value(forKey: BazarBit.USEREMAILADDRESS) as? String
            //self.labelEmail.setTextLabelButtonTextColor()
            
            if (UserDefaults.standard.object(forKey: BazarBit.USERPROFILEIMAGE) != nil)
            {
                var imgUrl:String = UserDefaults.standard.object(forKey: BazarBit.USERPROFILEIMAGE) as! String
                imgUrl = imgUrl.replacingOccurrences(of: " ", with: "%20")
                self.imgUser.sd_setImage(with: URL(string: imgUrl), placeholderImage: UIImage(named: "profile"))
            }
            else
            {
                self.imgUser.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "profile"))
            }
            
        } else {
            self.labelTitle.text = "LOG IN"
            self.imageViewLogout.image = UIImage(named: "login")
            self.labelTitle.setTextLabelButtonTextColor()
            
            self.labelName.text = "Guest User"
            self.labelEmail.text = "Guest Email Address"
            //self.labelEmail.setTextLabelButtonTextColor()
            
            self.imgUser.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "profile"))
        }
        tblDrawer.reloadData()
    }
    
    // MARK: - UITableview Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if IS_IPAD_DEVICE() {
            return 55.0
        }
        return 40.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //let arrRow:[AnyObject] = allSection[section]
        //return arrRow.count
        return alltitleValue.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        index = indexPath
        let dict : [String:AnyObject] = alltitleValue[indexPath.row] as! [String : AnyObject]
        
        let strMenu:String = dict["title"] as! String
        print(strMenu)
        
        let strSlug:String = dict["slug"] as! String
        print(strSlug)
        
        if strSlug == "" {
            switch strMenu {
            case "Home":
                APPDELEGATE.openHomePage()
                break
                
//            case "My Address":
//                self.callService_AddressList()
//                break
                
            case "Feedback": //FEEDBACK
                
                let feedbackVC :FeedBackViewController = self.storyboard?.instantiateViewController(withIdentifier: "FeedBackViewController") as! FeedBackViewController
                
                self.evo_drawerController?.toggleLeftDrawerSide(animated: true, completion: nil)
                
                if (UserDefaults.standard.object(forKey: BazarBit.USERID) != nil)  {
                    if tabbar.selectedIndex == 0 {
                        tabbar.navigationHomeVC.pushViewController(feedbackVC, animated: true)
                        
                    } else if tabbar.selectedIndex == 1 {
                        tabbar.navigationCategoryVC.pushViewController(feedbackVC, animated: true)
                        
                    } else if tabbar.selectedIndex == 2 {
                        APPDELEGATE.navigationWishListVC.pushViewController(feedbackVC, animated: true)
                        
                    } else if tabbar.selectedIndex == 3 {
                        tabbar.navigationProfileVC.pushViewController(feedbackVC, animated: true)
                    }
                } else {
                    if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
                        myDelegate.openLoginPage()
                        //PIAlertUtils.displayAlertWithMessage("You have to Login...!")
                    }
                }
                
                break
            case "Contact Us": //CONTACT US
                
                let contactUsVC :ContactUsViewController = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
                
                self.evo_drawerController?.toggleLeftDrawerSide(animated: true, completion: nil)
                
                if (UserDefaults.standard.object(forKey: BazarBit.USERID) != nil)  {
                    if tabbar.selectedIndex == 0 {
                        tabbar.navigationHomeVC.pushViewController(contactUsVC, animated: true)
                        
                    } else if tabbar.selectedIndex == 1 {
                        tabbar.navigationCategoryVC.pushViewController(contactUsVC, animated: true)
                        
                    } else if tabbar.selectedIndex == 2 {
                        APPDELEGATE.navigationWishListVC.pushViewController(contactUsVC, animated: true)
                        
                    } else if tabbar.selectedIndex == 3 {
                        tabbar.navigationProfileVC.pushViewController(contactUsVC, animated: true)
                    }
                } else {
                    if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
                        myDelegate.openLoginPage()
                        //PIAlertUtils.displayAlertWithMessage("You have to Login...!")
                    }
                }
                break
            default:
                break
            }
        }
            
        else {
            UserDefaults.standard.set(dict["slug"] as! String, forKey: "slug")
            UserDefaults.standard.set(dict["title"] as! String, forKey: "title")
            
            let cmsPageVC:CMSViewController = self.storyboard?.instantiateViewController(withIdentifier: "CMSViewController") as! CMSViewController
            
            self.evo_drawerController?.toggleLeftDrawerSide(animated: true, completion: nil)
            
            if tabbar.selectedIndex == 0 {
                tabbar.navigationHomeVC.pushViewController(cmsPageVC, animated: true)
                
            } else if tabbar.selectedIndex == 1 {
                tabbar.navigationCategoryVC.pushViewController(cmsPageVC, animated: true)
                
            } else if tabbar.selectedIndex == 2 {
                APPDELEGATE.navigationWishListVC.pushViewController(cmsPageVC, animated: true)
                
            } else if tabbar.selectedIndex == 3 {
                tabbar.navigationProfileVC.pushViewController(cmsPageVC, animated: true)
            }
        }
        
        // Get From String firstSection
        //        if indexPath.section != 1 { //STATIC
        //
        //            // Get From String firstSection
        //            //PREV
        //            let previousCell : DrawerTableViewCell  = tableView.cellForRow(at: index) as! DrawerTableViewCell
        //
        //            let arrPrevRow:[AnyObject] = allSection[index.section]
        //            previousCell.labelTitle.text = arrPrevRow[index.row] as? String
        //            previousCell.labelTitle.setTextLabelLightColor()
        //
        //            previousCell.labelDot.backgroundColor = UIColor(hexString: Constants.TEXT_LIGHT_COLOR)
        //
        //            //CURRENT
        //            let currentCell : DrawerTableViewCell  = tableView.cellForRow(at: indexPath) as! DrawerTableViewCell
        //
        //            print(allSection)
        //            let arrRow:[AnyObject] = allSection[indexPath.section]
        //            print(arrRow[indexPath.row] as? String ?? "nil")
        //            currentCell.labelTitle.text = arrRow[indexPath.row] as? String
        //            currentCell.labelTitle.setTextLabelThemeColor()
        //            index = indexPath
        //
        //            currentCell.labelDot.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        //
        //            let strMenu:String = (arrRow[indexPath.row] as? String)!
        //
        //            switch strMenu {
        //            case "Home":
        //                APPDELEGATE.openHomePage()
        //                break
        //            case "Order History": //ORDER HISTORY
        //                let orderHistoryVC : OrderViewController = self.storyboard?.instantiateViewController(withIdentifier: "OrderViewController") as! OrderViewController
        //
        //                orderHistoryVC.isFromMenu = true
        //
        //                self.evo_drawerController?.toggleLeftDrawerSide(animated: true, completion: nil)
        //
        //                if (UserDefaults.standard.object(forKey: BazarBit.USERID) != nil)  {
        //
        //                    if tabbar.selectedIndex == 0 {
        //                        tabbar.navigationHomeVC.pushViewController(orderHistoryVC, animated: true)
        //
        //                    } else if tabbar.selectedIndex == 1 {
        //                        tabbar.navigationCategoryVC.pushViewController(orderHistoryVC, animated: true)
        //
        //                    } else if tabbar.selectedIndex == 2 {
        //                        tabbar.navigationWishListVC.pushViewController(orderHistoryVC, animated: true)
        //
        //                    } else if tabbar.selectedIndex == 3 {
        //                        tabbar.navigationProfileVC.pushViewController(orderHistoryVC, animated: true)
        //                    }
        //
        //                } else {
        //                    if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
        //                        myDelegate.openLoginPage()
        //                        PIAlertUtils.displayAlertWithMessage("You have to Login...!")
        //                    }
        //                }
        //
        //                break
        //            case "My Address":
        //                self.callService_AddressList()
        //                break
        //            case "Feedback": //FEEDBACK
        //
        //                let feedbackVC :FeedBackViewController = self.storyboard?.instantiateViewController(withIdentifier: "FeedBackViewController") as! FeedBackViewController
        //
        //                self.evo_drawerController?.toggleLeftDrawerSide(animated: true, completion: nil)
        //
        //                if (UserDefaults.standard.object(forKey: BazarBit.USERID) != nil)  {
        //                    if tabbar.selectedIndex == 0 {
        //                        tabbar.navigationHomeVC.pushViewController(feedbackVC, animated: true)
        //
        //                    } else if tabbar.selectedIndex == 1 {
        //                        tabbar.navigationCategoryVC.pushViewController(feedbackVC, animated: true)
        //
        //                    } else if tabbar.selectedIndex == 2 {
        //                        tabbar.navigationWishListVC.pushViewController(feedbackVC, animated: true)
        //
        //                    } else if tabbar.selectedIndex == 3 {                        tabbar.navigationProfileVC.pushViewController(feedbackVC, animated: true)
        //                    }
        //                } else {
        //                    if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
        //                        myDelegate.openLoginPage()
        //                        PIAlertUtils.displayAlertWithMessage("You have to Login...!")
        //                    }
        //                }
        //
        //                break
        //            case "Contact Us": //CONTACT US
        //
        //                let contactUsVC :ContactUsViewController = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
        //
        //                self.evo_drawerController?.toggleLeftDrawerSide(animated: true, completion: nil)
        //
        //                if (UserDefaults.standard.object(forKey: BazarBit.USERID) != nil)  {
        //                    if tabbar.selectedIndex == 0 {
        //                        tabbar.navigationHomeVC.pushViewController(contactUsVC, animated: true)
        //
        //                    } else if tabbar.selectedIndex == 1 {
        //                        tabbar.navigationCategoryVC.pushViewController(contactUsVC, animated: true)
        //
        //                    } else if tabbar.selectedIndex == 2 {
        //                        tabbar.navigationWishListVC.pushViewController(contactUsVC, animated: true)
        //
        //                    } else if tabbar.selectedIndex == 3 {
        //                        tabbar.navigationProfileVC.pushViewController(contactUsVC, animated: true)
        //                    }
        //                } else {
        //                    if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
        //                        myDelegate.openLoginPage()
        //                        PIAlertUtils.displayAlertWithMessage("You have to Login...!")
        //                    }
        //                }
        //                break
        //            default:
        //                break
        //            }
        //        }
        //        else {   //DYNAMIC // Get From [[String:Anyobject]] secondSection (Array and In Dictionary also)
        //
        //            let dict:[String:AnyObject] = secondSection[indexPath.row] as! [String : AnyObject]
        //
        //            UserDefaults.standard.set(dict["slug"] as! String, forKey: "slug")
        //            UserDefaults.standard.set(dict["title"] as! String, forKey: "title")
        //
        //            let cmsPageVC:CMSViewController = self.storyboard?.instantiateViewController(withIdentifier: "CMSViewController") as! CMSViewController
        //
        //            self.evo_drawerController?.toggleLeftDrawerSide(animated: true, completion: nil)
        //
        //            if tabbar.selectedIndex == 0 {
        //                tabbar.navigationHomeVC.pushViewController(cmsPageVC, animated: true)
        //
        //            } else if tabbar.selectedIndex == 1 {
        //                tabbar.navigationCategoryVC.pushViewController(cmsPageVC, animated: true)
        //
        //            } else if tabbar.selectedIndex == 2 {
        //                tabbar.navigationWishListVC.pushViewController(cmsPageVC, animated: true)
        //
        //            } else if tabbar.selectedIndex == 3 {
        //                tabbar.navigationProfileVC.pushViewController(cmsPageVC, animated: true)
        //            }
        //        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : DrawerTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! DrawerTableViewCell
        
        let dict : [String:AnyObject] = alltitleValue[indexPath.row] as! [String : AnyObject]
        print(dict)
        
        cell.labelTitle.textColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        cell.labelDot.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        cell.labelSideView.backgroundColor = UIColor(hexString: Constants.THEME_PRIMARY_COLOR)
        
        if index.section == indexPath.section && index.row == indexPath.row {
            
            let str = dict["title"] as! String
            let strUpper = str.uppercased()
            cell.labelTitle.text = strUpper
           
            cell.backgroundColor = UIColor(hexString: "#DCDCDC")
            cell.labelSideView.isHidden = false
            isSelected = false
            
        } else {
            
            let str = dict["title"] as! String
            let strUpper = str.uppercased()
            cell.labelTitle.text = strUpper
            cell.labelSideView.isHidden = true
            cell.labelSideView.backgroundColor = UIColor(hexString: Constants.TEXT_LIGHT_COLOR)
            cell.backgroundColor = UIColor.clear
        }
        
        return cell
    }
    
    //MARK: OPEN ADDRESS LIST
    func openAddressListVC() {
        let myAddressVC : AddressListViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddressListViewController") as! AddressListViewController
        
        myAddressVC.isFromMenu = true
        myAddressVC.hidesBottomBarWhenPushed = true
        
        self.evo_drawerController?.toggleLeftDrawerSide(animated: true, completion: nil)
        
        
        if (UserDefaults.standard.object(forKey: BazarBit.USERID) != nil)  {
            if tabbar.selectedIndex == 0 {
                tabbar.navigationHomeVC.pushViewController(myAddressVC, animated: true)
                
            } else if tabbar.selectedIndex == 1 {
                tabbar.navigationCategoryVC.pushViewController(myAddressVC, animated: true)
                
            } else if tabbar.selectedIndex == 2 {
                APPDELEGATE.navigationWishListVC.pushViewController(myAddressVC, animated: true)
                
            } else if tabbar.selectedIndex == 3 {
                tabbar.navigationProfileVC.pushViewController(myAddressVC, animated: true)
            }
        } else {
            if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
                myDelegate.openLoginPage()
                //PIAlertUtils.displayAlertWithMessage("You have to Login...!")
            }
        }
    }
    
    //MARK: OPEN ADD ADDRESS LIST
    func openAddAddressVC() {
        
        let addAddressVC : AddAddressViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressViewController") as! AddAddressViewController
        
        addAddressVC.isFromMenu = true
        addAddressVC.hidesBottomBarWhenPushed = true
        
        self.evo_drawerController?.toggleLeftDrawerSide(animated: true, completion: nil)
        
        if (UserDefaults.standard.object(forKey: BazarBit.USERID) != nil)  {
            if tabbar.selectedIndex == 0 {
                tabbar.navigationHomeVC.pushViewController(addAddressVC, animated: true)
                
            } else if tabbar.selectedIndex == 1 {
                tabbar.navigationCategoryVC.pushViewController(addAddressVC, animated: true)
                
            } else if tabbar.selectedIndex == 2 {
                APPDELEGATE.navigationWishListVC.pushViewController(addAddressVC, animated: true)
                
            } else if tabbar.selectedIndex == 3 {
                tabbar.navigationProfileVC.pushViewController(addAddressVC, animated: true)
            }
        } else {
            if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
                myDelegate.openLoginPage()
                //PIAlertUtils.displayAlertWithMessage("You have to Login...!")
            }
        }
    }
    
    @IBAction func btnLogout(_ sender: Any) {
        
        if labelTitle.text == "LOG IN" {
            APPDELEGATE.openLoginPage()
        }else {
            self.ServiceCallForLogOut()
        }
    }
    
    //MARK: SERVICE CALL FOR ADDRESS LIST
//    func callService_AddressList()  {
//
//        let strurl: String = BazarBit.GetaddressListService()
//        let url: URL = URL(string: strurl)!
//
//        var headers: HTTPHeaders = [:]
//        headers[BazarBit.ContentType] = BazarBit.applicationJson
//        headers["app-id"] = BazarBit.appId
//        headers["app-secret"] = BazarBit.appSecret
//
//        let parameter: Parameters = [:]
//
//        let UserId = UserDefaults.standard.object(forKey: BazarBit.USERID)
//
//        if UserId != nil {
//            headers["auth-token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN) as? String
//            print(parameter)
//        }
//        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, completion: { (swiftyJson, responseDict) in
//
//            print(responseDict)
//            let dictResponse: [String: AnyObject] = responseDict
//
//            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
//            let status: String = PIService.object_forKeyWithValidationForClass_String(dict: dictResponse, key: "status")
//
//            if status == "ok" {
//
//                if (dictResponse["payload"] as? [AnyObject]) == nil {
//
//                    let addAddressVC: AddAddressViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressViewController") as! AddAddressViewController
//
//                    addAddressVC.isFromMenu = true
//                    addAddressVC.hidesBottomBarWhenPushed = true
//
//                    self.navigationController?.pushViewController(addAddressVC, animated: true)
//                }else {
//
//                    let arrPayLoadList: [AnyObject] = dictResponse["payload"] as! [AnyObject]
//
//                    if arrPayLoadList.count > 0 {
//                        self.openAddressListVC()
//                    } else {
//                        self.openAddAddressVC()
//                    }
//                }
//            } else {
//
//                PIAlertUtils.displayAlertWithMessage(message)
//            }
//        })
//    }
    
    //MARK: ServiceCallForLogOut
    func ServiceCallForLogOut() {
        let strurl: String = BazarBit.logoutService()
        let url: URL = URL(string: strurl)!
        
        var headers: HTTPHeaders = [:]
        
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        
        var parameter: Parameters = [:]
        
        parameter["user_token"] = UserDefaults.standard.object(forKey: BazarBit.AUTHTOKEN)
        
        print(parameter)
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: { (swiftyJson, responseDict) in
            
            self.dictResponse = responseDict
            
            let message = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            let status: String = PIService.object_forKeyWithValidationForClass_String(dict: self.dictResponse, key: "status")
            
            let statusCode: Int = BazarBit.getValue(dict: responseDict, key: "code")
            
            print(status)
            
            if statusCode == 401 || statusCode == 200 {
                
                if (self.dictResponse["payload"] != nil) {
                    UserDefaults.standard.removeObject(forKey: BazarBit.USERACCOUNT)
                    UserDefaults.standard.removeObject(forKey: BazarBit.AUTHTOKEN)
                    UserDefaults.standard.removeObject(forKey: BazarBit.USERID)
                    UserDefaults.standard.removeObject(forKey: BazarBit.USEREMAILID)
                    
                    UserDefaults.standard.removeObject(forKey: BazarBit.USERNAME)
                    UserDefaults.standard.removeObject(forKey: BazarBit.USEREMAILADDRESS)
                    UserDefaults.standard.removeObject(forKey: BazarBit.USERPROFILEIMAGE)
                    
                    UserDefaults.standard.removeObject(forKey: BazarBit.SHIPPINGADDRESS)
                    UserDefaults.standard.removeObject(forKey: BazarBit.BILLINGADDRESS)
                    
                    UserDefaults.standard.removeObject(forKey: "slug")
                    UserDefaults.standard.removeObject(forKey: "title")
                    
                    if let myDelegate = UIApplication.shared.delegate as? AppDelegate {
                        UserDefaults.standard.removeObject(forKey: BazarBit.AUTOPINCODE)
                        myDelegate.openLoginPage()
                        
                    }
                }
            }
            else {
                PIAlertUtils.displayAlertWithMessage(message)
            }
        })
    }
    
    //MARK: ServiceCallForGetCMSPage
    func ServiceCallForGetCMSPage() {
        
        let strurl:String = BazarBit.GetCMSPageService()
        let url:URL = URL(string: strurl)!
        
        var headers:HTTPHeaders = [:]
        headers[BazarBit.ContentType] = BazarBit.applicationJson
        headers["app-id"] = BazarBit.appId
        headers["app-secret"] = BazarBit.appSecret
        print(headers)
        
        let parameter:Parameters = [:]
        print(parameter)
        
        PIService.serviceCall(url: url, method: .post, parameter: parameter, encoding: JSONEncoding.default, headers: headers, viewVC: self, isLoaderShow: true, completion: {(swiftyJson, responseDict) in
            
            let status:String = responseDict["status"] as! String
            let messege = BazarBit.getmessageFromService(dict: responseDict as [String : AnyObject])
            
            if status == "ok"
            {
                self.secondSection = []
                if (responseDict["payload"] != nil)
                {
                    self.secondSection = responseDict["payload"] as! [[String:AnyObject]] as [AnyObject]
                    print(self.secondSection)
                    
                    for i in 0...self.secondSection.count - 1 {
                        let dict:[String: AnyObject] = self.secondSection[i] as! [String: AnyObject]
                        
                        let strSlug: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "slug")
                        let strTitle: String = PIService.object_forKeyWithValidationForClass_String(dict: dict, key: "title")
                        
                        self.dictMergeData = ["slug":strSlug as AnyObject,"title":strTitle as AnyObject]
                        print(self.dictMergeData)
                        
                        self.alltitleValue.append(self.dictMergeData as AnyObject)
                    }
                    print(self.alltitleValue)
                    print(self.alltitleValue.count)
                }
            }
            else
            {
                PIAlertUtils.displayAlertWithMessage(messege)
            }
        })
    }
}
